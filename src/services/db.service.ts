import { Sequelize } from "sequelize-typescript";
import { ENV_MYSQL_DB, ENV_MYSQL_HOSTNAME, ENV_MYSQL_PASSWORD, ENV_MYSQL_USER } from "../util/secrets.util";
import logger from "../util/logger.util";
import { QueryOptions } from "sequelize";
import { PreUser } from "../models/pre-user.model";
import { Doctor } from "../models/doctor.model";
import { FrontDesk } from "../models/front-desk.model";
import { State } from "../models/state.model";
import { City } from "../models/city.model";
import { Speciality } from "../models/speciality.model";
import { Clinic } from "../models/clinic.model";
import { ClinicSpeciality } from "../models/clinic-speciality.model";
import { ClinicDoctor } from "../models/clinic-doctor.model";
import { Patient } from "../models/patient.model";
import { Token } from "../models/token.model";
import { Examination } from "../models/examinations.model";
import { Symptom } from "../models/symptom.model";
import { PatientVisit } from "../models/patient-visit.model";
import { Employee } from "../models/employee.model";
import { CdEmployee } from "../models/cd-employee.model";
import { FeedbackQuestion } from "../models/feedback-question.model";
import { Feedback } from "../models/feedback.model";
import { Medicine } from "../models/medicine.model";
import { Habit } from "../models/habit.model";
import { Variable } from "../models/variable.model";
import { Recommendation } from "../models/recommendation.model";
import { VisitAttachment } from "../models/visit-attachment.model";
import { FdCd } from "../models/fd-cd.model";

class DBService {
  private _sequelize: Sequelize;

  private constructor() {
    logger.silly("[GD] DBService");
    this._sequelize = new Sequelize({
      dialect : "mysql",
      host    : ENV_MYSQL_HOSTNAME,
      database: ENV_MYSQL_DB,
      username: ENV_MYSQL_USER,
      password: ENV_MYSQL_PASSWORD,
    });

    this._sequelize.addModels([
      PreUser,
      Doctor,
      FrontDesk,
      State,
      City,
      Speciality,
      Clinic,
      ClinicSpeciality,
      ClinicDoctor,
      Patient,
      Token,
      Examination,
      Symptom,
      PatientVisit,
      Employee,
      CdEmployee,
      FeedbackQuestion,
      Feedback,
      Medicine,
      Habit,
      Variable,
      Recommendation,
      VisitAttachment,
      FdCd
    ]);
  }

  static getInstance(): DBService {
    return new DBService();
  }

  async rawQuery(sql: string | { query: string, values: any[] }, options?: QueryOptions): Promise<any> {
    return this._sequelize.query(sql, options);
  }

  getSequelize(): Sequelize {
    return this._sequelize;
  }
}

export const dbService = DBService.getInstance();
