import logger from "../../util/logger.util";
import { SymptomCreateDto } from "../../dtos/symptom/symptom-create.dto";
import { Symptom } from "../../models/symptom.model";
import { SymptomType } from "../../enums/symptom.enum";
import { HabitUpdateDto } from "../../dtos/habit/habit-update.dto";
import { HabitCreateDto } from "../../dtos/habit/habit-create.dto";
import { Habit } from "../../models/habit.model";
import { Helpers } from "../../util/helpers.util";

class SymptomService {
  constructor() {
    logger.silly("[N-GC] SymptomService");
  }

  static getInstance(): SymptomService {
    return new SymptomService();
  }

  async show(symptomId: number): Promise<Symptom> {
    return Symptom.findById(symptomId);
  }

  async showByDoctorId(doctorId: number): Promise<Symptom[]> {
    return Symptom.findAll({
      where: {
        doctor_id: doctorId
      }
    });
  }

  async showAllSymptoms(doctorId: number): Promise<Symptom[]> {
    return Symptom.findAll({
      where: {
        doctor_id: doctorId,
      }
    });
  }

  async showActiveSymptoms(doctorId: number): Promise<Symptom[]> {
    return Symptom.findAll({
      where: {
        doctor_id: doctorId,
        is_active: true
      },
      group: ["title"]
    });
  }

  async create(data: SymptomCreateDto): Promise<Symptom> {
    return Symptom.create({
      ...data,
      title: Helpers.titlecase(data.title),
    });
  }

  async bulkCreate(data: SymptomCreateDto[], doctorId: number): Promise<Symptom[]> {
    const symptoms: Symptom[] = [];
    for (const symptom of data) {
      const newSymptom = await Symptom.create({
        ...symptom,
        title: Helpers.titlecase(symptom.title),
        doctor_id: doctorId
      });
      symptoms.push(newSymptom);
    }
    return symptoms;
  }

  async update(symptom: Symptom, data: HabitUpdateDto): Promise<Symptom> {
    return symptom.update(data);
  }

  async delete(symptom: Symptom): Promise<any> {
    return symptom.destroy();
  }
}

export const symptomService = SymptomService.getInstance();
