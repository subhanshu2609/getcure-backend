import logger from "../../util/logger.util";
import { FrontDesk } from "../../models/front-desk.model";
import { FrontDeskCreateDto } from "../../dtos/front-desk/front-desk-create.dto";
import { Doctor } from "../../models/doctor.model";
import { DoctorUpdateDto } from "../../dtos/doctor/doctor-update.dto";
import { Helpers } from "../../util/helpers.util";
import { FrontDeskUpdateDto } from "../../dtos/front-desk/front-desk-update.dto";
import { Transaction } from "sequelize";
import { ClinicDoctor } from "../../models/clinic-doctor.model";
import { Clinic } from "../../models/clinic.model";
import { clinicService } from "./clinic.service";

class FrontDeskService {
  constructor() {
    logger.silly("[N-GC] FrontDeskService");
  }

  static getInstance(): FrontDeskService {
    return new FrontDeskService();
  }

  async createFrontDesk(data: FrontDeskCreateDto, transaction?: Transaction): Promise<FrontDesk> {
    return FrontDesk.create(data, {transaction});
  }

  async viewFrontDesksOfClinic(clinicId: number): Promise<FrontDesk[]> {
    return FrontDesk.findAll({
      where: {
        clinic_id: clinicId
      }
    });
  }

  async show(frontDeskId: number, withIncludes?: boolean): Promise<FrontDesk> {
    return FrontDesk.findOne({
      where  : {
        id: frontDeskId
      },
      include: withIncludes ? [
        {
          model  : ClinicDoctor,
          include: [
            Doctor
          ]
        },
        Clinic
      ] : []
    });
  }

  async showAllFrontDesks(): Promise<FrontDesk[]> {
    return FrontDesk.findAll({
      include: [ClinicDoctor, Clinic]
    });
  }

  async showFrontDeskByEmail(email: string, withIncludes?: boolean): Promise<FrontDesk> {
    return FrontDesk.findOne({
      where  : {
        email: email
      },
      include: withIncludes ? [
        {
          model  : ClinicDoctor,
          include: [
            Doctor,
            Clinic
          ]
        }
      ] : []
    });
  }

  async showFrontDeskByMobile(mobile: string, withIncludes?: boolean): Promise<FrontDesk> {
    return FrontDesk.findOne({
      where  : {
        mobile_no: mobile
      },
      include: withIncludes ? [
        {
          model  : ClinicDoctor,
          include: [
            Doctor,
            Clinic
          ]
        }
      ] : []
    });
  }

  async update(frontDesk: FrontDesk, data: FrontDeskUpdateDto): Promise<FrontDesk> {
    Helpers.removeUndefinedKeys(data);
    return frontDesk.update(data);
  }

  async delete(frontDesk: FrontDesk): Promise<any> {
    await frontDesk.destroy();
  }
}

export const frontDeskService = FrontDeskService.getInstance();
