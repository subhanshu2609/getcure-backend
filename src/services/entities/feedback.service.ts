import logger from "../../util/logger.util";
import { Feedback } from "../../models/feedback.model";
import { FeedbackCreateDto, FeedbackDto } from "../../dtos/feedback/feedback-create.dto";
import { PatientVisit } from "../../models/patient-visit.model";
import { FeedbackFiller } from "../../enums/feedback-filler.enum";
import { Transaction } from "sequelize";
import { recommendationService } from "./recommendation.service";
import { RecommendationCreateDto } from "../../dtos/recommendation/recommendation-create.dto";
import { RecommendationUpdateDto } from "../../dtos/recommendation/recommendation-update.dto";
import { Helpers } from "../../util/helpers.util";
import { Sequelize } from "sequelize";

class FeedbackService {
  constructor() {
    logger.silly("[N-GC] FeedbackService");
  }

  static getInstance(): FeedbackService {
    return new FeedbackService();
  }

  async show(feedbackId: number): Promise<Feedback> {
    return Feedback.findById(feedbackId);
  }

  async viewAll(): Promise<Feedback[]> {
    return Feedback.findAll();
  }

  async viewByClinicDoctor(clinicDoctorId: number): Promise<Feedback[]> {
    return Feedback.findAll({
      where: {
        clinic_doctor_id: clinicDoctorId
      }
    });
  }

  async viewByPatientVisit(patientVisitId: number): Promise<Feedback[]> {
    return Feedback.findAll({
      where: {
        patient_visit_id: patientVisitId,
        filler: FeedbackFiller.EMPLOYEE
      }
    });
  }

  async viewByQuestionCD(clinicDoctorIds: number[], question: string): Promise<Feedback[]> {
    const feedback: Feedback[] = [];
    const docFeedback          = await Feedback.findAll({
      where: {
        clinic_doctor_id: clinicDoctorIds,
        question        : Helpers.titlecase(question),
        filler          : FeedbackFiller.DOCTOR
      }
    });
    feedback.push(...docFeedback);
    const patFeedback = await Feedback.findAll({
      where: {
        clinic_doctor_id: clinicDoctorIds,
        question        : Helpers.titlecase(question),
        filler          : FeedbackFiller.PATIENT,
        patient_visit_id: {
          [Sequelize.Op.notIn]: feedback.map(i => i.patient_visit_id)
        }
      }
    });
    feedback.push(...patFeedback);
    const empFeedback = await Feedback.findAll({
      where: {
        clinic_doctor_id: clinicDoctorIds,
        question        : Helpers.titlecase(question),
        filler          : FeedbackFiller.EMPLOYEE,
        patient_visit_id: {
          [Sequelize.Op.notIn]: feedback.map(i => i.patient_visit_id)
        }
      }
    });
    feedback.push(...empFeedback);
    return feedback;
  }


  async create(data: FeedbackDto[], patientVisit: PatientVisit, filler: FeedbackFiller, transaction?: Transaction): Promise<void> {
    for (const feedback of data) {
      const medication = patientVisit.medication.find(m => m.disease.toLowerCase() === feedback.question.toLowerCase());
      await Feedback.create({
        patient_visit_id: patientVisit.id,
        clinic_doctor_id: patientVisit.clinic_doctor_id,
        question        : feedback.question,
        option          : feedback.option,
        medicine        : medication ? medication.medicines : [],
        filler          : filler
      });
      if (medication?.medicines?.length > 0) {
        const recommendation = await recommendationService.showByClinicDoctorIdDiseaseMedicine(
          patientVisit.clinic_doctor_id,
          Helpers.titlecase(feedback.question),
          medication.medicines.map(m => m.title)
        );
        if (!recommendation) {
          const recData: RecommendationCreateDto = {
            clinic_doctor_id  : patientVisit.clinic_doctor_id,
            disease           : Helpers.titlecase(feedback.question),
            medicines         : medication.medicines.map(m => m.title),
            total_count       : 1,
            cured             : 0,
            partially_cured   : 0,
            not_cured         : 0,
            symptoms_increased: 0
          };
          if (feedback.option === "Cured Complete") {
            recData.cured = 1;
          } else if (feedback.option === "Partially Cured") {
            recData.partially_cured = 1;
          } else if (feedback.option === "Not Cured") {
            recData.not_cured = 1;
          } else {
            recData.symptoms_increased = 1;
          }
          await recommendationService.createRecommendation(recData);
        } else {
          const recData: RecommendationUpdateDto = {
            total_count       : recommendation.total_count + 1,
            cured             : recommendation.cured,
            partially_cured   : recommendation.partially_cured,
            not_cured         : recommendation.not_cured,
            symptoms_increased: recommendation.symptoms_increased,
          };
          if (feedback.option === "Cured Complete") {
            recData.cured += 1;
          } else if (feedback.option === "Partially Cured") {
            recData.partially_cured += 1;
          } else if (feedback.option === "Not Cured") {
            recData.not_cured += 1;
          } else {
            recData.symptoms_increased += 1;
          }
          await recommendationService.updateRecommendation(recommendation, recData);
        }
      }
    }
  }

  async delete(feedback: Feedback): Promise<any> {
    return feedback.destroy();
  }
}

export const feedbackService = FeedbackService.getInstance();
