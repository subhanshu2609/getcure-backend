import logger from "../../util/logger.util";
import { Recommendation } from "../../models/recommendation.model";
import { clinicService } from "./clinic.service";
import {
  RecommendationBulkCreateDto,
  RecommendationCreateDto
} from "../../dtos/recommendation/recommendation-create.dto";
import { Op } from "sequelize";
import { RecommendationUpdateDto } from "../../dtos/recommendation/recommendation-update.dto";
import { patientVisitService } from "./patient-visit.service";
import { feedbackService } from "./feedback.service";
import _ from "lodash";
import { Variable } from "../../models/variable.model";
import { MedicineParameter, MedicineUpdateParameters } from "../../enums/medicine-parameter.enum";
import { Feedback } from "../../models/feedback.model";
import { Sequelize } from "sequelize";
import { FeedbackFiller } from "../../enums/feedback-filler.enum";
import { PatientVisit } from "../../models/patient-visit.model";
import { feedbackQuestionService } from "./feedback-question.service";

class RecommendationService {
  constructor() {
    logger.silly("[N-GC] RecommendationService");
  }

  static getInstance(): RecommendationService {
    return new RecommendationService();
  }

  async show(recommendationId: number): Promise<Recommendation> {
    return Recommendation.findOne({
      where: {
        id: recommendationId
      }
    });
  }

  async showByClinicDoctorIdDiseaseMedicine(clinicDoctorId: number, disease: string, medicine: string[]): Promise<Recommendation> {
    const med = JSON.stringify(medicine);
    return Recommendation.findOne({
      where: {
        clinic_doctor_id: clinicDoctorId,
        disease         : disease,
        medicines       : {
          [Op.eq]: medicine
        }
      }
    });
  }

  async showByDoctor(doctorId: number): Promise<Recommendation[]> {
    const clinicDoctors = await clinicService.showAllClinicDoctorByDoctor(doctorId);
    return Recommendation.findAll({
      where: {
        clinic_doctor_id: clinicDoctors.map(i => i.id)
      }
    });
  }

  async showByClinicDoctor(clinicDoctorId: number[]): Promise<Recommendation[]> {
    return Recommendation.findAll({
      where: {
        clinic_doctor_id: clinicDoctorId
      }
    });
  }

  async createRecommendation(data: RecommendationCreateDto): Promise<Recommendation> {
    return Recommendation.create(data);
  }

  async updateRecommendation(recommendation: Recommendation, data: RecommendationUpdateDto): Promise<Recommendation> {
    return recommendation.update(data);
  }

  async bulkCreateRecommendation(data: RecommendationBulkCreateDto) {
    for (const element of data.recommendations) {
      await Recommendation.create(element);
    }
  }

  async diseaseAnalysis(doctorId: number, filters: { start_date?: string, end_date?: string }) {
    const diseases: {
      name: string,
      total: number,
      feedback: number,
      cured: number,
      partial: number,
      not_cured: number,
      symptoms_increased: number,
      medicines: {
        title: string,
        count: number
      }[],
      quality: number
    }[]                           = [];
    const clinicDoctors           = await clinicService.showAllClinicDoctorByDoctor(doctorId);
    const patientVisits           = await patientVisitService.viewByClinicDoctors(clinicDoctors.map(i => i.id), filters);
    const visitDiseases: string[] = [];
    for (const visit of patientVisits) {
      const visitMedDis = visit.medication.map(m => m.disease);
      visitDiseases.push(...visitMedDis);
    }
    const diseaseCount = _.countBy(visitDiseases);
    for (const [key, value] of Object.entries(diseaseCount)) {
      const feedbacks           = await feedbackService.viewByQuestionCD(clinicDoctors.map(i => i.id), key);
      const docData             = {
        cured             : 0,
        partial           : 0,
        not_cured         : 0,
        symptoms_increased: 0,
      };
      const medicines: string[] = [];
      for (const feedback of feedbacks) {
        if (feedback.option === "Cured Complete") {
          docData.cured += 1;
        } else if (feedback.option === "Partially Cured") {
          docData.partial += 1;
        } else if (feedback.option === "Not Cured") {
          docData.not_cured += 1;
        } else {
          docData.symptoms_increased += 1;
        }
        if (feedback.medicine.length > 0) {
          const titles = feedback.medicine.map(m => m.title);
          medicines.push(...titles);
        }
      }
      const medicineCount = _.countBy(medicines);
      const finalMed: {
        title: string,
        count: number
      }[]                 = [];
      for (const [key, value] of Object.entries(medicineCount)) {
        finalMed.push({title: key, count: value});
      }
      const singleDoc = {
        name              : key,
        total             : value,
        feedback          : feedbacks.length,
        cured             : Math.round(docData.cured * 1000 / feedbacks.length) / 10,
        partial           : Math.round(docData.partial * 1000 / feedbacks.length) / 10,
        not_cured         : Math.round(docData.not_cured * 1000 / feedbacks.length) / 10,
        symptoms_increased: Math.round(docData.symptoms_increased * 1000 / feedbacks.length) / 10,
        medicines         : finalMed,
        quality           : (docData.cured + (docData.partial / 2) - (docData.not_cured) - (2 * docData.symptoms_increased)) * 100 / feedbacks.length
      };
      diseases.push(singleDoc);
    }
    return diseases;
  }


  async medicineAnalysis(doctorId: number, filters: { start_date?: string; end_date?: string }) {
    const clinicDoctors          = await clinicService.showAllClinicDoctorByDoctor(doctorId);
    const patientVisits          = await patientVisitService.viewByClinicDoctors(clinicDoctors.map(i => i.id), filters);
    const allMedicines: string[] = [];
    const variables: Variable[]  = await Variable.findAll({
      where: {
        doctor_id: doctorId
      }
    });
    const analysis: {
      title: string,
      pres_count: number,
      qty: number,
      diseases: { name: string, count: number }[]
    }[]                          = [];
    for (const visit of patientVisits) {
      for (const medication of visit.medication) {
        for (const medicine of medication.medicines) {
          const dose           = variables.find(v => v.title === medicine.dose && v.type === MedicineUpdateParameters.DOSE)?.value || 0;
          const duration       = variables.find(v => v.title === medicine.duration && v.type === MedicineUpdateParameters.DURATION)?.value || 0;
          const frequency      = variables.find(v => v.title === medicine.frequency && v.type === MedicineUpdateParameters.FREQUENCY)?.value || 0;
          const previousObject = analysis.find(m => m.title === medicine.title);
          if (previousObject) {
            const index         = analysis.indexOf(previousObject);
            analysis[index].pres_count += 1;
            analysis[index].qty = analysis[index].qty + (dose * duration * frequency);
            const disease       = analysis[index].diseases.find(d => d.name === medication.disease);
            if (disease) {
              const disIndex = analysis[index].diseases.indexOf(disease);
              analysis[index].diseases[disIndex].count += 1;
            } else {
              analysis[index].diseases.push({name: medication.disease, count: 1});
            }
          } else {
            analysis.push({
              title     : medicine.title,
              pres_count: 1,
              qty       : dose * duration * frequency,
              diseases  : [{name: medication.disease, count: 1}]
            });
          }
        }
      }
    }
    return analysis;
  }

  async patientFeedbackAnalysis(doctorId: number, filters: { start_date?: string; end_date?: string }): Promise<any[]> {
    const clinicDoctors = await clinicService.showAllClinicDoctorByDoctor(doctorId);
    const questions     = await feedbackQuestionService.index();
    const response: {
      question: string,
      options: {
        title: string,
        count: number
      }[]
    }[]                 = [];
    for (const question of questions) {
      const feedbacks = await Feedback.findAll({
        where: {
          clinic_doctor_id: clinicDoctors.map(c => c.id),
          question        : question.title
        }
      });
      const feedbackCount: {
        title: string,
        count: number
      }[]             = [];
      for (const option of question.options) {
        feedbackCount.push({
          title: option,
          count: 0
        });
      }
      for (const feedback of feedbacks) {
        const index = feedbackCount.findIndex(f => f.title === feedback.option);
        feedbackCount[index].count += 1;
      }
      response.push({
        question: question.title,
        options : feedbackCount
      });
    }
    const percentage: {
      question: string,
      options: {
        title: string,
        percent: number
      }[]
    }[] = [];
    for (const res of response) {
      let total = 0;
      for (const op of res.options) {
        total += op.count;
      }
      const options: {
        title: string,
        percent: number
      }[] = [];
      for (const op of res.options) {
        options.push({
          title: op.title,
          percent: Math.round((op.count / total) * 100)
        });
      }
      percentage.push({
        question: res.question,
        options: options
      });
    }
    return percentage;
  }
}

export const recommendationService = RecommendationService.getInstance();
