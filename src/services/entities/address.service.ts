import logger from "../../util/logger.util";
import { State } from "../../models/state.model";
import { StateCreateDto } from "../../dtos/state/state-create.dto";
import { CityCreateDto } from "../../dtos/state/city-create.dto";
import { City } from "../../models/city.model";
import { Helpers } from "../../util/helpers.util";

class AddressService {
  constructor() {
    logger.silly("[N-GC] AddressService");
  }

  static getInstance(): AddressService {
    return new AddressService();
  }

  async listStates(): Promise<State[]> {
    return State.findAll();
  }

  async listActiveStates(filters: { query?: string }): Promise<State[]> {
    let whereClause = {is_active: true};

    if (filters.query) {
      whereClause = {
        ...whereClause,
        ["title" as string]: {
          like: "%" + filters.query + "%"
        }
      };
    }
    return State.findAll({
      where: whereClause,
      order: [
        [
          "title",
          "asc"
        ]
      ]
    });
  }

  async listCities(stateId: number): Promise<City[]> {
    return City.findAll({
      where: {
        state_id: stateId
      }
    });
  }

  async findCity(id: number): Promise<City> {
    return City.findById(id);
  }

  async findState(id: number): Promise<State> {
    return State.findById(id);
  }

  async addState(data: StateCreateDto): Promise<State> {
    return State.create({title: Helpers.titlecase(data.title)});
  }

  async addCity(data: CityCreateDto): Promise<City> {
    return City.create(data);
  }

  async deleteAddress(address: State) {
    return address.destroy();
  }
}

export const addressService = AddressService.getInstance();
