import logger from "../../util/logger.util";
import { Transaction } from "sequelize";
import { TokenCreateDto } from "../../dtos/token/token-create.dto";
import { Token } from "../../models/token.model";
import moment, { now } from "moment";
import { ClinicDoctor } from "../../models/clinic-doctor.model";
import { Helpers } from "../../util/helpers.util";
import { Bookie } from "../../enums/bookie.enum";
import { Patient } from "../../models/patient.model";
import { Clinic } from "../../models/clinic.model";
import { DoctorNotAvailableException } from "../../exceptions/user/doctor-not-available.exception";
import { Op } from "sequelize";
import { PatientVisit } from "../../models/patient-visit.model";
import { BookingType } from "../../enums/token.enum";
import { Employee } from "../../models/employee.model";

class TokenService {
  constructor() {
    logger.silly("[N-GC] TokenService");
  }

  static getInstance(): TokenService {
    return new TokenService();
  }

  async show(tokenId: number): Promise<Token> {
    return Token.find({
      where  : {
        id: tokenId
      },
      include: [Patient]
    });
  }

  async showByDateTime(data: { clinic_doctor_id: number, date: Date, time: string }): Promise<Token> {
    return Token.findOne({
      where: {
        clinic_doctor_id: data.clinic_doctor_id,
        date            : data.date,
        time            : data.time
      }
    });
  }

  async showByPatientId(patient_id: string): Promise<Token[]> {
    return Token.findAll({
      where  : {
        patient_id: patient_id
      },
      include: [ClinicDoctor]
    });
  }

  async showByMobile(mobile: string): Promise<Token[]> {
    return Token.findAll({
      where  : {
        ["patient_id" as string]: {
          like: "%" + mobile + "%"
        }
      },
      include: [
        {
          model  : ClinicDoctor,
          as     : "clinic_doctor",
          include: [Clinic]
        }
      ]
    });
  }

  async showTokensBetweenDates(clinicDoctorId: number, start_date?: string, end_date?: string): Promise<Token[]> {
    if (!start_date) {
      start_date = moment.utc().add(1, "day").format("YYYY-MM-DD");
    }
    if (!end_date) {
      end_date = moment.utc(start_date).add(1, "day").format("YYYY-MM-DD");
    }

    return Token.findAll({
      where: {
        clinic_doctor_id: clinicDoctorId,
        date            : {
          [Op.between]: [
            start_date,
            end_date
          ]
        }
      },
      order: [
        [
          "date",
          "desc"
        ],
        [
          "time",
          "desc"
        ]
      ]
    });
  }

  async showTokenByDate(clinic_doctor_id: number, date: string, filters: { date?: string; query?: string }, withIncludes?: boolean): Promise<Token[]> {
    let whereClause: any;
    if (filters.query) {
      whereClause = {
        patient_name: {
          [Op.like]: `%${filters.query}%`
        }
      };
    }
    return Token.findAll({
      where  : {
        clinic_doctor_id: clinic_doctor_id,
        date            : date,
        ...whereClause
      },
      include: withIncludes ? [Patient] : [],
      order  : [
        [
          "token_no",
          "asc"
        ]
      ]
    });
  }

  async showBookedTokens(clinic_doctor_id: number, filters: { date?: string; query?: string }, withIncludes?: boolean): Promise<Token[]> {
    let whereClause: any;
    if (filters.query) {
      whereClause = {
        patient_name: {
          [Op.like]: `%${filters.query}%`
        }
      };
    }
    return Token.findAll({
      where  : {
        clinic_doctor_id: clinic_doctor_id,
        date            : {
          $gte: moment.utc().format("YYYY-MM-DD")
        },
        ...whereClause
      },
      include: withIncludes ? [Patient] : []
    });
  }

  async showUnbookedTokens(clinicDoctor: ClinicDoctor, date: string): Promise<any[]> {
    const day = Helpers.slugify(moment.utc(date).format("dddd"));
    let timing;
    for (const t of clinicDoctor.doctor_timings) {
      if (t.day === day) {
        timing = t;
      }
    }
    if (timing?.slots?.length === 0 || !timing) {
      throw new DoctorNotAvailableException();
    }

    const tokens: { time: string, is_booked: boolean, token_no?: number, slot_time?: string, patient_id?: string, patient_name?: string }[] = [];

    const booked = await this.showTokenByDate(clinicDoctor.id, date, {}, false);
    let index    = 0;

    for (const slot of timing.slots) {
      const seconds        = moment.utc(`${date} ${slot.end_time}`).diff(`${date} ${slot.start_time}`, "seconds");
      // throw seconds;
      const timePerPatient = +((+seconds) / slot.no_of_patients).toString().split(".")[0];
      // throw timePerPatient;
      const slot_time      = moment.utc(`${date} ${slot.start_time}`);
      const slot_next_time = moment.utc(`${date} ${slot.start_time}`).add(clinicDoctor.slot_time, "minutes");
      for (const st = moment.utc(`${date} ${slot.start_time}`), bookedTokens = booked; st < moment.utc(`${date} ${slot.end_time}`); st.add(timePerPatient, "seconds"), index++) {
        // throw st;
        // throw st.add(timePerPatient, "seconds");
        if (index + 1 > slot.no_of_patients) {
          continue;
        }
        if (st >= slot_time && st < slot_next_time) {
        } else {
          slot_time.add(clinicDoctor.slot_time, "minutes");
          slot_next_time.add(clinicDoctor.slot_time, "minutes");
        }
        tokens.push({
          time     : st.format("HH:mm:ss"),
          is_booked: false,
          token_no : index + 1,
          slot_time: slot_time.format("HH:mm:ss")
        });
      }
    }
    for (const bookedToken of booked) {
      const indexOf                = tokens.indexOf(tokens.filter(i => i.time === bookedToken.time)[0]);
      tokens[indexOf].is_booked    = true;
      tokens[indexOf].patient_name = bookedToken.patient_name;
      tokens[indexOf].patient_id   = bookedToken.patient_id;
    }
    return tokens;
  }

  async createToken(data: TokenCreateDto, patient_id: string, patient_name: string, bookie: string, transaction?: Transaction): Promise<Token> {
    return await Token.create({
      ...data,
      patient_id  : patient_id,
      patient_name: patient_name,
      booked_by   : Bookie.FRONT_DESK,
      booked_via  : bookie,
      booked_at   : moment.utc().format("YYYY-MM-DD HH:mm:ss")
    }, {transaction});
  }

  async createTokenByDoctor(data: TokenCreateDto, patient_id: string, patient_name: string, bookie: string, transaction?: Transaction): Promise<Token> {
    return await Token.create({
      ...data,
      patient_id  : patient_id,
      patient_name: patient_name,
      booked_by   : Bookie.DOCTOR,
      booked_via  : bookie,
    }, {transaction});
  }

  async bookMyToken(data: TokenCreateDto, transaction?: Transaction): Promise<Token> {
    return await Token.create({...data, booked_by: Bookie.PATIENT, booked_via: data.patient_name}, {transaction});
  }

  async bookTokenByEmployee(data: TokenCreateDto, bookie: string, transaction?: Transaction): Promise<Token> {
    return await Token.create({
      ...data,
      booked_by : Bookie.EMPLOYEE,
      booked_via: bookie,
      booked_at : moment.utc().format("YYYY-MM-DD HH:mm:ss")
    }, {transaction});
  }

  async syncTokens(offlineTokens: TokenCreateDto[], onlineTokens: Token[], clinicDoctor: ClinicDoctor, transaction?: Transaction): Promise<Token[]> {
    await this.bulkDelete(onlineTokens, transaction);
    const offlineGoneOnline: Token[] = [];
    for (const offlineToken of offlineTokens) {
      offlineGoneOnline.push(await this.createTokenByDoctor(offlineToken, offlineToken.patient_id, offlineToken.patient_name, clinicDoctor.doctor_name, transaction));
    }
    const newTokens: Token[] = [];
    for (const onlineToken of onlineTokens) {
      const tokensThatDay = await this.showUnbookedTokens(onlineToken.clinic_doctor, onlineToken.date.toString());
      let previous        = tokensThatDay.find(o => o.time === onlineToken.time);
      let previousIndex   = tokensThatDay.indexOf(previous);
      // throw previousIndex;
      let flag            = true;
      while (flag) {
        if (previousIndex >= tokensThatDay.length) {
          flag        = false;
          const visit = await PatientVisit.create({
            clinic_doctor_id: onlineToken.clinic_doctor_id,
            patient_id      : onlineToken.patient_id,
            patient_name    : onlineToken.patient_name,
            appointment_type: onlineToken.appointment_type,
            visit_type      : onlineToken.visit_type,
            booking_type    : BookingType.CANCELLED,
            booked_at       : moment.utc(onlineToken.createdAt).format(),
            appointment_time: moment.utc(onlineToken.date + " " + onlineToken.time).format(),
            updated_by      : Bookie.PATIENT,
            fees            : 0
          });
        } else {
          if (previous.is_booked === true) {
            const alreadyCreated = offlineGoneOnline.find(a => a.time === previous.time && a.date === onlineToken.date);
            if (alreadyCreated?.patient_id !== onlineToken?.patient_id) {
              // throw {a: previous.patient_id, b: alreadyCreated.patient_id};
              console.log("Case 1");
              previousIndex = previousIndex + 1;
              previous      = tokensThatDay[previousIndex];
            } else {
              console.log("Case 2");
              flag = false;
            }
          } else if (previous.is_booked === false) {
            console.log("Case 3");
            const token = await Token.create({
              clinic_doctor_id: onlineToken.clinic_doctor_id,
              patient_id      : onlineToken.patient_id,
              patient_name    : onlineToken.patient_name,
              appointment_type: onlineToken.appointment_type,
              visit_type      : onlineToken.visit_type,
              booking_type    : onlineToken.booking_type,
              date            : onlineToken.date,
              time            : previous.time,
              token_no        : previous.token_no,
              is_present      : onlineToken.is_present,
              fees            : onlineToken.fees,
              booked_at       : onlineToken.booked_at,
              booked_by       : onlineToken.booked_by,
              booked_via      : onlineToken.booked_via,
              present_time    : onlineToken.present_time
            }, {transaction});
            flag        = false;
            newTokens.push(token);
          }
        }
      }
    }
    return offlineGoneOnline;
  }


  async delete(token: Token): Promise<any> {
    await token.destroy();
  }

  async bulkDelete(tokens: Token[], transaction?: Transaction): Promise<any> {
    for (const token of tokens) {
      await token.destroy({transaction});
    }
  }

  async viewTokensOfEmployee(employee: Employee, filters: { start_date?: string; end_date?: string; query?: string }): Promise<Token[]> {
    let whereClause: any;
    if (filters.query) {
      whereClause = {
        patient_name: {
          [Op.like]: `%${filters.query}%`
        }
      };
    }
    return Token.findAll({
      where  : {
        booked_via: employee.first_name + " " + employee.last_name,
        booked_by : Bookie.EMPLOYEE,
        ...whereClause,
        date      : {
          [Op.between]: [
            filters.start_date ? filters.start_date : "2020-01-01",
            filters.end_date ? moment.utc(filters.end_date).add(1, "day").format("YYYY-MM-DD") : moment.utc().format("YYYY-MM-DD")
          ]
        }
      },
      order  : [
        [
          "date",
          "asc"
        ],
        [
          "time",
          "asc"
        ]
      ],
      include: [{model: ClinicDoctor, include: [Clinic]}]
    });
  }
}

export const tokenService = TokenService.getInstance();
