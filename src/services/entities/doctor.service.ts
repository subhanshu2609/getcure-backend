import logger from "../../util/logger.util";
import { DoctorSignupDto } from "../../dtos/doctor/doctor-signup.dto";
import { isUndefined } from "util";
import { DoctorUpdateDto } from "../../dtos/doctor/doctor-update.dto";
import { Helpers } from "../../util/helpers.util";
import { PreUser } from "../../models/pre-user.model";
import { Transaction } from "sequelize";
import { Doctor } from "../../models/doctor.model";
import { DoctorEntryDto } from "../../dtos/doctor/doctor-entry.dto";
import { Sequelize } from "sequelize";
import { ClinicDoctor } from "../../models/clinic-doctor.model";
import { Clinic } from "../../models/clinic.model";

class DoctorService {
  private readonly LIMIT = 20;

  private constructor() {
    logger.silly("[N-GC] DoctorService");
  }

  static getInstance(): DoctorService {
    return new DoctorService();
  }

  async preSignup(data: { mobile_no: string }): Promise<PreUser> {
    let token     = "123456";
    //       Helpers.generateRandomString(6, {
    //   includeLowerCase: false,
    //   includeNumbers  : true,
    //   includeUpperCase: false
    // });
    const preUser = await PreUser.findOne({
      where: {
        mobile_no: data.mobile_no
      }
    });
    if (data.mobile_no === "9711635385" || data.mobile_no === "9718529289" ||
      data.mobile_no === "9999999999" || data.mobile_no === "9958217724" ||
      data.mobile_no === "8605590316" || data.mobile_no === "8126555585" ||
      data.mobile_no === "9555437096" || data.mobile_no === "9818496835") {
      token = "123456";
    }
    if (preUser) {
      return preUser.update({otp: token});
    }
    return PreUser.create({
      mobile_no: data.mobile_no,
      otp      : token
    });
  }

  async addDoctor(data: DoctorEntryDto, transaction?: Transaction): Promise<Doctor> {
    const doctor = await Doctor.create(data, {transaction});
    return doctor.update({
      get_cure_code: "GCD" + (1000 + doctor.id),
    }, {transaction});
  }

  async create(data: DoctorSignupDto, transaction?: Transaction): Promise<Doctor> {
    return Doctor.create({
      ...data,
      is_verified: false
    });
  }

  async show(doctorId: number, withIncludes?: boolean): Promise<Doctor> {
    return Doctor.findOne({
      where: {
        id: doctorId
      },
      include: withIncludes ? [
        {
          model  : ClinicDoctor,
          include: [Clinic]
        }
      ] : []
    });
  }

  async showAll(): Promise<Doctor[]> {
    return Doctor.findAll({
      include: [Clinic]
    });
  }

  async index(filters: { query?: string }): Promise<Doctor[]> {
    let whereClause;
    if (filters.query) {
      whereClause = {
        [Sequelize.Op.or]: {
          name         : {
            like: "%" + filters.query + "%"
          },
          get_cure_code: {
            like: "%" + filters.query + "%"
          }
        }
      };
    } else {
      return [];
    }
    return Doctor.findAll({
      where : whereClause,
      order : [
        [
          "name",
          "asc"
        ]
      ],
      include: [Clinic],
      limit : 10,
      offset: 0
    });
  }

  async showByCity(filters: { state_id: string; query?: string }): Promise<ClinicDoctor[]> {
    let whereClause = {};
    if (filters.query) {
      whereClause = {
        ...whereClause,
      };
    }
    return ClinicDoctor.findAll({
      where  : {
        [Sequelize.Op.and]: [
          {
            ["$clinic.state_id$"]: filters.state_id,
          },
          {
            [Sequelize.Op.or]: {
              doctor_name              : {
                like: `%${filters.query}%`
              },
              ["$clinic.name$"]        : {
                like: `%${filters.query}%`
              },
              ["$doctor.specialities$"]: {
                [Sequelize.Op.like]: [`%${Helpers.titlecase(filters.query)}%`]
              }
            }
          }
        ]
      },
      include: [
        Clinic,
        Doctor
      ],
    });
  }

  async showDoctorByCode(doctor_code: string): Promise<Doctor> {
    return Doctor.findOne({
      where: {
        get_cure_code: doctor_code
      }
    });
  }

  async showUserByEmail(email: string, withIncludes?: boolean): Promise<Doctor> {
    return Doctor.findOne({
      where  : {
        email: email
      },
      include: withIncludes ? [
        {
          model  : ClinicDoctor,
          include: [Clinic]
        }
      ] : []
    });
  }

  async showUserByMobile(mobile: string, withIncludes?: boolean): Promise<Doctor> {
    return Doctor.findOne({
      where  : {
        mobile_no: mobile
      },
      include: withIncludes ? [
        {
          model  : ClinicDoctor,
          include: [Clinic]
        }
      ] : []
    });
  }

  async update(user: Doctor, data: DoctorUpdateDto): Promise<Doctor> {
    Helpers.removeUndefinedKeys(data);
    return user.update(data);
  }

  async delete(doctor: Doctor): Promise<any> {
    await doctor.destroy();
  }
}

export const doctorService = DoctorService.getInstance();
