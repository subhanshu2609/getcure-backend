import logger from "../../util/logger.util";
import { Helpers } from "../../util/helpers.util";
import { Speciality } from "../../models/speciality.model";
import { Transaction } from "sequelize";

class SpecialityService {
  constructor() {
    logger.silly("[N-GC] SpecialityService");
  }

  static getInstance(): SpecialityService {
    return new SpecialityService();
  }

  async index(filter: { query?: string }): Promise<Speciality[]> {
    let whereClause;
    if (filter.query) {
      whereClause = {
        ["title" as string]: {
          like: "%" + filter.query + "%"
        }
      };
    }
    return Speciality.findAll({
      where : whereClause,
      limit : 10,
      offset: 0
    });
  }

  async createSpeciality(title: string, transaction?: Transaction): Promise<Speciality> {
    return Speciality.create({title: Helpers.titlecase(title)}, {transaction});
  }

  async show(specialityId: number, withIncludes?: boolean): Promise<Speciality> {
    return Speciality.findOne({
      where: {
        id: specialityId
      }
    });
  }

  async updateSpeciality(speciality: Speciality, title: string): Promise<Speciality> {
    return speciality.update({title: title});
  }

  async delete(speciality: Speciality): Promise<any> {
    await speciality.destroy();
  }
}

export const specialityService = SpecialityService.getInstance();
