import logger from "../../util/logger.util";
import { Helpers } from "../../util/helpers.util";
import { Clinic } from "../../models/clinic.model";
import { ClinicUpdateDto } from "../../dtos/clinic/clinic-update.dto";
import { Transaction } from "sequelize";
import { ClinicDoctor } from "../../models/clinic-doctor.model";
import { TokenCreateDto } from "../../dtos/token/token-create.dto";
import { Token } from "../../models/token.model";
import { PatientCreateDto } from "../../dtos/patient/patient-create.dto";
import { Patient } from "../../models/patient.model";
import { PatientUpdateDto } from "../../dtos/patient/patient-update.dto";
import { Sequelize } from "sequelize";

class PatientService {
  constructor() {
    logger.silly("[N-GC] PatientService");
  }

  static getInstance(): PatientService {
    return new PatientService();
  }

  async show(patient_id: string, withIncludes?: boolean): Promise<Patient> {
    return Patient.findOne({
      where: {
        patient_id: patient_id
      }
    });
  }

  async showById(id: number, withIncludes?: boolean): Promise<Patient> {
    return Patient.findOne({
      where: {
        id: id
      }
    });
  }

  async index(filters: { query?: string }): Promise<Patient[]> {
    let whereClause = {};
    if (filters.query) {
      whereClause = {
        [Sequelize.Op.or]: [
          {
            name: {
              [Sequelize.Op.like]: `%${filters.query}%`
            }
          },
          {
            patient_id: {
              [Sequelize.Op.like]: `%${filters.query}%`
            }
          }
        ]
      };
    }
    return Patient.findAll({
      where: whereClause,
      order: [
        [
          "id",
          "desc"
        ]
      ]
    });
  }

  async showByMobileNumber(mobile_no: string, transaction?: Transaction): Promise<Patient[]> {
    return Patient.findAll({
      where: {
        mobile_no: mobile_no
      },
      transaction
    });
  }

  async createPatient(data: PatientCreateDto, transaction?: Transaction): Promise<Patient> {
    let unique_id    = "A" + data.mobile_no;
    const family     = await this.showFamilyPatient(data.mobile_no);
    const unique_ids = await this.showFamily(family);
    const names      = await family.map(f => f.name);
    if (names.indexOf(data.name) !== -1) {
      return family[names.indexOf(data.name)];
    }
    const length = unique_ids.length;
    const char   = unique_id.charCodeAt(0);
    unique_id    = unique_id.replace((String.fromCharCode(char)), (String.fromCharCode(char + length)));
    console.log(unique_id);
    return await Patient.create({
      ...data,
      name      : Helpers.titlecase(data.name),
      patient_id: unique_id
    }, {transaction});
  }

  async showFamily(patients: Patient[]): Promise<string[]> {
    const names: string[] = [];
    for (const patient of patients) {
      names.push(patient.patient_id);
    }
    console.log(names);
    return names;
  }

  async showFamilyPatient(mobile_no: string): Promise<Patient[]> {
    return Patient.findAll({
      where: {
        ["patient_id" as any]: {
          like: "%" + mobile_no
        }
      }
    });
  }

  async update(patient: Patient, data: PatientUpdateDto): Promise<Patient> {
    return patient.update(data);
  }
}

export const patientService = PatientService.getInstance();
