import logger from "../../util/logger.util";
import { Medicine } from "../../models/medicine.model";
import { MedicineCreateDto } from "../../dtos/medicine/medicine-create.dto";
import { MedicineUpdateDto } from "../../dtos/medicine/medicine-update.dto";
import { Variable } from "../../models/variable.model";
import { ParameterUpdateDto } from "../../dtos/medicine/parameter-update.dto";
import { MedicineParameter, MedicineUpdateParameters } from "../../enums/medicine-parameter.enum";
import { ClinicDoctor } from "../../models/clinic-doctor.model";
import { Doctor } from "../../models/doctor.model";
import { Sequelize } from "sequelize";

class MedicineService {
  constructor() {
    logger.silly("[N-GC] MedicineService");
  }

  static getInstance(): MedicineService {
    return new MedicineService();
  }

  async show(medicineId: number): Promise<Medicine> {
    return Medicine.findById(medicineId);
  }

  async viewAll(): Promise<Medicine[]> {
    return Medicine.findAll();
  }

  async viewByDoctor(doctorId: number): Promise<Medicine[]> {
    return Medicine.findAll({
      where: {
        doctor_id: doctorId
      },
      group: ["title"]
    });
  }

  async viewParameterByDoctor(doctorId: number): Promise<Variable[]> {
    return await Variable.findAll({where: {doctor_id: doctorId}, group: ["title"]});
  }

  async create(data: MedicineCreateDto): Promise<Medicine> {
    return Medicine.create(data);
  }

  async createParameter(doctor: Doctor, clinicDoctors: ClinicDoctor[], inputData: ParameterUpdateDto): Promise<string[]> {
    let cd;
    switch (inputData.type) {
      case MedicineUpdateParameters.CATEGORY:
        for (const clinicDoctor of clinicDoctors) {
          cd = await clinicDoctor.update({
            medicine_categories: clinicDoctor.medicine_categories.concat(inputData.name)
          });
        }
        await Variable.create({
          doctor_id: doctor.id,
          title    : inputData.name,
          value    : inputData.value,
          type     : MedicineUpdateParameters.CATEGORY
        });
        return cd.medicine_categories;

      case MedicineUpdateParameters.DOSE:
        for (const clinicDoctor of clinicDoctors) {
          cd = await clinicDoctor.update({
            medicine_doses: clinicDoctor.medicine_doses.concat(inputData.name)
          });
        }
        await Variable.create({
          doctor_id: clinicDoctors[0].doctor_id,
          title    : inputData.name,
          value    : inputData.value,
          type     : MedicineParameter.DOSE
        });
        return cd.medicine_doses;

      case MedicineUpdateParameters.UNIT:
        for (const clinicDoctor of clinicDoctors) {
          cd = await clinicDoctor.update({
            medicine_units: clinicDoctor.medicine_units.concat(inputData.name)
          });
        }
        await Variable.create({
          doctor_id: doctor.id,
          title    : inputData.name,
          value    : inputData.value,
          type     : MedicineUpdateParameters.UNIT
        });
        return cd.medicine_units;

      case MedicineUpdateParameters.ROUTE:
        for (const clinicDoctor of clinicDoctors) {
          cd = await clinicDoctor.update({
            medicine_routes: clinicDoctor.medicine_routes.concat(inputData.name)
          });
        }
        await Variable.create({
          doctor_id: doctor.id,
          title    : inputData.name,
          value    : inputData.value,
          type     : MedicineUpdateParameters.ROUTE
        });
        return cd.medicine_routes;

      case MedicineUpdateParameters.FREQUENCY:
        for (const clinicDoctor of clinicDoctors) {
          cd = await clinicDoctor.update({
            medicine_frequencies: clinicDoctor.medicine_frequencies.concat(inputData.name)
          });
        }
        await Variable.create({
          doctor_id: clinicDoctors[0].doctor_id,
          title    : inputData.name,
          value    : inputData.value,
          type     : MedicineParameter.FREQUENCY
        });
        return cd.medicine_frequencies;

      case MedicineUpdateParameters.DIRECTION:
        for (const clinicDoctor of clinicDoctors) {
          cd = await clinicDoctor.update({
            medicine_directions: clinicDoctor.medicine_directions.concat(inputData.name)
          });
        }
        await Variable.create({
          doctor_id: doctor.id,
          title    : inputData.name,
          value    : inputData.value,
          type     : MedicineUpdateParameters.DIRECTION
        });
        return cd.medicine_directions;

      case MedicineUpdateParameters.DURATION:
        for (const clinicDoctor of clinicDoctors) {
          cd = await clinicDoctor.update({
            medicine_durations: clinicDoctor.medicine_durations.concat(inputData.name)
          });
        }
        await Variable.create({
          doctor_id: clinicDoctors[0].doctor_id,
          title    : inputData.name,
          value    : inputData.value,
          type     : MedicineParameter.DURATION
        });
        return cd.medicine_durations;
    }
  }

  async bulkCreate(data: MedicineCreateDto[]): Promise<Medicine[]> {
    const medicines: Medicine[] = [];
    for (const single of data) {
      const medicine = await Medicine.create(single);
      medicines.push(medicine);
    }
    return medicines;
  }

  async update(medicine: Medicine, data: MedicineUpdateDto): Promise<Medicine> {
    return medicine.update(data);
  }

  async delete(medicine: Medicine): Promise<any> {
    return medicine.destroy();
  }
}

export const medicineService = MedicineService.getInstance();
