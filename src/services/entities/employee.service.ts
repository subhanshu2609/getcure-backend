import logger from "../../util/logger.util";
import { Employee } from "../../models/employee.model";
import { EmployeeCreateDto } from "../../dtos/employee/employee-create.dto";
import { EmployeeUpdateDto } from "../../dtos/employee/employee-update.dto";
import { Transaction } from "sequelize";
import { ClinicDoctor } from "../../models/clinic-doctor.model";
import { CdEmployee } from "../../models/cd-employee.model";
import { clinicService } from "./clinic.service";
import { ClinicNotFoundException } from "../../exceptions/clinic/clinic-not-found.exception";
import { Clinic } from "../../models/clinic.model";
import { Doctor } from "../../models/doctor.model";
import { Sequelize } from "sequelize";

class EmployeeService {
  constructor() {
    logger.silly("[N-GC] EmployeeService");
  }

  static getInstance(): EmployeeService {
    return new EmployeeService();
  }

  async show(employeeId: number): Promise<Employee> {
    return Employee.findById(employeeId);
  }

  async showByMobile(mobile_no: string): Promise<Employee> {
    return Employee.findOne({
      where: {
        mobile_no: mobile_no
      }
    });
  }

  async index(): Promise<Employee[]> {
    return Employee.findAll();
  }

  async create(data: EmployeeCreateDto, transaction?: Transaction): Promise<Employee> {
    return Employee.create(data, {transaction});
  }

  async update(employee: Employee, data: EmployeeUpdateDto, transaction?: Transaction): Promise<Employee> {
    return employee.update(data, {transaction});
  }

  async delete(employee: Employee): Promise<any> {
    return employee.destroy();
  }

  async showCdEmployeeByCdAndEmployee(clinicDoctorId: number, employeeId: number): Promise<CdEmployee> {
    return CdEmployee.findOne({
      where: {
        clinic_doctor_id: clinicDoctorId,
        employee_id     : employeeId
      }
    });
  }

  async createCdEmployee(clinic_ids: number[], doctor_ids: number[], employee_id: number, transaction?: Transaction): Promise<CdEmployee[]> {
    const clinicDoctors: ClinicDoctor[] = [];
    if (clinic_ids.length > 0) {
      for (const clinicId of clinic_ids) {
        const cds = await clinicService.showAllClinicDoctorByClinic(clinicId);
        cds.forEach((cd) => clinicDoctors.push(cd));
      }
    }
    if (doctor_ids.length > 0) {
      for (const doctorId of doctor_ids) {
        const cds = await clinicService.showAllClinicDoctorByDoctor(doctorId);
        cds.forEach((cd) => clinicDoctors.push(cd));
      }
    }
    const clinicDoctorIds = clinicDoctors.map(cd => cd.id)
      .filter((value, index, self) => self.indexOf(value) === index);

    const cdEmployee: CdEmployee[] = [];
    for (const clinicDoctorId of clinicDoctorIds) {
      const oldObject = await CdEmployee.findOne({
        where: {
          employee_id     : employee_id,
          clinic_doctor_id: clinicDoctorId
        }
      });
      if (!oldObject) {
        cdEmployee.push(await CdEmployee.create({
          employee_id     : employee_id,
          clinic_doctor_id: clinicDoctorId
        }, {transaction}));
      } else cdEmployee.push(oldObject);
    }
    return cdEmployee;
  }

  async showClinicDoctorsByEmployee(employee: Employee, filters: { state_id?: number, query?: string }): Promise<ClinicDoctor[]> {
    let whereClause: any;
    if (filters.state_id) {
      whereClause = {
        ["$clinic.state_id$"]: +filters.state_id
      };
    }
    if (filters.query) {
      whereClause = {
        ...whereClause,
        [Sequelize.Op.or]: [
          {
            ["$doctor.name$"]: {
              like: "%" + filters.query + "%"
            }
          },
          {
            ["$clinic.name$"]: {
              like: "%" + filters.query + "%"
            }
          }
        ]
      };
    }
    return await ClinicDoctor.findAll({
      where  : whereClause,
      include: [
        {
          model: CdEmployee,
          as   : "cdEmployees",
          required: true,
          where: {
            employee_id: employee.id
          }
        },
        Doctor,
        Clinic
      ],
      order  : [
        [
          "doctor_name",
          "asc"
        ]
      ]
    });
  }

  async deleteCdEmployee(cdEmployee: CdEmployee) {
    await cdEmployee.destroy();
  }
}

export const employeeService = EmployeeService.getInstance();
