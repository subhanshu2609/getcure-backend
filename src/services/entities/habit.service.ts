import logger from "../../util/logger.util";
import { Habit } from "../../models/habit.model";
import { HabitCreateDto } from "../../dtos/habit/habit-create.dto";
import { HabitType } from "../../enums/symptom.enum";
import { HabitUpdateDto } from "../../dtos/habit/habit-update.dto";
import { Helpers } from "../../util/helpers.util";

class HabitService {

  constructor() {
    logger.silly("[N-GC] HabitService");
  }

  static getInstance(): HabitService {
    return new HabitService();
  }

  async show(habitId: number): Promise<Habit> {
    return Habit.findOne({
      where: {
        id: habitId
      }
    });
  }

  async showDoctorHabits(doctorId: number): Promise<Habit[]> {
    return Habit.findAll({
      where: {
        doctor_id: doctorId
      },
      group: ["title"],
      order: [
        [
          "title",
          "asc"
        ]
      ]
    });
  }

  async showDoctorAllergies(doctorId: number): Promise<Habit[]> {
    return Habit.findAll({
      where: {
        doctor_id: doctorId,
        type     : HabitType.ALLERGY
      },
      group: ["title"],
      order: [
        [
          "title",
          "asc"
        ]
      ]
    });
  }

  async showDoctorLifestyle(doctorId: number): Promise<Habit[]> {
    return Habit.findAll({
      where: {
        doctor_id: doctorId,
        type     : HabitType.LIFESTYLE
      },
      group: ["title"],
      order: [
        [
          "title",
          "asc"
        ]
      ]
    });
  }

  async createAllergy(data: HabitCreateDto): Promise<Habit> {
    return Habit.create({
      ...data,
      title: Helpers.titlecase(data.title),
      type : HabitType.ALLERGY
    });
  }

  async createLifestyle(data: HabitCreateDto): Promise<Habit> {
    return Habit.create({
      ...data,
      title: Helpers.titlecase(data.title),
      type : HabitType.LIFESTYLE
    });
  }

  async bulkCreate(data: HabitCreateDto[]): Promise<Habit[]> {
    const habits: Habit[] = [];
    for (const habit of data) {
      const newHabit = await Habit.create({
        ...habit,
        title: Helpers.titlecase(habit.title)
      });
      habits.push(newHabit);
    }
    return habits;
  }

  async update(habit: Habit, data: HabitUpdateDto): Promise<Habit> {
    return habit.update(data);
  }

  async delete(habit: Habit): Promise<any> {
    return habit.destroy();
  }
}

export const habitService = HabitService.getInstance();
