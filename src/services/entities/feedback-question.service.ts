import logger from "../../util/logger.util";
import { FeedbackQuestion } from "../../models/feedback-question.model";
import { FeedbackQuestionCreateDto } from "../../dtos/feedback/feedback-question-create.dto";
import { FeedbackQuestionUpdateDto } from "../../dtos/feedback/feedback-question-update.dto";

class FeedbackQuestionService {
  constructor() {
    logger.silly("[N-GC] FeedbackQuestionService");
  }

  static getInstance(): FeedbackQuestionService {
    return new FeedbackQuestionService();
  }

  async show(feedbackQuestionId: number): Promise<FeedbackQuestion> {
    return FeedbackQuestion.findById(feedbackQuestionId);
  }

  async index(): Promise<FeedbackQuestion[]> {
    return FeedbackQuestion.findAll({where: {is_active: true}});
  }

  async viewAll(): Promise<FeedbackQuestion[]> {
    return FeedbackQuestion.findAll();
  }

  async create(data: FeedbackQuestionCreateDto): Promise<FeedbackQuestion> {
    return FeedbackQuestion.create(data);
  }

  async update(feedbackQuestion: FeedbackQuestion, data: FeedbackQuestionUpdateDto): Promise<FeedbackQuestion> {
    return feedbackQuestion.update(data);
  }

  async delete(feedbackQuestion: FeedbackQuestion): Promise<any> {
    return feedbackQuestion.destroy();
  }
}

export const feedbackQuestionService = FeedbackQuestionService.getInstance();
