import logger from "../../util/logger.util";
import { PatientVisit } from "../../models/patient-visit.model";
import { Op } from "sequelize";
import moment from "moment";
import { ClinicDoctor } from "../../models/clinic-doctor.model";
import { PatientVisitCreateDto } from "../../dtos/patint-visit/patient-visit-create.dto";
import { Token } from "../../models/token.model";
import { Clinic } from "../../models/clinic.model";
import { State } from "../../models/state.model";
import { Bookie } from "../../enums/bookie.enum";
import app from "../../app";
import { Employee } from "../../models/employee.model";

class PatientVisitService {
  constructor() {
    logger.silly("[N-GC] PatientVisitService");
  }

  static getInstance(): PatientVisitService {
    return new PatientVisitService();
  }

  async show(patientVisitId: number): Promise<PatientVisit> {
    return PatientVisit.findById(patientVisitId);
  }

  async showByAppointmentClinicDoctorAndPatient(patientId: string, clinicDoctorId: number, appointmentTime: string): Promise<PatientVisit> {
    const time = appointmentTime.split(" ");
    return PatientVisit.findOne({
      where: {
        patient_id      : patientId,
        clinic_doctor_id: clinicDoctorId,
        appointment_time: time[0] + "T" + time[1] + ".000Z"
      }
    });
  }

  async viewAll(date?: string): Promise<PatientVisit[]> {
    let whereClause: any;
    if (date) {
      whereClause = {
        appointment_time: {
          [Op.between]: [
            date,
            moment.utc(date).add(1, "days").format("YYYY-MM-DD")
          ]
        }
      };
    }
    return PatientVisit.findAll({
      where  : whereClause,
      include: [
        {
          model  : ClinicDoctor,
          include: [
            {
              model: Clinic,
            }
          ]
        }
      ],
      order  : [
        [
          "appointment_time",
          "desc"
        ]
      ]
    });
  }

  async viewByClinicDoctor(clinicDoctorId: number, filters: { start_date?: string; end_date?: string; query?: string }): Promise<PatientVisit[]> {
    let whereClause: any;
    if (filters.query) {
      whereClause = {
        patient_name: {
          [Op.like]: `%${filters.query}%`
        }
      };
    }
    return PatientVisit.findAll({
      where: {
        clinic_doctor_id: clinicDoctorId,
        appointment_time: {
          [Op.between]: [
            filters.start_date ? filters.start_date : "2020-01-01",
            filters.end_date
              ? moment.utc(filters.end_date).add(1, "days").format("YYYY-MM-DD")
              : moment.utc().add(1, "days").format("YYYY-MM-DD")
          ]
        },
        ...whereClause
      },
      order: [["appointment_time", "desc"]]
    });
  }

  async viewByClinicDoctors(clinicDoctorIds: number[], filters: { start_date?: string; end_date?: string; }): Promise<PatientVisit[]> {
    return PatientVisit.findAll({
      where: {
        clinic_doctor_id: clinicDoctorIds,
        appointment_time: {
          [Op.between]: [
            filters.start_date ? filters.start_date : "2020-01-01",
            filters.end_date
              ? moment.utc(filters.end_date).add(1, "days").format("YYYY-MM-DD")
              : moment.utc().add(1, "days").format("YYYY-MM-DD")
          ]
        }
      },
      order: [["appointment_time", "desc"]]
    });
  }


  async create(data: PatientVisitCreateDto): Promise<PatientVisit> {
    return PatientVisit.create({
      ...data,
      booked_by: Bookie.DOCTOR
    });
  }

  //
  // async update(patientVisit: PatientVisit, data: PatientVisitUpdateDto): Promise<PatientVisit> {
  //   return patientVisit.update(data);
  // }

  async delete(patientVisit: PatientVisit): Promise<any> {
    return patientVisit.destroy();
  }

  async viewPatientVisitsOfEmployee(employee: Employee, filters: {start_date?: string; end_date?: string; query?: string}): Promise<PatientVisit[]> {
    let whereClause: any;
    if (filters.query) {
      whereClause = {
        patient_name: {
          [Op.like]: `%${filters.query}%`
        }
      };
    }
    return PatientVisit.findAll({
      where: {
        booked_via: employee.first_name + " " + employee.last_name,
        booked_by: Bookie.EMPLOYEE,
        ...whereClause,
        appointment_time: {
          [Op.between]: [
            filters.start_date ? filters.start_date : "2020-01-01",
            filters.end_date ? moment.utc(filters.end_date).add(1, "day").format("YYYY-MM-DD") : moment.utc().format("YYYY-MM-DD")
          ]
        }
      },
      order: [["appointment_time", "asc"]],
      include: [{model: ClinicDoctor, include: [Clinic]}]
    });
  }
}

export const patientVisitService = PatientVisitService.getInstance();
