import logger from "../../util/logger.util";
import { Examination } from "../../models/examinations.model";
import { ExaminationCreateDto } from "../../dtos/examination/examination-create.dto";
import { ExaminationUpdateDto } from "../../dtos/examination/examination-update.dto";
import { SymptomCreateDto } from "../../dtos/symptom/symptom-create.dto";
import { Symptom } from "../../models/symptom.model";
import { Helpers } from "../../util/helpers.util";

class ExaminationService {
  constructor() {
    logger.silly("[N-GC] ExaminationService");
  }

  static getInstance(): ExaminationService {
    return new ExaminationService();
  }

  async show(examinationId: number): Promise<Examination> {
    return Examination.findById(examinationId);
  }

  async showByDoctorId(doctorId: number): Promise<Examination[]> {
    return Examination.findAll({
      where: {
        doctor_id: doctorId
      },
      group: ["title"]
    });
  }

  async create(data: ExaminationCreateDto): Promise<Examination> {
    return Examination.create(data);
  }

  async bulkCreate(data: ExaminationCreateDto[]): Promise<Examination[]> {
    const examinations: Examination[] = [];
    for (const examination of data) {
      const newExamination = await Examination.create({
        ...examination,
        title: Helpers.titlecase(examination.title)
      });
      examinations.push(newExamination);
    }
    return examinations;
  }

  async update(data: ExaminationUpdateDto, examination: Examination): Promise<Examination> {
    return examination.update(data);
  }

  async delete(examination: Examination): Promise<any> {
    return examination.destroy();
  }
}

export const examinationService = ExaminationService.getInstance();
