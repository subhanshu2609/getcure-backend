import logger from "../../util/logger.util";
import { Helpers } from "../../util/helpers.util";
import { ClinicCreateDto } from "../../dtos/clinic/clinic-create.dto";
import { Clinic } from "../../models/clinic.model";
import { ClinicUpdateDto } from "../../dtos/clinic/clinic-update.dto";
import { Transaction } from "sequelize";
import { ClinicDoctor } from "../../models/clinic-doctor.model";
import { Doctor } from "../../models/doctor.model";
import { ClinicDoctorCreateDto } from "../../dtos/doctor/clinic-doctor-create.dto";
import { ClinicDoctorTimingUpdateDto, ClinicDoctorUpdateDto } from "../../dtos/doctor/clinic-doctor-update.dto";
import { Timing } from "../../models/timing.model";
import { FdCd } from "../../models/fd-cd.model";

class ClinicService {
  constructor() {
    logger.silly("[N-GC] ClinicService");
  }

  static getInstance(): ClinicService {
    return new ClinicService();
  }

  async index(filters: { query?: string }): Promise<Clinic[]> {
    let whereClause;
    if (filters.query) {
      whereClause = {
        ["name" as string]: {
          like: "%" + filters.query + "%"
        }
      };
    }
    return Clinic.findAll({
      where: whereClause
    });
  }

  async showByState(stateId: number, filters: { query?: string }): Promise<Clinic[]> {
    let whereClause;
    if (filters.query) {
      whereClause = {
        ["name" as string]: {
          like: "%" + filters.query + "%"
        }
      };
    }
    return Clinic.findAll({
      where: {
        ...whereClause,
        state_id: stateId
      }
    });
  }

  async createClinic(data: ClinicCreateDto, transaction?: Transaction): Promise<Clinic> {
    const clinic = await Clinic.create(data, {transaction});
    return clinic.update({
      get_cure_code: "GCC" + (1000 + clinic.id)
    }, {transaction});
  }

  async show(clinicId: number, withIncludes?: boolean): Promise<Clinic> {
    return Clinic.findOne({
      where: {
        id: clinicId
      }
    });
  }

  async showClinicByCode(clinic_code: string, withIncludes?: boolean): Promise<Clinic> {
    return Clinic.findOne({
      where: {
        get_cure_code: clinic_code
      }
    });
  }

  async showClinicDoctor(clinic_id: number, doctor_id: number, withIncludes?: boolean): Promise<ClinicDoctor> {
    return ClinicDoctor.findOne({
      where: {
        clinic_id: clinic_id,
        doctor_id: doctor_id
      }
    });
  }

  async showClinicDoctorById(clinicDoctorId: number, withIncludes?: boolean): Promise<ClinicDoctor> {
    return ClinicDoctor.findOne({
      where  : {
        id: clinicDoctorId
      },
      include: withIncludes ? [Clinic] : [Doctor]
    });
  }

  async showAllClinicDoctorByDoctor(doctorId: number, withIncludes?: boolean): Promise<ClinicDoctor[]> {
    return ClinicDoctor.findAll({
      where  : {
        doctor_id: doctorId
      },
      include: [Clinic]
    });
  }

  async showAllClinicDoctorByClinic(clinicId: number, withIncludes?: boolean): Promise<ClinicDoctor[]> {
    return ClinicDoctor.findAll({
      where  : {
        clinic_id: clinicId
      },
      include: [Doctor]
    });
  }

  async showClinicDoctorByClinicIdAndFrontDesk(clinicId: number, frontDeskId: number, withIncludes?: boolean): Promise<ClinicDoctor[]> {
    const fdCd = await FdCd.findAll({
      where: {
        front_desk_id: frontDeskId
      }
    });
    return ClinicDoctor.findAll({
      where: {
        clinic_id    : clinicId,
        id: fdCd.map(f => f.clinic_doctor_id)
      }
    });
  }

  async showClinicDoctorByFrontDesk(frontDeskId: number, withIncludes?: boolean): Promise<ClinicDoctor[]> {
    const fdCd = await FdCd.findAll({
      where: {
        front_desk_id: frontDeskId
      }
    });
    return ClinicDoctor.findAll({
      where  : {
        id: fdCd.map(f => f.clinic_doctor_id)
      },
      include: [Doctor]
    });
  }

  async createClinicDoctor(clinic_id: number, doctor_id: number, name: string, transaction?: Transaction): Promise<ClinicDoctor> {
    return ClinicDoctor.create({
      clinic_id  : clinic_id,
      doctor_id  : doctor_id,
      doctor_name: name
    }, {transaction});
  }

  async addClinicDoctor(data: ClinicDoctorCreateDto, transaction?: Transaction): Promise<ClinicDoctor> {
    return ClinicDoctor.create(data, {transaction, include: [Doctor]});
  }

  async updateClinicDoctor(clinicDoctor: ClinicDoctor, data: ClinicDoctorUpdateDto): Promise<ClinicDoctor> {
    return clinicDoctor.update(data);
  }

  async updateClinicDoctorTiming(clinicDoctor: ClinicDoctor, data: Timing[]): Promise<ClinicDoctor> {
    return clinicDoctor.update({
      doctor_timings: data
    });
  }

  async updateClinicDoctorFees(clinicDoctor: ClinicDoctor, data: {consultation_fee?: number; emergency_fee?: number}): Promise<ClinicDoctor> {
    return clinicDoctor.update({
      emergency_fee: data.emergency_fee ? data.emergency_fee : clinicDoctor.emergency_fee,
      consultation_fee: data.consultation_fee ? data.consultation_fee : clinicDoctor.consultation_fee,
    });
  }

  async update(clinic: Clinic, data: ClinicUpdateDto): Promise<Clinic> {
    Helpers.removeUndefinedKeys(data);
    return clinic.update(data);
  }

  async delete(clinic: Clinic): Promise<any> {
    await clinic.destroy();
  }

  async deleteClinicDoctor(clinicDoctor: ClinicDoctor): Promise<any> {
    await clinicDoctor.destroy();
  }
}

export const clinicService = ClinicService.getInstance();
