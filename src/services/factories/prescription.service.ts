import logger from "../../util/logger.util";
import { PatientVisit } from "../../models/patient-visit.model";
import * as fs from "fs";
import * as path from "path";
import appRoot from "app-root-path";
import * as handlebars from "handlebars";

class PrescriptionService {
  constructor() {
    logger.silly("[N-GC] PrescriptionService");
  }

  static getInstance(): PrescriptionService {
    return new PrescriptionService();
  }

  async generatePdf(patientVisit: PatientVisit) {
    const templateHtml = fs.readFileSync(path.join(appRoot.path, "public/letter-head.hbs"), "utf8");
    const template     = handlebars.compile(templateHtml, {
      explicitPartialContext: true,
      strict                : false
    });

    const html = template({
      order: "Hello"
    }, {
      allowProtoMethodsByDefault   : true,
      allowProtoPropertiesByDefault: true
    });
    return html;
  }

}

export const prescriptionService = PrescriptionService.getInstance();
