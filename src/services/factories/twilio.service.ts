// getting ready
import logger from "../../util/logger.util";
import { UnprocessableEntityException } from "../../exceptions/root/unprocessable-entity.exception";
import { Twilio } from "twilio";

const twilioNumber = "+12059973544";
const accountSid   = "ACaeacfe14e945646de1c0c8f70248c7a1";
const authToken    = "b3876bb7ad5ae4f72caffe10e9b4c4eb";


// start sending message

class TwilioService {
  constructor() {
    logger.silly("[N-GC] TwilioService");
  }


  static getInstance(): TwilioService {
    return new TwilioService();
  }

  async sendText(phoneNumber: string, otp: string) {
    const client = new Twilio(accountSid, authToken);

    const number = "+91" + phoneNumber;
    console.log(number);
    const valid = await /^\+?[1-9]\d{1,14}$/.test(number);
    if (!valid) {
      throw new UnprocessableEntityException("number must be E164 format!", "Enter Valid Number");
    }

    const textContent = {
      body: `You have successfully registered for GetCure. Your OTP is ${otp}`,
      to  : number,
      from: twilioNumber
    };

    client.messages.create(textContent)
      .then(message => console.log(message.to));
  }

  validE164(num: string) {
    return /^\+?[1-9]\d{1,14}$/.test(num);
  }
}

export const twilioService = TwilioService.getInstance();
