import express from "express";
import cors from "cors";
import bodyParser from "body-parser";
import path from "path";
import * as _ from "lodash";
import { dbService } from "./services/db.service";
import { ENV_APP_PORT_REST } from "./util/secrets.util";
import { mattermostService } from "./services/mattermost.service";
import { errorHandler } from "./handlers/error-handler";
import { upload } from "./services/factories/multer.service";
import { doctorService } from "./services/entities/doctor.service";
import { DoctorController } from "./controllers/doctor.controller";
import { doctorMiddleware } from "./middlewares/doctor.middleware";
import { FrontDeskController } from "./controllers/front-desk.controller";
import { frontDeskMiddleware } from "./middlewares/front-desk.middleware";
import { ClinicController } from "./controllers/clinic.controller";
import { TokenController } from "./controllers/token.controller";
import { AddressController } from "./controllers/address.controller";
import { InvoiceController } from "./controllers/invoice.controller";
import { ExaminationController } from "./controllers/examination.controller";
import { SymptomController } from "./controllers/symptom.controller";
import { PatientController } from "./controllers/patient.controller";
import { patientMiddleware } from "./middlewares/patient.middleware";
import { EmployeeController } from "./controllers/employee.controller";
import { adminMiddleware } from "./middlewares/admin.middleware";
import { SpecialityController } from "./controllers/speciality.controller";
import { employeeMiddleware } from "./middlewares/employee.middleware";
import { FeedbackQuestionController } from "./controllers/feedback-question.controller";
import { FeedbackQuestion } from "./models/feedback-question.model";
import { FeedbackController } from "./controllers/feedback.controller";
import { MedicineController } from "./controllers/medicine.controller";
import { PatientVisitController } from "./controllers/patient-visit.controller";
import { HabitController } from "./controllers/habit.controller";
import { RecommendationController } from "./controllers/recommendation.controller";


// Create Express server
const app = express();

// Entities
doctorService;

// Factories
// cryptService;
// jwtService;
// s3Service;
// validatorService;
// snsService;

// Others
dbService;
mattermostService;

// Express configuration
app.set("port", process.env.PORT || ENV_APP_PORT_REST);
// app.use(snsHeaderMiddleware);
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

// CORS Setup
const allowedOrigins = [
  "http://localhost:3000",
  "http://localhost:3001",
  "https://master.dkoj3fcp70fiq.amplifyapp.com",
  "https://master.d3uj50etbyl1v7.amplifyapp.com"
];

app.use(cors({
  origin : (origin, callback) => {
    if (!origin || _.includes(allowedOrigins, origin)) {
      callback(undefined, true);
    } else {
      callback(new Error("Not allowed by CORS"));
    }
  },
  methods: [
    "GET",
    "HEAD",
    "PUT",
    "PATCH",
    "POST",
    "DELETE"
  ]
}));
app.options("*");

// Static Public Content
app.use("/public", express.static("./public", {maxAge: 31557600000}));

// Global Middleware(s)

/**
 * Primary app routes.
 */

// AUTH
app.post("/generate-otp", errorHandler(DoctorController.generateOtp));
app.post("/register", errorHandler(DoctorController.signup));
app.post("/login-doctor", errorHandler(DoctorController.loginDoctor));
app.post("/login-front-desk", errorHandler(FrontDeskController.loginFrontDesk));
app.post("/login-patient", errorHandler(PatientController.authenticate));
app.post("/login-employee", errorHandler(EmployeeController.loginEmployee));


// ADMIN
app.post("/examinations", errorHandler(ExaminationController.addExamination));
app.get("/patient-visits", [adminMiddleware], errorHandler(PatientVisitController.viewPatientVisit));
app.get("/all-tokens", [adminMiddleware], errorHandler(TokenController.viewAllTokens));

// EMPLOYEE
app.get("/employees", [adminMiddleware], errorHandler(EmployeeController.showEmployees));
app.get("/employee/:employeeId([0-9]+)", [adminMiddleware], errorHandler(EmployeeController.showEmployee));
app.get("/cd-employee/:employeeId([0-9]+)", [adminMiddleware], errorHandler(EmployeeController.viewClinicDoctors));
app.post("/employees", [adminMiddleware], errorHandler(EmployeeController.createEmployee));
app.put("/employee/:employeeId([0-9]+)", [adminMiddleware], errorHandler(EmployeeController.updateEmployee));
app.delete("/employee/:employeeId([0-9]+)/clinic-doctor/:clinicDoctorId([0-9]+)/unlink", [adminMiddleware], errorHandler(EmployeeController.unlinkClinicDoctors));
app.put("/employee/me", [employeeMiddleware], errorHandler(EmployeeController.updateMe));
app.delete("/employee/:employeeId([0-9]+)", [adminMiddleware], errorHandler(EmployeeController.deleteEmployee));


// PATIENT
app.get("/patients", [adminMiddleware], errorHandler(PatientController.indexPatients));
app.get("/patients/doctor", [doctorMiddleware], errorHandler(PatientController.indexPatients));
app.get("/patients/front-desk", [frontDeskMiddleware], errorHandler(PatientController.indexPatients));
app.get("/patients/employee", [employeeMiddleware], errorHandler(PatientController.indexPatients));
app.post("/signup-patient", errorHandler(PatientController.createPatient));
app.post("/patient-otp", errorHandler(PatientController.generateOtp));
app.post("/add-patient", [patientMiddleware], errorHandler(PatientController.addPatient));
app.post("/patient/bulk-create", [doctorMiddleware], errorHandler(PatientController.addBulkPatient));
app.put("/patient/:patientId([0-9]+)", [patientMiddleware], errorHandler(PatientController.updatePatient));
app.get("/upcoming-appointments", [patientMiddleware], errorHandler(PatientController.upcomingAppointments));
app.get("/previous-appointments", [patientMiddleware], errorHandler(PatientController.previousAppointments));


// SPECIALITY
app.get("/specialities", errorHandler(SpecialityController.indexSpeciality));
app.post("/specialities", errorHandler(SpecialityController.createSpeciality));
app.put("/speciality/:specialityId([0-9]+)", [adminMiddleware], errorHandler(SpecialityController.updateSpeciality));
app.delete("/speciality/:specialityId([0-9]+)", [adminMiddleware], errorHandler(SpecialityController.deleteSpeciality));


// DOCTOR
app.get("/doctors", errorHandler(DoctorController.indexDoctors));
app.get("/all-doctors", [adminMiddleware], errorHandler(DoctorController.showAllDoctors));
app.get("/my-doctor-profile", [doctorMiddleware], errorHandler(DoctorController.showMyDoctorProfile));
app.post("/doctors", [adminMiddleware], errorHandler(DoctorController.createDoctor));
app.put("/doctor/:doctorId([0-9]+)", [adminMiddleware], errorHandler(DoctorController.updateDoctor));
app.put("/update-doctor/:doctorId([0-9]+)", [doctorMiddleware], errorHandler(DoctorController.updateDoctor));
app.put("/doctor/:doctorId([0-9]+)/front-desk", [frontDeskMiddleware], errorHandler(DoctorController.updateDoctorByFD));
app.put("/clinic-doctors/:doctorId([0-9]+)", [adminMiddleware], errorHandler(DoctorController.updateClinicDoctor));
app.put("/clinic-doctors/:doctorId([0-9]+)/doctor", [doctorMiddleware], errorHandler(DoctorController.updateClinicDoctor));
app.put("/my-doctor-profile", [doctorMiddleware], errorHandler(DoctorController.updateMyDoctorProfile));
app.put("/my-fees/:clinicDoctorId([0-9]+)", [doctorMiddleware], errorHandler(DoctorController.updateMyClinicDoctorFees));
app.put("/my-clinic-timing/:clinicDoctorId([0-9]+)", [doctorMiddleware], errorHandler(DoctorController.updateMyClinicTimings));
app.put("/doctor-timing/:clinicDoctorId([0-9]+)", [frontDeskMiddleware], errorHandler(DoctorController.updateClinicTimings));
app.delete("/my-doctor-profile", [doctorMiddleware], errorHandler(DoctorController.deleteMyDoctorProfile));
app.delete("/clinic-doctor/:doctorId([0-9]+)", [adminMiddleware], errorHandler(DoctorController.deleteDoctorFromClinic));

app.post("/image-upload/:type", [
  adminMiddleware,
  upload.single("image")
], errorHandler(DoctorController.uploadImage));

app.post("/document-upload/:type", [
  doctorMiddleware,
  upload.single("image")
], errorHandler(DoctorController.uploadImage));

// CLINIC
app.post("/add-clinic", [doctorMiddleware], errorHandler(ClinicController.addClinic));
app.post("/add-clinic-by-code", [doctorMiddleware], errorHandler(ClinicController.addClinicByCode));
app.get("/clinic/:clinicId([0-9]+)", [adminMiddleware], errorHandler(ClinicController.showClinic));
app.get("/clinics", [adminMiddleware], errorHandler(ClinicController.indexClinics));
app.get("/clinics-by-state/:stateId([0-9]+)", [doctorMiddleware], errorHandler(ClinicController.indexClinicsByState));
app.post("/clinics", [adminMiddleware], errorHandler(ClinicController.createClinic));
app.put("/clinic/:clinicId([0-9]+)", [adminMiddleware], errorHandler(ClinicController.updateClinic));
app.delete("/clinic/:clinicId([0-9]+)", [adminMiddleware], errorHandler(ClinicController.deleteClinic));


// CLINIC DOCTOR
app.get("/my-clinics/:doctorId([0-9]+)", [doctorMiddleware], errorHandler(ClinicController.clinicsByDoctor));
app.get("/clinic-doctor/:doctorId([0-9]+)", [adminMiddleware], errorHandler(ClinicController.showCDOfDoctor));
app.get("/clinic-doctor/:doctorId([0-9]+)/:clinicId([0-9]+)", [adminMiddleware], errorHandler(ClinicController.showCDOfDoctor));
app.get("/clinic-doctors/:clinicId([0-9]+)", [adminMiddleware], errorHandler(ClinicController.doctorsOfClinic));
app.get("/clinic-doctors/:clinicId([0-9]+)/doctors", [patientMiddleware], errorHandler(ClinicController.doctorsOfClinic));
app.get("/clinic-doctors-by-state", errorHandler(DoctorController.showByState));
app.get("/employee/clinic-doctors", [employeeMiddleware], errorHandler(EmployeeController.clinicDoctorsOfEmployee));
app.get("/front-desk/clinic-doctors", [frontDeskMiddleware], errorHandler(FrontDeskController.showClinicDoctorsByFrontDesk));
app.put("/clinic-doctor/:clinicDoctorId([0-9]+)/more", [adminMiddleware], errorHandler(ClinicController.instantUpdate));

// EXAMINATION
app.get("/examinations", [doctorMiddleware], errorHandler(ExaminationController.showExaminations));
app.get("/admin/examinations/:doctorId([0-9]+)", [adminMiddleware], errorHandler(ExaminationController.showExaminationsByDoctor));
app.post("/examinations", [doctorMiddleware], errorHandler(ExaminationController.addExamination));
app.post("/admin/examinations", [adminMiddleware], errorHandler(ExaminationController.addExamination));
app.post("/examinations/bulk-create", [doctorMiddleware], errorHandler(ExaminationController.bulkCreateExaminations));
app.put("/examinations/:examinationId([0-9]+)", [doctorMiddleware], errorHandler(ExaminationController.updateExamination));
app.put("/admin/examinations/:examinationId([0-9]+)", [adminMiddleware], errorHandler(ExaminationController.updateExamination));
app.delete("/examinations/:examinationId([0-9]+)", [doctorMiddleware], errorHandler(ExaminationController.deleteExamination));
app.delete("/admin/examinations/:examinationId([0-9]+)", [adminMiddleware], errorHandler(ExaminationController.deleteExamination));


// SYMPTOM
app.get("/symptoms", [doctorMiddleware], errorHandler(SymptomController.showSymptoms));
app.post("/symptom", [doctorMiddleware], errorHandler(SymptomController.addSymptom));
app.post("/symptoms/bulk-create", [doctorMiddleware], errorHandler(SymptomController.bulkCreateSymptom));
app.delete("/symptom/:symptomId([0-9]+)", [doctorMiddleware], errorHandler(SymptomController.deleteSymptom));
app.get("/symptoms/doctor/:doctorId([0-9]+)", [adminMiddleware], errorHandler(SymptomController.showAllSymptom));
app.post("/symptom/add", [adminMiddleware], errorHandler(SymptomController.addSymptom));
app.put("/symptom/:symptomId([0-9]+)/update", [adminMiddleware], errorHandler(SymptomController.updateSymptom));
app.delete("/symptom/:symptomId([0-9]+)/delete", [adminMiddleware], errorHandler(SymptomController.deleteSymptom));


// FRONT DESK
app.get("/my-front-desk", [frontDeskMiddleware], errorHandler(FrontDeskController.showMyFrontDeskProfile));
app.get("/front-desks/:clinicId([0-9]+)", [adminMiddleware], errorHandler(FrontDeskController.showFrontDesksOfClinic));
app.get("/front-desks", [adminMiddleware], errorHandler(FrontDeskController.showAllFrontDesks));
app.get("/front-desk/:fdId([0-9]+)", [adminMiddleware], errorHandler(FrontDeskController.showFrontDesk));
app.post("/front-desk", [adminMiddleware], errorHandler(FrontDeskController.createFrontDesk));
app.put("/update-front-desk/:fdId([0-9]+)", [adminMiddleware], errorHandler(FrontDeskController.updateFrontDesk));
app.put("/my-front-desk", [frontDeskMiddleware], errorHandler(FrontDeskController.updateMyFrontDeskProfile));
app.delete("/front-desk/:fdId([0-9]+)", [adminMiddleware], errorHandler(FrontDeskController.deleteFrontDesk));
app.put("/unlink-front-desk", [adminMiddleware], errorHandler(FrontDeskController.unlinkFrontDesk));


// TOKEN
app.post("/token/front-desk/book", [frontDeskMiddleware], errorHandler(TokenController.addToken));
app.post("/token/patient/book", [patientMiddleware], errorHandler(TokenController.bookToken));
app.post("/token/employee/book", [employeeMiddleware], errorHandler(TokenController.bookTokenByEmployee));
app.post("/token/doctor/book", [doctorMiddleware], errorHandler(TokenController.bookTokenByDoctor));
app.put("/token/:tokenId([0-9]+)/front-desk", [frontDeskMiddleware], errorHandler(TokenController.updateToken));
app.put("/token/:tokenId([0-9]+)/employee", [employeeMiddleware], errorHandler(TokenController.updateToken));
app.put("/token/:tokenId([0-9]+)/patient/cancel", [patientMiddleware], errorHandler(TokenController.cancelToken));
app.put("/token/:tokenId([0-9]+)/front-desk/cancel", [frontDeskMiddleware], errorHandler(TokenController.cancelTokenByFD));
app.put("/token/:tokenId([0-9]+)/employee/cancel", [frontDeskMiddleware], errorHandler(TokenController.cancelTokenByEmployee));
app.post("/sync-tokens/:clinicDoctorId([0-9]+)", [doctorMiddleware], errorHandler(TokenController.syncTokens));
// app.delete("/token", [frontDeskMiddleware], errorHandler(TokenController.deleteToken));
app.get("/tokens/:clinicDoctorId([0-9]+)", errorHandler(TokenController.showBookedTokensBetweenDates));
app.get("/booked-tokens/:clinicDoctorId([0-9]+)", errorHandler(TokenController.showTokensByDate));
app.get("/unbooked-tokens/:clinicDoctorId([0-9]+)", errorHandler(TokenController.showUnbookedTokens));


// ADDRESS
app.get("/states", errorHandler(AddressController.listStates));
app.get("/active-states", errorHandler(AddressController.listActiveStates));
app.post("/states", [adminMiddleware], errorHandler(AddressController.addState));
app.delete("/state/:stateId([0-9]+)", [adminMiddleware], errorHandler(AddressController.deleteState));
app.get("/cities/:stateId([0-9]+)", errorHandler(AddressController.listCities));
app.post("/cities", errorHandler(AddressController.addCity));


// INVOICE
app.get("/doctors-invoice/:clinicDoctorId([0-9]+)", [frontDeskMiddleware], errorHandler(InvoiceController.displayDoctorInvoice));
app.get("/doctors-invoice/:clinicDoctorId([0-9]+)/doctor", [doctorMiddleware], errorHandler(InvoiceController.displayDoctorInvoice));
app.get("/daily-invoice/:clinicDoctorId([0-9]+)", [frontDeskMiddleware], errorHandler(InvoiceController.displayDailyInvoice));
app.get("/daily-invoice/:clinicDoctorId([0-9]+)/doctor", [doctorMiddleware], errorHandler(InvoiceController.displayDailyInvoice));
app.get("/daily-invoice/:clinicDoctorId([0-9]+)/employee", [employeeMiddleware], errorHandler(InvoiceController.displayDailyInvoice));
app.get("/clinics-invoice/:clinicId([0-9]+)", [frontDeskMiddleware], errorHandler(InvoiceController.displayClinicInvoice));
app.get("/admin-invoice", [adminMiddleware], errorHandler(InvoiceController.adminInvoice));
app.get("/prescription/:patientVisitId([0-9]+)", [doctorMiddleware], errorHandler(InvoiceController.getPrescription));


// PATIENT VISIT
app.get("/visits/clinic-doctor/:clinicDoctorId([0-9]+)", [doctorMiddleware], errorHandler(PatientVisitController.viewClinicDoctorsVisits));
app.get("/patient-visits/:clinicDoctorId([0-9]+)/front-desk", [frontDeskMiddleware], errorHandler(PatientVisitController.viewClinicDoctorsVisits));
app.get("/patient-visits/:clinicDoctorId([0-9]+)/employee", [employeeMiddleware], errorHandler(PatientVisitController.viewClinicDoctorsVisits));
app.get("/previous-work-history", [employeeMiddleware], errorHandler(PatientVisitController.viewVisitsByEmployee));
app.get("/upcoming-work-history", [employeeMiddleware], errorHandler(TokenController.viewTokensByEmployee));
app.get("/cancelled-tokens/:clinicDoctorId([0-9]+)", [doctorMiddleware], errorHandler(TokenController.viewCancelledTokens));
app.post("/cancel-bulk-tokens/:clinicDoctorId([0-9]+)", [doctorMiddleware], errorHandler(TokenController.cancelBulkTokens));
app.post("/patient-visits/bulk-create", [doctorMiddleware], errorHandler(PatientVisitController.createBulkPatientVisits));
app.post("/attachments", [
  doctorMiddleware,
  upload.array("examination")
], errorHandler(PatientVisitController.uploadPatientVisitAttachments));


// FEEDBACK QUESTIONS
app.get("/feedback-questions", errorHandler(FeedbackQuestionController.indexQuestions));
app.get("/all-feedback-questions", [adminMiddleware], errorHandler(FeedbackQuestionController.viewAllQuestions));
app.post("/feedback-questions", [adminMiddleware], errorHandler(FeedbackQuestionController.createQuestion));
app.put("/feedback-question/:questionId([0-9]+)", [adminMiddleware], errorHandler(FeedbackQuestionController.updateQuestion));
app.delete("/feedback-question/:questionId([0-9]+)", [adminMiddleware], errorHandler(FeedbackQuestionController.deleteQuestion));


// FEEDBACK
app.get("/feedbacks/:clinicDoctorId([0-9]+)", [doctorMiddleware], errorHandler(FeedbackController.viewClinicDoctorFeedbacks));
app.get("/feedbacks/:patientVisitId([0-9]+)/employee", [employeeMiddleware], errorHandler(FeedbackController.viewPatientVisitFeedback));
app.post("/feedback/patient", [patientMiddleware], errorHandler(FeedbackController.createFeedback));
app.post("/feedback/employee", [employeeMiddleware], errorHandler(FeedbackController.createEmployeeFeedback));
app.post("/feedback/doctor", [doctorMiddleware], errorHandler(FeedbackController.createDoctorFeedback));
app.post("/sync/feedbacks", [doctorMiddleware], errorHandler(FeedbackController.syncDoctorFeedback));


// MEDICINE
app.get("/medicines/:doctorId([0-9]+)", errorHandler(MedicineController.viewDoctorMedicine));
app.get("/medicine-parameters/:doctorId([0-9]+)", errorHandler(MedicineController.viewDoctorMedicineParameter));
app.post("/medicines", [adminMiddleware], errorHandler(MedicineController.createMedicine));
app.post("/doctor-medicines", [doctorMiddleware], errorHandler(MedicineController.createBulkMedicine));
app.post("/medicine-parameters/:doctorId([0-9]+)/bulk", [doctorMiddleware], errorHandler(MedicineController.bulkCreateParams));
app.put("/medicine/:medicineId([0-9]+)", [adminMiddleware], errorHandler(MedicineController.updateMedicine));
app.put("/medicine-parameter/:doctorId([0-9]+)", [adminMiddleware], errorHandler(MedicineController.updateMedicineParam));
app.put("/medicine-parameter/:doctorId([0-9]+)/doctor", [doctorMiddleware], errorHandler(MedicineController.updateMedicineParam));
app.delete("/medicine/:medicineId([0-9]+)", [adminMiddleware], errorHandler(MedicineController.deleteMedicine));

// HABIT
app.get("/allergies/doctor/:doctorId([0-9]+)", errorHandler(HabitController.showAllergies));
app.get("/lifestyles/doctor/:doctorId([0-9]+)", errorHandler(HabitController.showLifestyles));
app.get("/habits/:doctorId([0-9]+)", errorHandler(HabitController.showHabits));
app.post("/habits", errorHandler(HabitController.bulkCreateHabits));
app.post("/allergy", errorHandler(HabitController.createAllergy));
app.post("/lifestyle", errorHandler(HabitController.createLifestyle));
app.put("/habit/:habitId([0-9]+)", [adminMiddleware], errorHandler(HabitController.updateHabit));
app.delete("/habit/:habitId([0-9]+)", [adminMiddleware], errorHandler(HabitController.deleteHabit));


// ANALYSIS
app.get("/disease-analysis/:doctorId([0-9]+)", errorHandler(RecommendationController.diseaseAnalysis));
app.get("/medicine-analysis/:doctorId([0-9]+)", errorHandler(RecommendationController.medicineAnalysis));
app.get("/patient-feedback/:doctorId([0-9]+)", errorHandler(RecommendationController.patientFeedbackAnalysis));


app.get("*", (req, res) => {
  res.send({data: "Works"});
});


export default app;
