import {
  AfterDelete,
  AutoIncrement, BeforeCreate, BelongsTo, BelongsToMany,
  Column,
  DataType, Default, ForeignKey, HasMany, HasOne,
  Model,
  PrimaryKey,
  Table,
  Unique
} from "sequelize-typescript";
import { Helpers } from "../util/helpers.util";
import { ClinicDoctor } from "./clinic-doctor.model";
import { PatientVisit } from "./patient-visit.model";
import { isNullOrUndefined } from "util";
import { AppointmentTypes } from "../enums/token.enum";
import { FeedbackFiller } from "../enums/feedback-filler.enum";
import { PrescribedMedicine } from "./about-patient.model";

@Table({
  timestamps: true,
  tableName : "feedbacks"
})
export class Feedback extends Model<Feedback> {
  @Unique
  @PrimaryKey
  @AutoIncrement
  @Column(DataType.BIGINT)
  id: number;

  @ForeignKey(() => ClinicDoctor)
  @Column(DataType.BIGINT)
  clinic_doctor_id: number;

  @ForeignKey(() => PatientVisit)
  @Column(DataType.BIGINT)
  patient_visit_id: number;

  @Column(DataType.STRING)
  question: string;

  @Column(DataType.STRING)
  option: string;

  @Column(DataType.ENUM({values: Helpers.iterateEnum<FeedbackFiller>(FeedbackFiller)}))
  filler: FeedbackFiller;

  @Column({
    type: DataType.JSON,
    set : function (this: Feedback, value: PrescribedMedicine[]) {
      this.setDataValue("medicine", JSON.stringify(value || []));
    },
    get : function (this: Feedback) {
      const value = this.getDataValue("medicine");
      if (isNullOrUndefined(value)) {
        return [];
      }
      return JSON.parse(value);
    }
  })
  medicine: PrescribedMedicine[];

  @BelongsTo(() => PatientVisit)
  patientVisit: PatientVisit;

}
