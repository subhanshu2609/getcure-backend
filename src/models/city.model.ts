import {
  AfterDelete,
  AutoIncrement, BeforeCreate, BelongsTo, BelongsToMany,
  Column,
  DataType, Default, ForeignKey, HasMany, HasOne,
  Model,
  PrimaryKey,
  Table,
  Unique
} from "sequelize-typescript";
import { Helpers } from "../util/helpers.util";
import { State } from "./state.model";

@Table({
  timestamps: true,
  paranoid  : true,
  tableName : "cities"
})
export class City extends Model<City> {
  @Unique
  @PrimaryKey
  @AutoIncrement
  @Column(DataType.BIGINT)
  id: number;

  @Unique
  @Column(DataType.STRING)
  title: string;

  @Unique
  @Column(DataType.STRING)
  slug: string;

  @ForeignKey(() => State)
  @Column(DataType.BIGINT)
  state_id: number;

  @Default(true)
  @Column(DataType.BOOLEAN)
  is_active: boolean;

  @BeforeCreate
  static addSlug(instance: City) {
    instance.slug = Helpers.slugify(instance.title);
  }

  // @AfterDelete
  // static deleteImage(instance: Address) {
  //   fs.unlinkSync(instance.image_url);
  // }

}
