import {
  AfterDelete,
  AutoIncrement, BeforeCreate, BelongsTo, BelongsToMany,
  Column,
  DataType, Default, ForeignKey, HasMany, HasOne,
  Model,
  PrimaryKey,
  Table,
  Unique
} from "sequelize-typescript";
import { Helpers } from "../util/helpers.util";
import { Clinic } from "./clinic.model";
import { Speciality } from "./speciality.model";
import { ClinicDoctor } from "./clinic-doctor.model";
import { Employee } from "./employee.model";
import { Doctor } from "./doctor.model";
import { PatientVisit } from "./patient-visit.model";

@Table({
  timestamps: true,
  paranoid  : false,
  tableName : "visit_attachments"
})
export class VisitAttachment extends Model<VisitAttachment> {
  @Unique
  @PrimaryKey
  @AutoIncrement
  @Column(DataType.BIGINT)
  id: number;

  @ForeignKey(() => ClinicDoctor)
  @Column(DataType.BIGINT)
  clinic_doctor_id: number;

  @ForeignKey(() => Doctor)
  @Column(DataType.BIGINT)
  doctor_id: number;

  @ForeignKey(() => PatientVisit)
  @Column(DataType.BIGINT)
  patient_visit_id: number;

  @Column(DataType.STRING)
  appointment_time: string;

  @Column(DataType.STRING)
  attachment_url: string;

  @BelongsTo(() => ClinicDoctor)
  clinicDoctor: ClinicDoctor;

  @BelongsTo(() => Doctor)
  doctor: Doctor;

  @BelongsTo(() => PatientVisit)
  patientVisit: PatientVisit;

}
