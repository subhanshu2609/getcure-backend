import {
  AutoIncrement, BelongsTo, BelongsToMany,
  Column,
  DataType, Default, ForeignKey, HasMany, HasOne,
  Model,
  PrimaryKey,
  Table,
  Unique
} from "sequelize-typescript";
import { genSaltSync, hashSync } from "bcrypt";
import { FrontDesk } from "./front-desk.model";
import { Clinic } from "./clinic.model";
import { ClinicDoctor } from "./clinic-doctor.model";
import { isNullOrUndefined } from "util";
import { Verification } from "./about-patient.model";

@Table({
  timestamps: true,
  paranoid  : false,
  tableName : "doctors"
})
export class Doctor extends Model<Doctor> {
  @Unique
  @PrimaryKey
  @AutoIncrement
  @Column(DataType.BIGINT)
  id: number;

  @Column(DataType.STRING)
  name: string;

  @Column(DataType.INTEGER)
  age: number;

  @Column(DataType.STRING)
  gender: string;

  @Column(DataType.STRING)
  degree: string;

  @Unique
  @Column(DataType.STRING)
  email: string;

  @Column({
    type: DataType.STRING,
    set : function (this: Doctor, value: string) {
      this.setDataValue("password", hashSync(value, genSaltSync(2)));
    }
  })
  password: string;

  @Unique
  @Column(DataType.STRING)
  mobile_no: string;

  @Column({
    type: DataType.JSON,
    set : function (this: Doctor, value: string[]) {
      this.setDataValue("specialities", JSON.stringify(value || []));
    },
    get : function (this: Clinic) {
      const value = this.getDataValue("specialities");
      if (isNullOrUndefined(value)) {
        return [];
      }
      return JSON.parse(value);
    }
  })
  specialities: string[];

  @Column(DataType.STRING)
  language: string;

  @Column(DataType.STRING)
  experience?: string;

  @Column(DataType.STRING)
  designation?: string;

  @Column(DataType.STRING)
  image_url?: string;

  @Column(DataType.BOOLEAN)
  is_active: boolean;

  @Column(DataType.BIGINT)
  get_cure_code: number;

  @Column({
    type: DataType.JSON,
    set : function (this: ClinicDoctor, value: Date[]) {
      this.setDataValue("holidays", JSON.stringify(value || []));
    },
    get : function (this: ClinicDoctor) {
      const value = this.getDataValue("holidays");
      if (isNullOrUndefined(value)) {
        return [];
      }
      return JSON.parse(value);
    }
  })
  holidays?: Date[];

  @Default([])
  @Column(DataType.JSON)
  identity_verification_url?: Verification[];

  @BelongsToMany(() => Clinic, () => ClinicDoctor)
  clinics?: Clinic[];

  @HasMany(() => ClinicDoctor)
  clinicDoctor?: ClinicDoctor[];
}

