import {
  AllowNull,
  AutoIncrement, BeforeCreate, BelongsTo, BelongsToMany,
  Column,
  DataType, Default, ForeignKey, HasMany, HasOne,
  Model,
  PrimaryKey,
  Table,
  Unique
} from "sequelize-typescript";
import { Helpers } from "../util/helpers.util";
import { ClinicDoctor } from "./clinic-doctor.model";
import { AppointmentTypes, BookingType, VisitType } from "../enums/token.enum";
import { Patient } from "./patient.model";
import { Bookie } from "../enums/bookie.enum";

@Table({
  timestamps: true,
  paranoid  : false,
  tableName : "tokens"
})
export class Token extends Model<Token> {
  @Unique
  @PrimaryKey
  @AutoIncrement
  @Column(DataType.BIGINT)
  id: number;

  @Column(DataType.STRING)
  appointment_id: string;

  @ForeignKey(() => ClinicDoctor)
  @Column(DataType.BIGINT)
  clinic_doctor_id: number;

  @ForeignKey(() => Patient)
  @Column(DataType.STRING)
  patient_id: string;

  @Column(DataType.STRING)
  patient_name: string;

  @Column(DataType.ENUM({values: Helpers.iterateEnum<AppointmentTypes>(AppointmentTypes)}))
  appointment_type: AppointmentTypes;

  @Column(DataType.ENUM({values: Helpers.iterateEnum<VisitType>(VisitType)}))
  visit_type: VisitType;

  @Column(DataType.ENUM({values: Helpers.iterateEnum<BookingType>(BookingType)}))
  booking_type: BookingType;

  @Column(DataType.DATEONLY)
  date: Date;

  @Column(DataType.TIME)
  time: string;

  @Column(DataType.INTEGER)
  token_no: number;

  @Column(DataType.BOOLEAN)
  is_present: boolean;

  @Column(DataType.FLOAT)
  fees: number;

  @Column(DataType.DATE)
  booked_at: Date;

  @Column(DataType.ENUM({values: Helpers.iterateEnum<Bookie>(Bookie)}))
  booked_by: Bookie;

  @Column(DataType.STRING)
  booked_via: string;

  @Column(DataType.DATE)
  present_time: Date;

  @AllowNull(true)
  @Column(DataType.STRING)
  transaction_id?: string;

  @BelongsTo(() => ClinicDoctor)
  clinic_doctor: ClinicDoctor;

  @BelongsTo(() => Patient, {targetKey: "patient_id"})
  patient: Patient;
}
