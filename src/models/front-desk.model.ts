import {
  AutoIncrement, BelongsTo, BelongsToMany,
  Column,
  DataType, Default, ForeignKey, HasMany, HasOne,
  Model,
  PrimaryKey,
  Table,
  Unique
} from "sequelize-typescript";
import { Clinic } from "./clinic.model";
import { genSaltSync, hashSync } from "bcrypt";
import { ClinicDoctor } from "./clinic-doctor.model";
import { FdCd } from "./fd-cd.model";

@Table({
  timestamps: true,
  paranoid  : false,
  tableName : "front_desks"
})
export class FrontDesk extends Model<FrontDesk> {
  @Unique
  @PrimaryKey
  @AutoIncrement
  @Column(DataType.BIGINT)
  id: number;

  @Column(DataType.STRING)
  name?: string;

  @Unique
  @Column(DataType.STRING)
  email?: string;

  @Unique
  @Column(DataType.STRING)
  mobile_no: string;

  @Column({
    type: DataType.STRING,
    set : function (this: FrontDesk, value: string) {
      this.setDataValue("password", hashSync(value, genSaltSync(2)));
    }
  })
  password: string;

  @Column(DataType.STRING)
  gender?: string;

  @ForeignKey(() => Clinic)
  @Column(DataType.BIGINT)
  clinic_id: number;

  @Column(DataType.STRING)
  image_url?: string;

  @Default(1)
  @Column(DataType.BOOLEAN)
  is_active: boolean;

  @BelongsToMany(() => ClinicDoctor, () => FdCd)
  clinicDoctors: ClinicDoctor[];

  @BelongsTo(() => Clinic)
  clinic: Clinic;
}
