import {
  AfterDelete,
  AutoIncrement, BeforeCreate, BelongsTo, BelongsToMany,
  Column,
  DataType, Default, ForeignKey, HasMany, HasOne,
  Model,
  PrimaryKey,
  Table,
  Unique
} from "sequelize-typescript";
import { Helpers } from "../util/helpers.util";
import { ClinicDoctor } from "./clinic-doctor.model";
import { PatientVisit } from "./patient-visit.model";
import { isNullOrUndefined } from "util";
import { AppointmentTypes } from "../enums/token.enum";
import { FeedbackFiller } from "../enums/feedback-filler.enum";
import { PrescribedMedicine } from "./about-patient.model";
import { Doctor } from "./doctor.model";
import { MedicineParameter, MedicineUpdateParameters } from "../enums/medicine-parameter.enum";

@Table({
  timestamps: true,
  tableName : "variables"
})
export class Variable extends Model<Variable> {
  @Unique
  @PrimaryKey
  @AutoIncrement
  @Column(DataType.BIGINT)
  id: number;

  @ForeignKey(() => Doctor)
  @Column(DataType.BIGINT)
  doctor_id: number;

  @Column(DataType.STRING)
  title: string;

  @Column(DataType.INTEGER)
  value: number;

  @Column(DataType.ENUM({values: Helpers.iterateEnum<MedicineUpdateParameters>(MedicineUpdateParameters)}))
  type: MedicineUpdateParameters;
}
