import { DoseTimeEnum, ExaminationStats } from "../enums/dose-time.enum";
import { ExaminationParameterEnum } from "../enums/examination-parameter.enum";
import { bool } from "aws-sdk/clients/signer";

export interface PatientBriefHistory {
  title: string;
  date: Date;
  visible_till: Date;
  is_cured: boolean;
}

export interface PatientVisitReason {
  title: string;
  date: Date;
  visible_till: Date;
  is_cured: boolean;
}

export interface PatientDiagnosis {
  title: string;
  date: Date;
  visible_till: Date;
  is_cured: boolean;
}

export interface PatientExamination {
  examination_id: number;
  title: string;
  parameter: PatientParameter[];
  status: ExaminationStats;
}

export interface PatientParameter {
  title: string;
  type: ExaminationParameterEnum;
  references: string[];
  unit: string;
  bio_reference?: string;
  result?: string[];
  sample: string;
  method: string;
}


export interface Medication {
  disease: string;
  symptom_id?: number;
  medicines: PrescribedMedicine[];
}

export interface PrescribedMedicine {
  medicine_id?: number;
  title: string;
  dose: string;
  unit: string;
  route: string;
  frequency: string;
  direction: string;
  duration: string;
}

export interface Verification {
  type: string;
  url: string;
}
