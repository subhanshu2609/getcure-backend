import {
  AfterDelete,
  AutoIncrement, BeforeCreate, BelongsTo, BelongsToMany,
  Column,
  DataType, Default, ForeignKey, HasMany, HasOne,
  Model,
  PrimaryKey,
  Table,
  Unique
} from "sequelize-typescript";
import { Helpers } from "../util/helpers.util";
import { Clinic } from "./clinic.model";
import { Speciality } from "./speciality.model";
import { Doctor } from "./doctor.model";
import { isNullOrUndefined } from "util";
import { Timing } from "./timing.model";
import { FrontDesk } from "./front-desk.model";
import { ClinicDoctor } from "./clinic-doctor.model";
import { Parameter } from "./parameter.model";
import { HabitType, SymptomType } from "../enums/symptom.enum";

@Table({
  timestamps: true,
  paranoid  : false,
  tableName : "habits"
})
export class Habit extends Model<Habit> {
  @Unique
  @PrimaryKey
  @AutoIncrement
  @Column(DataType.BIGINT)
  id: number;

  @ForeignKey(() => ClinicDoctor)
  @Column(DataType.BIGINT)
  clinic_doctor_id?: number;

  @ForeignKey(() => Doctor)
  @Column(DataType.BIGINT)
  doctor_id: number;

  @Column(DataType.STRING)
  title: string;

  @Column(DataType.ENUM({values: Helpers.iterateEnum<SymptomType>(HabitType)}))
  type: HabitType;

  @Default(true)
  @Column(DataType.BOOLEAN)
  is_active: boolean;

  @BelongsTo(() => ClinicDoctor)
  clinicDoctor?: ClinicDoctor;

  @BelongsTo(() => Doctor)
  doctor: Doctor;

}
