import {
  AfterDelete,
  AutoIncrement, BeforeCreate, BelongsTo, BelongsToMany,
  Column,
  DataType, Default, ForeignKey, HasMany, HasOne,
  Model,
  PrimaryKey,
  Table,
  Unique
} from "sequelize-typescript";
import { Helpers } from "../util/helpers.util";
import { Clinic } from "./clinic.model";
import { ClinicSpeciality } from "./clinic-speciality.model";

@Table({
  timestamps: true,
  paranoid  : true,
  tableName : "specialities"
})
export class Speciality extends Model<Speciality> {
  @Unique
  @PrimaryKey
  @AutoIncrement
  @Column(DataType.BIGINT)
  id: number;

  @Unique
  @Column(DataType.STRING)
  title: string;

  @Unique
  @Column(DataType.STRING)
  slug: string;

  @Default(true)
  @Column(DataType.BOOLEAN)
  is_active: boolean;

  @BelongsToMany(() => Clinic, () => ClinicSpeciality)
  clinics?: Clinic[];

  @BeforeCreate
  static addSlug(instance: Speciality) {
    instance.slug = Helpers.slugify(instance.title);
  }

  // @AfterDelete
  // static deleteImage(instance: Address) {
  //   fs.unlinkSync(instance.image_url);
  // }

}
