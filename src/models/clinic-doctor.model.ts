import {
  AfterDelete,
  AutoIncrement, BeforeCreate, BelongsTo, BelongsToMany,
  Column,
  DataType, Default, ForeignKey, HasMany, HasOne,
  Model,
  PrimaryKey,
  Table,
  Unique
} from "sequelize-typescript";
import { Helpers } from "../util/helpers.util";
import { Clinic } from "./clinic.model";
import { Speciality } from "./speciality.model";
import { Doctor } from "./doctor.model";
import { isNullOrUndefined } from "util";
import { Timing } from "./timing.model";
import { FrontDesk } from "./front-desk.model";
import { Employee } from "./employee.model";
import { CdEmployee } from "./cd-employee.model";
import { FdCd } from "./fd-cd.model";

@Table({
  timestamps: true,
  paranoid  : false,
  tableName : "clinics_doctors"
})
export class ClinicDoctor extends Model<ClinicDoctor> {
  @Unique
  @PrimaryKey
  @AutoIncrement
  @Column(DataType.BIGINT)
  id: number;

  @ForeignKey(() => Clinic)
  @Column(DataType.BIGINT)
  clinic_id: number;

  @ForeignKey(() => Doctor)
  @Column(DataType.BIGINT)
  doctor_id: number;

  @Column(DataType.STRING)
  doctor_name: string;

  @Column({
    type: DataType.JSON,
    set : function (this: ClinicDoctor, value: Timing[]) {
      this.setDataValue("doctor_timings", JSON.stringify(value || []));
    },
    get : function (this: ClinicDoctor) {
      const value = this.getDataValue("doctor_timings");
      if (isNullOrUndefined(value)) {
        return [];
      }
      return JSON.parse(value);
    }
  })
  doctor_timings?: Timing[];

  @ForeignKey(() => FrontDesk)
  @Column(DataType.BIGINT)
  front_desk_id?: number;

  @Column(DataType.FLOAT)
  consultation_fee?: number;

  @Column(DataType.FLOAT)
  emergency_fee: number;

  @Column(DataType.FLOAT)
  online_charge: number;

  @Column(DataType.FLOAT)
  paid_visit_charge?: number;

  @Column(DataType.FLOAT)
  free_visit_charge?: number;

  @Column(DataType.FLOAT)
  on_call_paid_visit_charge?: number;

  @Column(DataType.FLOAT)
  on_call_free_visit_charge?: number;

  @Column(DataType.INTEGER)
  follow_up_appointments?: number;

  @Column(DataType.INTEGER)
  follow_up_days?: number;

  @Column(DataType.INTEGER)
  slot_time: number;

  @Default([])
  @Column(DataType.JSON)
  medicine_categories?: string[];

  @Default([])
  @Column(DataType.JSON)
  medicine_doses?: string[];

  @Default([])
  @Column(DataType.JSON)
  medicine_units?: string[];

  @Default([])
  @Column(DataType.JSON)
  medicine_routes?: string[];

  @Default([])
  @Column(DataType.JSON)
  medicine_frequencies?: string[];

  @Default([])
  @Column(DataType.JSON)
  medicine_directions?: string[];

  @Default([])
  @Column(DataType.JSON)
  medicine_durations?: string[];

  @BelongsTo(() => Clinic)
  clinic: Clinic;

  @BelongsTo(() => Doctor)
  doctor: Doctor;

  @BelongsToMany(() => FrontDesk, () => FdCd)
  front_desk?: FrontDesk[];

  @HasMany(() => CdEmployee)
  cdEmployees?: CdEmployee[];
}
