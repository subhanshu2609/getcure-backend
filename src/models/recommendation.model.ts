import {
  AfterDelete,
  AutoIncrement, BeforeCreate, BelongsTo, BelongsToMany,
  Column,
  DataType, Default, ForeignKey, HasMany, HasOne,
  Model,
  PrimaryKey,
  Table,
  Unique
} from "sequelize-typescript";
import { Helpers } from "../util/helpers.util";
import { ClinicDoctor } from "./clinic-doctor.model";
import { PatientVisit } from "./patient-visit.model";
import { isNullOrUndefined } from "util";
import { AppointmentTypes } from "../enums/token.enum";
import { FeedbackFiller } from "../enums/feedback-filler.enum";
import { Medication, PrescribedMedicine } from "./about-patient.model";

@Table({
  timestamps: true,
  tableName : "recommendations"
})
export class Recommendation extends Model<Recommendation> {
  @Unique
  @PrimaryKey
  @AutoIncrement
  @Column(DataType.BIGINT)
  id: number;

  @ForeignKey(() => ClinicDoctor)
  @Column(DataType.BIGINT)
  clinic_doctor_id: number;

  @Column(DataType.STRING)
  disease: string;

  @Column({
    type: DataType.JSON,
    set : function (this: Recommendation, value: string[]) {
      this.setDataValue("medicines", JSON.stringify(value || []));
    },
    get : function (this: Recommendation) {
      const value = this.getDataValue("medicines");
      if (isNullOrUndefined(value)) {
        return [];
      }
      return JSON.parse(value);
    }
  })
  medicines: string[];

  @Default(1)
  @Column(DataType.BIGINT)
  total_count: number;

  @Default(0)
  @Column(DataType.BIGINT)
  cured: number;

  @Default(0)
  @Column(DataType.BIGINT)
  partially_cured: number;

  @Default(0)
  @Column(DataType.BIGINT)
  not_cured: number;

  @Default(0)
  @Column(DataType.BIGINT)
  symptoms_increased: number;
}
