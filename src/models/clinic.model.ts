import {
  AfterDelete,
  AutoIncrement, BeforeCreate, BelongsTo, BelongsToMany,
  Column,
  DataType, Default, ForeignKey, HasMany, HasOne,
  Model,
  PrimaryKey,
  Table,
  Unique
} from "sequelize-typescript";
import { Helpers } from "../util/helpers.util";
import { City } from "./city.model";
import { Doctor } from "./doctor.model";
import { ClinicDoctor } from "./clinic-doctor.model";
import { Speciality } from "./speciality.model";
import { ClinicSpeciality } from "./clinic-speciality.model";
import { Timing } from "./timing.model";
import { isNullOrUndefined } from "util";
import { State } from "./state.model";
import { FrontDesk } from "./front-desk.model";

@Table({
  timestamps: true,
  paranoid  : true,
  tableName : "clinics"
})
export class Clinic extends Model<Clinic> {
  @Unique
  @PrimaryKey
  @AutoIncrement
  @Column(DataType.BIGINT)
  id: number;

  @Column(DataType.STRING)
  name: string;

  @Column(DataType.STRING)
  type?: string;

  @Column(DataType.STRING)
  establishment_year?: string;

  @Column(DataType.INTEGER)
  no_of_beds?: number;

  @Column(DataType.INTEGER)
  no_of_doctors?: number;

  @Column(DataType.STRING)
  state: string;

  @ForeignKey(() => State)
  @Column(DataType.BIGINT)
  state_id?: number;

  @Column(DataType.STRING)
  timings: string;

  @Column(DataType.TEXT)
  about?: string;

  @Column({
    type: DataType.JSON,
    set : function (this: Clinic, value: string[]) {
      this.setDataValue("specialities", JSON.stringify(value || []));
    },
    get : function (this: Clinic) {
      const value = this.getDataValue("specialities");
      if (isNullOrUndefined(value)) {
        return [];
      }
      return JSON.parse(value);
    }
  })
  specialities: string[];

  @Column(DataType.STRING)
  address: string;

  @Column(DataType.STRING)
  pin_code?: string;

  @Column(DataType.STRING)
  phone_no?: string;

  @Column(DataType.STRING)
  latitude?: string;

  @Column(DataType.STRING)
  longitude?: string;

  @Column(DataType.STRING)
  get_cure_code?: string;

  @Column({
    type: DataType.JSON,
    set : function (this: Clinic, value: string[]) {
      this.setDataValue("image_urls", JSON.stringify(value || []));
    },
    get : function (this: Clinic) {
      const value = this.getDataValue("image_urls");
      if (isNullOrUndefined(value)) {
        return [];
      }
      return JSON.parse(value);
    }
  })
  image_urls?: string[];

  @Default(1)
  @Column(DataType.BOOLEAN)
  is_active: boolean;

  @BelongsToMany(() => Doctor, () => ClinicDoctor)
  doctors: Doctor[];


}
