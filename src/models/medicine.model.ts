import {
  AutoIncrement, BeforeCreate, BelongsTo, BelongsToMany,
  Column,
  DataType, Default, ForeignKey, HasMany, HasOne,
  Model,
  PrimaryKey,
  Table,
  Unique
} from "sequelize-typescript";
import { Doctor } from "./doctor.model";

@Table({
  timestamps: true,
  paranoid  : false,
  tableName : "medicines"
})
export class Medicine extends Model<Medicine> {
  @Unique
  @PrimaryKey
  @AutoIncrement
  @Column(DataType.BIGINT)
  id: number;

  @ForeignKey(() => Doctor)
  @Column(DataType.BIGINT)
  doctor_id: number;

  @Column(DataType.STRING)
  title: string;

  @Column(DataType.STRING)
  salt: string;

  @Default([])
  @Column(DataType.JSON)
  interaction_drugs: string[];

  @Column(DataType.STRING)
  category: string;

  @Column(DataType.STRING)
  default_dose: string;

  @Column(DataType.STRING)
  default_unit: string;

  @Column(DataType.STRING)
  default_route: string;

  @Column(DataType.STRING)
  default_frequency: string;

  @Column(DataType.STRING)
  default_direction: string;

  @Column(DataType.STRING)
  default_duration: string;

  @BelongsTo(() => Doctor)
  doctor: Doctor;
}
