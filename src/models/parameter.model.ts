import { ExaminationParameterEnum } from "../enums/examination-parameter.enum";

export interface Parameter {
  title: string;
  sample: string;
  method: string;
  type: ExaminationParameterEnum;
  references: string[];
  unit: string;
  bio_reference?: string[];
}
