import {
  AfterDelete,
  AutoIncrement, BeforeCreate, BelongsTo, BelongsToMany,
  Column,
  DataType, Default, ForeignKey, HasMany, HasOne,
  Model,
  PrimaryKey,
  Table,
  Unique
} from "sequelize-typescript";
import { Helpers } from "../util/helpers.util";
import { Clinic } from "./clinic.model";
import { Speciality } from "./speciality.model";
import { ClinicDoctor } from "./clinic-doctor.model";
import { Employee } from "./employee.model";

@Table({
  timestamps: true,
  paranoid  : false,
  tableName : "cd_employee"
})
export class CdEmployee extends Model<CdEmployee> {
  @Unique
  @PrimaryKey
  @AutoIncrement
  @Column(DataType.BIGINT)
  id: number;

  @ForeignKey(() => ClinicDoctor)
  @Column(DataType.BIGINT)
  clinic_doctor_id: number;

  @ForeignKey(() => Employee)
  @Column(DataType.BIGINT)
  employee_id: number;

  @BelongsTo(() => ClinicDoctor)
  clinicDoctor: ClinicDoctor;

  @BelongsTo(() => Employee)
  employee: Employee;


}
