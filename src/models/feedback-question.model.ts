import {
  AfterDelete,
  AutoIncrement, BeforeCreate, BelongsTo, BelongsToMany,
  Column,
  DataType, Default, ForeignKey, HasMany, HasOne,
  Model,
  PrimaryKey,
  Table,
  Unique
} from "sequelize-typescript";
import { Helpers } from "../util/helpers.util";

@Table({
  timestamps: true,
  tableName : "feedback_questions"
})
export class FeedbackQuestion extends Model<FeedbackQuestion> {
  @Unique
  @PrimaryKey
  @AutoIncrement
  @Column(DataType.BIGINT)
  id: number;

  @Column(DataType.STRING)
  title: string;

  @Column(DataType.JSON)
  options: string[];

  @Default(true)
  @Column(DataType.BOOLEAN)
  is_active: boolean;
}
