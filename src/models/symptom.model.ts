import {
  AfterDelete,
  AutoIncrement, BeforeCreate, BelongsTo, BelongsToMany,
  Column,
  DataType, Default, ForeignKey, HasMany, HasOne,
  Model,
  PrimaryKey,
  Table,
  Unique
} from "sequelize-typescript";
import { Helpers } from "../util/helpers.util";
import { Clinic } from "./clinic.model";
import { Speciality } from "./speciality.model";
import { Doctor } from "./doctor.model";
import { isNullOrUndefined } from "util";
import { Timing } from "./timing.model";
import { FrontDesk } from "./front-desk.model";
import { ClinicDoctor } from "./clinic-doctor.model";
import { Parameter } from "./parameter.model";
import { SymptomType } from "../enums/symptom.enum";
import { VisibilityEnum } from "../enums/visibility.enum";

@Table({
  timestamps: true,
  paranoid  : false,
  tableName : "symptoms"
})
export class Symptom extends Model<Symptom> {
  @Unique
  @PrimaryKey
  @AutoIncrement
  @Column(DataType.BIGINT)
  id: number;

  @ForeignKey(() => ClinicDoctor)
  @Column(DataType.BIGINT)
  clinic_doctor_id?: number;

  @ForeignKey(() => Doctor)
  @Column(DataType.BIGINT)
  doctor_id: number;

  @Column(DataType.STRING)
  title: string;

  @Column(DataType.ENUM({values: Helpers.iterateEnum<VisibilityEnum>(VisibilityEnum)}))
  visibility_period: VisibilityEnum;

  @Default(true)
  @Column(DataType.BOOLEAN)
  is_active: boolean;

  @BelongsTo(() => ClinicDoctor)
  clinicDoctor?: ClinicDoctor;

  @BelongsTo(() => Doctor)
  doctor: Doctor;

}
