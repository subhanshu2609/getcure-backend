import {
  AfterDelete,
  AutoIncrement, BeforeCreate, BelongsTo, BelongsToMany,
  Column,
  DataType, Default, ForeignKey, HasMany, HasOne,
  Model,
  PrimaryKey,
  Table,
  Unique
} from "sequelize-typescript";
import { Helpers } from "../util/helpers.util";
import { Clinic } from "./clinic.model";
import { Speciality } from "./speciality.model";
import { Doctor } from "./doctor.model";
import { isNullOrUndefined } from "util";
import { Timing } from "./timing.model";
import { ClinicDoctor } from "./clinic-doctor.model";

@Table({
  timestamps: true,
  paranoid  : false,
  tableName : "patients"
})
export class Patient extends Model<Patient> {
  @Unique
  @PrimaryKey
  @AutoIncrement
  @Column(DataType.BIGINT)
  id: number;

  @Column(DataType.STRING)
  name: string;

  @Column(DataType.INTEGER)
  age: number;

  @Column(DataType.STRING)
  mobile_no: string;

  @Column(DataType.STRING)
  gender: string;

  @Column(DataType.STRING)
  address: string;

  @PrimaryKey
  @Column(DataType.STRING)
  patient_id: string;

}
