import {
  AutoIncrement, BeforeCreate, BelongsTo, BelongsToMany,
  Column,
  DataType, Default, ForeignKey, HasMany, HasOne,
  Model,
  PrimaryKey,
  Table,
  Unique
} from "sequelize-typescript";
import { Doctor } from "./doctor.model";
import { isNullOrUndefined } from "util";
import { ClinicDoctor } from "./clinic-doctor.model";
import { Parameter } from "./parameter.model";

@Table({
  timestamps: true,
  paranoid  : false,
  tableName : "examinations"
})
export class Examination extends Model<Examination> {
  @Unique
  @PrimaryKey
  @AutoIncrement
  @Column(DataType.BIGINT)
  id: number;

  @ForeignKey(() => ClinicDoctor)
  @Column(DataType.BIGINT)
  clinic_doctor_id?: number;

  @ForeignKey(() => Doctor)
  @Column(DataType.BIGINT)
  doctor_id: number;

  @Column(DataType.STRING)
  title: string;

  @Column({
    type: DataType.JSON,
    set : function (this: Examination, value: Parameter[]) {
      this.setDataValue("parameters", JSON.stringify(value || []));
    },
    get : function (this: Examination) {
      const value = this.getDataValue("parameters");
      if (isNullOrUndefined(value)) {
        return [];
      }
      return JSON.parse(value);
    }
  })
  parameters: Parameter[];

  @Column(DataType.FLOAT)
  price?: number;

  @BelongsTo(() => ClinicDoctor)
  clinicDoctor?: ClinicDoctor;

  @BelongsTo(() => Doctor)
  doctor: Doctor;

}
