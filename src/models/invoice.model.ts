export interface DoctorInvoice {
  date: string;
  new_visits: number;
  follow_ups: number;
  emergencies: number;
  total_appointments: number;
  fees_collected: number;
  on_call: number;
}

export interface ClinicInvoice {
  doctor_name: string;
  new_visits: number;
  follow_ups: number;
  emergencies: number;
  total_appointments: number;
  fees_collected: number;
  on_call: number;
}

export interface DailyInvoice {
  booked: number;
  completed: number;
  on_line: number;
  front_desk: number;
  on_call: number;
  present: number;
}
