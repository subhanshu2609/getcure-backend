import { DaysEnum } from "../enums/days.enum";

export interface Timing {
  day: DaysEnum;
  slots: Slot[];
}

export interface Slot {
  start_time: string;
  end_time: string;
  no_of_patients: number;
}
