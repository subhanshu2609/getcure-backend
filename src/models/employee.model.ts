import {
  AutoIncrement,
  BelongsToMany,
  Column,
  DataType,
  Default, HasMany,
  Model,
  PrimaryKey,
  Table,
  Unique
} from "sequelize-typescript";
import { genSaltSync, hashSync } from "bcrypt";
import { Helpers } from "../util/helpers.util";
import { EmployeeType } from "../enums/employee-type.enum";
import { ClinicDoctor } from "./clinic-doctor.model";
import { CdEmployee } from "./cd-employee.model";

@Table({
  timestamps: true,
  paranoid  : false,
  tableName : "employees"
})
export class Employee extends Model<Employee> {
  @Unique
  @PrimaryKey
  @AutoIncrement
  @Column(DataType.BIGINT)
  id: number;

  @Column(DataType.STRING)
  first_name: string;

  @Column(DataType.STRING)
  last_name?: string;

  @Column({
    type: DataType.STRING,
    set : function (this: Employee, value: string) {
      this.setDataValue("password", hashSync(value, genSaltSync(2)));
    }
  })
  password: string;

  @Column(DataType.ENUM({values: Helpers.iterateEnum<EmployeeType>(EmployeeType)}))
  type: EmployeeType;

  @Column(DataType.STRING)
  aadhar_no?: string;

  @Unique
  @Column(DataType.STRING)
  mobile_no: string;

  @Column(DataType.STRING)
  gender: string;

  @Column(DataType.DATEONLY)
  dob: Date;

  @Column(DataType.STRING)
  profile_image?: string;

  @Column(DataType.STRING)
  address: string;

  @Column(DataType.STRING)
  pin_code: string;

  @Default(1)
  @Column(DataType.BOOLEAN)
  is_active: boolean;

  @BelongsToMany(() => ClinicDoctor, () => CdEmployee)
  clinicDoctors: ClinicDoctor[];
}
