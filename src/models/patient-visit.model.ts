import {
  AfterDelete,
  AutoIncrement, BeforeCreate, BelongsTo, BelongsToMany,
  Column,
  DataType, Default, ForeignKey, HasMany, HasOne,
  Model,
  PrimaryKey,
  Table,
  Unique
} from "sequelize-typescript";
import { Helpers } from "../util/helpers.util";
import { Clinic } from "./clinic.model";
import { Speciality } from "./speciality.model";
import { Doctor } from "./doctor.model";
import { isNullOrUndefined } from "util";
import { Timing } from "./timing.model";
import { ClinicDoctor } from "./clinic-doctor.model";
import { Patient } from "./patient.model";
import {
  Medication,
  PatientBriefHistory,
  PatientDiagnosis,
  PatientExamination,
  PatientVisitReason
} from "./about-patient.model";
import { AppointmentTypes, BookingType, VisitType } from "../enums/token.enum";
import { bool } from "aws-sdk/clients/signer";
import { Bookie } from "../enums/bookie.enum";
import { Feedback } from "./feedback.model";

@Table({
  timestamps: true,
  paranoid  : false,
  tableName : "patients_visits"
})
export class PatientVisit extends Model<PatientVisit> {
  @Unique
  @PrimaryKey
  @AutoIncrement
  @Column(DataType.BIGINT)
  id: number;

  @Column(DataType.STRING)
  appointment_id: string;

  @ForeignKey(() => ClinicDoctor)
  @Column(DataType.BIGINT)
  clinic_doctor_id: number;

  @ForeignKey(() => Patient)
  @Column(DataType.STRING)
  patient_id: string;

  @Column(DataType.STRING)
  patient_name: string;

  @Column(DataType.FLOAT)
  temperature: number;

  @Column(DataType.STRING)
  blood_pressure: string;

  @Column(DataType.FLOAT)
  pulse: number;

  @Column(DataType.FLOAT)
  weight: number;

  @Column({
    type: DataType.JSON,
    set : function (this: PatientVisit, value: PatientBriefHistory[]) {
      this.setDataValue("brief_history", JSON.stringify(value || []));
    },
    get : function (this: PatientVisit) {
      const value = this.getDataValue("brief_history");
      if (isNullOrUndefined(value)) {
        return [];
      }
      return JSON.parse(value);
    }
  })
  brief_history: PatientBriefHistory[];

  @Column({
    type: DataType.JSON,
    set : function (this: PatientVisit, value: PatientVisitReason[]) {
      this.setDataValue("visit_reason", JSON.stringify(value || []));
    },
    get : function (this: PatientVisit) {
      const value = this.getDataValue("visit_reason");
      if (isNullOrUndefined(value)) {
        return [];
      }
      return JSON.parse(value);
    }
  })
  visit_reason: PatientVisitReason[];

  @Column({
    type: DataType.JSON,
    set : function (this: PatientVisit, value: PatientExamination[]) {
      this.setDataValue("examination", JSON.stringify(value || []));
    },
    get : function (this: PatientVisit) {
      const value = this.getDataValue("examination");
      if (isNullOrUndefined(value)) {
        return [];
      }
      return JSON.parse(value);
    }
  })
  examination: PatientExamination[];

  @Column({
    type: DataType.JSON,
    set : function (this: PatientVisit, value: PatientDiagnosis[]) {
      this.setDataValue("diagnosis", JSON.stringify(value || []));
    },
    get : function (this: PatientVisit) {
      const value = this.getDataValue("diagnosis");
      if (isNullOrUndefined(value)) {
        return [];
      }
      return JSON.parse(value);
    }
  })
  diagnosis: PatientDiagnosis[];

  @Column({
    type: DataType.JSON,
    set : function (this: PatientVisit, value: Medication[]) {
      this.setDataValue("medication", JSON.stringify(value || []));
    },
    get : function (this: PatientVisit) {
      const value = this.getDataValue("medication");
      if (isNullOrUndefined(value)) {
        return [];
      }
      return JSON.parse(value);
    }
  })
  medication: Medication[];

  @Column({
    type: DataType.JSON,
    set : function (this: PatientVisit, value: string[]) {
      this.setDataValue("allergies", JSON.stringify(value || []));
    },
    get : function (this: PatientVisit) {
      const value = this.getDataValue("allergies");
      if (isNullOrUndefined(value)) {
        return [];
      }
      return JSON.parse(value);
    }
  })
  allergies: string[];

  @Column({
    type: DataType.JSON,
    set : function (this: PatientVisit, value: string[]) {
      this.setDataValue("lifestyle", JSON.stringify(value || []));
    },
    get : function (this: PatientVisit) {
      const value = this.getDataValue("lifestyle");
      if (isNullOrUndefined(value)) {
        return [];
      }
      return JSON.parse(value);
    }
  })
  lifestyle: string[];

  @Column(DataType.DATE)
  booked_at: Date;

  @Column(DataType.DATE)
  appointment_time: Date;

  @Column(DataType.DATE)
  present_time: Date;

  @Column(DataType.ENUM({values: Helpers.iterateEnum<AppointmentTypes>(AppointmentTypes)}))
  appointment_type: AppointmentTypes;

  @Column(DataType.ENUM({values: Helpers.iterateEnum<VisitType>(VisitType)}))
  visit_type: VisitType;

  @Column(DataType.ENUM({values: Helpers.iterateEnum<BookingType>(BookingType)}))
  booking_type: BookingType;

  @Column(DataType.ENUM({values: Helpers.iterateEnum<Bookie>(Bookie)}))
  booked_by: Bookie;

  @Column(DataType.STRING)
  booked_via: string;

  @Column(DataType.ENUM({values: Helpers.iterateEnum<Bookie>(Bookie)}))
  updated_by: Bookie;

  @Default(false)
  @Column(DataType.BOOLEAN)
  is_doctor_feedback: boolean;

  @Default(false)
  @Column(DataType.BOOLEAN)
  is_patient_feedback: boolean;

  @Column(DataType.FLOAT)
  fees: number;

  @BelongsTo(() => ClinicDoctor)
  clinic_doctor: ClinicDoctor;

  @BelongsTo(() => Patient, {targetKey: "patient_id"})
  patient: Patient;

  @HasMany(() => Feedback)
  feedback: Feedback[];

}
