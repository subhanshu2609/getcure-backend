import {
  AfterDelete,
  AutoIncrement, BeforeCreate, BelongsTo, BelongsToMany,
  Column,
  DataType, Default, ForeignKey, HasMany, HasOne,
  Model,
  PrimaryKey,
  Table,
  Unique
} from "sequelize-typescript";
import { Helpers } from "../util/helpers.util";
import { Clinic } from "./clinic.model";
import { Speciality } from "./speciality.model";

@Table({
  timestamps: true,
  paranoid  : false,
  tableName : "clinics_specialities"
})
export class ClinicSpeciality extends Model<ClinicSpeciality> {
  @Unique
  @PrimaryKey
  @AutoIncrement
  @Column(DataType.BIGINT)
  id: number;

  @ForeignKey(() => Clinic)
  @Column(DataType.BIGINT)
  clinic_id: number;

  @ForeignKey(() => Speciality)
  @Column(DataType.BIGINT)
  speciality_id: number;

  @BelongsTo(() => Clinic)
  clinic: Clinic;

  @BelongsTo(() => Speciality)
  speciality: Speciality;


}
