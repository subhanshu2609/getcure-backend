import {
  AfterDelete,
  AutoIncrement, BeforeCreate, BelongsTo, BelongsToMany,
  Column,
  DataType, Default, ForeignKey, HasMany, HasOne,
  Model,
  PrimaryKey,
  Table,
  Unique
} from "sequelize-typescript";
import { Helpers } from "../util/helpers.util";
import { Clinic } from "./clinic.model";
import { Speciality } from "./speciality.model";
import { ClinicDoctor } from "./clinic-doctor.model";
import { Employee } from "./employee.model";
import { FrontDesk } from "./front-desk.model";

@Table({
  timestamps: true,
  tableName : "fd_cd"
})
export class FdCd extends Model<FdCd> {
  @Unique
  @PrimaryKey
  @AutoIncrement
  @Column(DataType.BIGINT)
  id: number;

  @ForeignKey(() => ClinicDoctor)
  @Column(DataType.BIGINT)
  clinic_doctor_id: number;

  @ForeignKey(() => FrontDesk)
  @Column(DataType.BIGINT)
  front_desk_id: number;

  @BelongsTo(() => ClinicDoctor)
  clinicDoctor: ClinicDoctor;

  @BelongsTo(() => FrontDesk)
  frontDesk: FrontDesk;
}
