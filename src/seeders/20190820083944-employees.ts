import { QueryInterface, SequelizeStatic } from "sequelize";
import { dbService } from "../services/db.service";
import { Employee } from "../models/employee.model";
import { EmployeeType } from "../enums/employee-type.enum";

dbService; // Initialising Sequelize...

const employees: any[] = [
  {
    first_name: "Getcure",
    last_name : "Admin",
    password  : "admin",
    type      : EmployeeType.ADMIN,
    aadhar_no: "261435385157",
    mobile_no : "9711635385",
    gender    : "male",
    dob       : "1999-01-21",
    address   : "J-2/ 106 B DDA Flats Kalkaji, New Delhi",
    pin_code   : "110019",
  }
];

export = {
  /**
   * Write code here to seed data.
   *
   * @param queryInterface
   * @param Sequelize
   */
  up: async (queryInterface: QueryInterface, Sequelize: SequelizeStatic) => {
    return Employee.bulkCreate(employees);
  },

  /**
   * Write code here for drop seed data.
   *
   * @param queryInterface
   * @param Sequelize
   */
  down: async (queryInterface: QueryInterface, Sequelize: SequelizeStatic) => {
    return Employee.truncate();
  }
};
