import { Request, Response } from "express";
import { doctorService } from "../services/entities/doctor.service";
import { DoctorNotFoundException } from "../exceptions/user/doctor-not-found.exception";
import { habitService } from "../services/entities/habit.service";
import { HabitTransformer } from "../transformers/habit.transformer";
import { HabitCreateDto } from "../dtos/habit/habit-create.dto";
import Ajv from "ajv";
import * as fs from "fs";
import { UnprocessableEntityException } from "../exceptions/root/unprocessable-entity.exception";
import { HabitUpdateDto } from "../dtos/habit/habit-update.dto";
import { HabitNotFoundException } from "../exceptions/habit/habit-not-found.exception";
import toBoolean from "validator/lib/toBoolean";

export class HabitController {

  static async showAllergies(req: Request, res: Response) {
    const doctor = await doctorService.show(+req.params.doctorId);
    if (!doctor) {
      throw new DoctorNotFoundException();
    }
    const allergies = await habitService.showDoctorAllergies(doctor.id);
    return res.json({
      data: await new HabitTransformer().transformList(allergies)
    });
  }

  static async showLifestyles(req: Request, res: Response) {
    const doctor = await doctorService.show(+req.params.doctorId);
    if (!doctor) {
      throw new DoctorNotFoundException();
    }
    const allergies = await habitService.showDoctorLifestyle(doctor.id);
    return res.json({
      data: await new HabitTransformer().transformList(allergies)
    });
  }

  static async showHabits(req: Request, res: Response) {
    const doctor = await doctorService.show(+req.params.doctorId);
    if (!doctor) {
      throw new DoctorNotFoundException();
    }
    const habits = await habitService.showDoctorHabits(doctor.id);
    return res.json({
      data: await new HabitTransformer().transformList(habits)
    });
  }

  static async createAllergy(req: Request, res: Response) {
    const inputData = req.body as HabitCreateDto;
    const ajv       = new Ajv();
    const schema    = JSON.parse(fs.readFileSync("./schema/habit/habit-create.schema.json").toString());
    try {
      const valid = await ajv.validate(schema, inputData);
    } catch (e) {
      throw new UnprocessableEntityException(e.errors);
    }
    const doctor = await doctorService.show(inputData.doctor_id);
    if (!doctor) {
      throw new DoctorNotFoundException();
    }

    const habit = await habitService.createAllergy(inputData);
    return res.json({
      data: await new HabitTransformer().transform(habit)
    });
  }

  static async createLifestyle(req: Request, res: Response) {
    const inputData = req.body as HabitCreateDto;
    const ajv       = new Ajv();
    const schema    = JSON.parse(fs.readFileSync("./schema/habit/habit-create.schema.json").toString());
    try {
      const valid = await ajv.validate(schema, inputData);
    } catch (e) {
      throw new UnprocessableEntityException(e.errors);
    }
    const doctor = await doctorService.show(inputData.doctor_id);
    if (!doctor) {
      throw new DoctorNotFoundException();
    }

    const habit = await habitService.createLifestyle(inputData);
    return res.json({
      data: await new HabitTransformer().transform(habit)
    });
  }

  static async bulkCreateHabits(req: Request, res: Response) {
    req.body.habits = JSON.parse(req.body.habits);
    const inputData = req.body as {habits: HabitCreateDto[]};
    const ajv       = new Ajv();
    const schema    = JSON.parse(fs.readFileSync("./schema/habit/habit-bulk-create.schema.json").toString());
    try {
      const valid = await ajv.validate(schema, inputData);
    } catch (e) {
      throw new UnprocessableEntityException(e.errors);
    }
    const doctor = await doctorService.show(inputData.habits[0].doctor_id);
    if (!doctor) {
      throw new DoctorNotFoundException();
    }

    const habit = await habitService.bulkCreate(inputData.habits);
    return res.json({
      data: await new HabitTransformer().transformList(habit)
    });
  }

  static async updateHabit(req: Request, res: Response) {
    if (req.body.is_active) {
      req.body.is_active = toBoolean(req.body.is_active);
    }
    const inputData = req.body as HabitUpdateDto;
    const ajv       = new Ajv();
    const schema    = JSON.parse(fs.readFileSync("./schema/habit/habit-update.schema.json").toString());
    try {
      const valid = await ajv.validate(schema, inputData);
    } catch (e) {
      throw new UnprocessableEntityException(e.errors);
    }

    const habit = await habitService.show(+req.params.habitId);
    if (!habit) {
      throw new HabitNotFoundException();
    }
    const updatedHabit = await habitService.update(habit, inputData);
    return res.json({
      data: await new HabitTransformer().transform(updatedHabit)
    });
  }

  static async deleteHabit(req: Request, res: Response) {
    const habit = await habitService.show(+req.params.habitId);
    if (!habit) {
      throw new HabitNotFoundException();
    }
    await habitService.delete(habit);
    return res.json("success");
  }
}
