import { Request, Response } from "express";
import { patientVisitService } from "../services/entities/patient-visit.service";
import { PatientVisitTransformer } from "../transformers/patient-visit.transformer";
import { PatientVisitBulkCreateDto, PatientVisitCreateDto } from "../dtos/patint-visit/patient-visit-create.dto";
import { clinicService } from "../services/entities/clinic.service";
import { ClinicNotLinkedException } from "../exceptions/clinic/clinic-not-linked.exception";
import { PatientVisit } from "../models/patient-visit.model";
import { FeedbackDto } from "../dtos/feedback/feedback-create.dto";
import { employeeService } from "../services/entities/employee.service";
import { EmployeeNotFoundException } from "../exceptions/employee/employee-not-found.exception";
import { ImageNotFoundException } from "../exceptions/user/image-not-found.exception";
import moment from "moment";
import { s3Service } from "../services/factories/s3.service";
import { ENV_S3_BASE_URL } from "../util/secrets.util";
import { ClinicNotFoundException } from "../exceptions/clinic/clinic-not-found.exception";
import { PatientVisitNotFoundException } from "../exceptions/patient/patient-visit-not-found.exception";
import { VisitAttachment } from "../models/visit-attachment.model";
import { UnprocessableEntityException } from "../exceptions/root/unprocessable-entity.exception";
import { UnauthorizedException } from "../exceptions/root/unauthorized.exception";
import { BookingType } from "../enums/token.enum";
import { Bookie } from "../enums/bookie.enum";
import { ClinicDoctor } from "../models/clinic-doctor.model";
import { Clinic } from "../models/clinic.model";

export class PatientVisitController {
  // static async createPatientVisit(req: Request, res: Response) {
  //   const inputData = req.body as
  // }

  static async viewPatientVisit(req: Request, res: Response) {
    const date          = req.query.date as string;
    const patientVisits = await patientVisitService.viewAll(date);
    return res.json({
      data: await new PatientVisitTransformer().transformList(patientVisits, [
        "patient",
        "feedback"
      ])
    });
  }

  static async viewClinicDoctorsVisits(req: Request, res: Response) {
    const filters      = req.query as { start_date?: string; end_date?: string; query?: string };
    const clinicDoctor = await clinicService.showClinicDoctorById(+req.params.clinicDoctorId);
    if (!clinicDoctor) {
      throw new ClinicNotLinkedException();
    }
    const visits = await patientVisitService.viewByClinicDoctor(clinicDoctor.id, filters);
    return res.json({
      data: await new PatientVisitTransformer().transformList(visits, ["patient"])
    });
  }

  static async viewVisitsByEmployee(req: Request, res: Response) {
    const employee = req.employee;
    const filters: {start_date?: string; end_date?: string; query?: string} = req.query;
    if (!employee) {
      throw new EmployeeNotFoundException();
    }
    const visits        = await patientVisitService.viewPatientVisitsOfEmployee(employee, filters);
    return res.json({
      data: await new PatientVisitTransformer().transformList(visits, ["patient"])
    });
  }


  static async createBulkPatientVisits(req: Request, res: Response) {
    req.body.patient_visits = JSON.parse(req.body.patient_visits) as PatientVisitBulkCreateDto;
    const inputData         = req.body as PatientVisitBulkCreateDto;
    const clinicDoctor      = await clinicService.showClinicDoctorById(+inputData.patient_visits[0]?.clinic_doctor_id);
    if (!clinicDoctor) {
      throw new ClinicNotLinkedException();
    }
    for (const patientVisit of inputData.patient_visits) {
      patientVisit.clinic_doctor_id = +patientVisit.clinic_doctor_id;
      patientVisit.temperature      = +patientVisit.temperature;
      patientVisit.pulse            = +patientVisit.pulse;
      patientVisit.weight           = +patientVisit.weight;
      patientVisit.fees             = +patientVisit.fees;
      await patientVisitService.create(patientVisit);
    }
    return res.json("success");
  }

  static async uploadPatientVisitAttachments(req: Request, res: Response) {
    const images    = req.files as Express.Multer.File[];
    const inputData = req.body as { clinic_doctor_id: number; patient_id: string; appointment_time: string };
    if (!inputData.appointment_time || !inputData.clinic_doctor_id || !inputData.patient_id) {
      throw new UnprocessableEntityException("Insufficient Data");
    }
    const clinicDoctor = await clinicService.showClinicDoctorById(+inputData.clinic_doctor_id);
    if (!clinicDoctor) {
      throw new ClinicNotFoundException();
    }
    if (req.doctor_id != clinicDoctor.doctor_id) {
      throw new ClinicNotLinkedException();
    }
    const patientVisit = await patientVisitService.showByAppointmentClinicDoctorAndPatient(inputData.patient_id, +inputData.clinic_doctor_id, inputData.appointment_time);
    if (!patientVisit) {
      throw new PatientVisitNotFoundException();
    }
    const now       = moment.now();
    const attachments: VisitAttachment[] = [];
    for (const image of images) {
      await s3Service.uploadToS3(image.buffer, now + image.originalname, "examinations");
      const image_url = ENV_S3_BASE_URL + `examinations/` + now + image.originalname;
      const attachment = await VisitAttachment.create({
        clinic_doctor_id: clinicDoctor.id,
        doctor_id: req.doctor_id,
        patient_visit_id: patientVisit.id,
        appointment_time: patientVisit.appointment_time,
        attachment_url: image_url
      });
      attachments.push(attachment);
    }
    return res.json({
      data: attachments
    });
  }
}
