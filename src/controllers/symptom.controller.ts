import { Request, Response } from "express";
import Ajv from "ajv";
import * as fs from "fs";
import { UnprocessableEntityException } from "../exceptions/root/unprocessable-entity.exception";
import { SymptomCreateDto } from "../dtos/symptom/symptom-create.dto";
import { doctorService } from "../services/entities/doctor.service";
import { DoctorNotFoundException } from "../exceptions/user/doctor-not-found.exception";
import { symptomService } from "../services/entities/symptom.service";
import { SymptomTransformer } from "../transformers/symptom.transformer";
import { SymptomNotFoundException } from "../exceptions/sympton/symptom-not-found.exception";
import { HabitUpdateDto } from "../dtos/habit/habit-update.dto";
import toBoolean from "validator/lib/toBoolean";
import { HabitCreateDto } from "../dtos/habit/habit-create.dto";
import { habitService } from "../services/entities/habit.service";
import { HabitTransformer } from "../transformers/habit.transformer";

export class SymptomController {

  static async showSymptoms(req: Request, res: Response) {
    const doctor = await doctorService.show(req.doctor_id);
    if (!doctor) {
      throw new DoctorNotFoundException();
    }
    const symptoms = await symptomService.showActiveSymptoms(doctor.id);
    return res.json({
      data: await new SymptomTransformer().transformList(symptoms)
    });
  }


  static async showAllSymptom(req: Request, res: Response) {
    const doctor = await doctorService.show(+req.params.doctorId);
    if (!doctor) {
      throw new DoctorNotFoundException();
    }
    const symptoms = await symptomService.showAllSymptoms(doctor.id);
    return res.json({
      data: await new SymptomTransformer().transformList(symptoms)
    });
  }

  static async addSymptom(req: Request, res: Response) {
    req.body.doctor_id = +req.body.doctor_id;
    const inputData    = req.body as SymptomCreateDto;
    const ajv          = new Ajv();
    const schema       = JSON.parse(fs.readFileSync("./schema/symptom/symptom-create.schema.json").toString());
    try {
      const valid = await ajv.validate(schema, inputData);
    } catch (e) {
      throw new UnprocessableEntityException(e.errors);
    }
    const doctor = await doctorService.show(inputData.doctor_id);
    if (!doctor) {
      throw new DoctorNotFoundException();
    }
    const symptom = await symptomService.create(inputData);
    return res.json({
      data: await new SymptomTransformer().transform(symptom)
    });
  }

  static async bulkCreateSymptom(req: Request, res: Response) {
    req.body.symptoms = JSON.parse(req.body.symptoms);
    const inputData = req.body as {symptoms: SymptomCreateDto[]};
    const ajv       = new Ajv();
    const schema    = JSON.parse(fs.readFileSync("./schema/symptom/symptom-bulk-create.schema.json").toString());
    try {
      const valid = await ajv.validate(schema, inputData);
    } catch (e) {
      throw new UnprocessableEntityException(e.errors);
    }
    const doctor = await doctorService.show(req.doctor_id);
    if (!doctor) {
      throw new DoctorNotFoundException();
    }

    const symptoms = await symptomService.bulkCreate(inputData.symptoms, req.doctor_id);
    return res.json({
      data: await new SymptomTransformer().transformList(symptoms)
    });
  }

  static async updateSymptom(req: Request, res: Response) {
    if (req.body.is_active) {
      req.body.is_active = toBoolean(req.body.is_active);
    }
    const inputData = req.body as HabitUpdateDto;
    const ajv       = new Ajv();
    const schema    = JSON.parse(fs.readFileSync("./schema/habit/habit-update.schema.json").toString());
    try {
      const valid = await ajv.validate(schema, inputData);
    } catch (e) {
      throw new UnprocessableEntityException(e.errors);
    }

    const symptom = await symptomService.show(+req.params.symptomId);
    if (!symptom) {
      throw new SymptomNotFoundException();
    }
    const updatedSymptom = await symptomService.update(symptom, inputData);
    return res.json({
      data: await new SymptomTransformer().transform(updatedSymptom)
    });
  }

  static async deleteSymptom(req: Request, res: Response) {
    const symptom = await symptomService.show(+req.params.symptomId);
    if (!symptom) {
      throw new SymptomNotFoundException();
    }
    await symptomService.delete(symptom);
    return res.json("success");
  }
}
