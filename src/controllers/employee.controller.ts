import { Request, Response } from "express";
import { employeeService } from "../services/entities/employee.service";
import { EmployeeNotFoundException } from "../exceptions/employee/employee-not-found.exception";
import { EmployeeTransformer } from "../transformers/employee.transformer";
import { compareSync } from "bcrypt";
import jwt from "jsonwebtoken";
import { EmployeeCreateDto } from "../dtos/employee/employee-create.dto";
import Ajv from "ajv";
import * as fs from "fs";
import { UnprocessableEntityException } from "../exceptions/root/unprocessable-entity.exception";
import { EmployeeUpdateDto } from "../dtos/employee/employee-update.dto";
import toBoolean from "validator/lib/toBoolean";
import { dbService } from "../services/db.service";
import { clinicService } from "../services/entities/clinic.service";
import { ClinicDoctorTransformer } from "../transformers/clinic-doctor.transformer";
import { ClinicNotLinkedException } from "../exceptions/clinic/clinic-not-linked.exception";
import { EmployeeNotLinkedException } from "../exceptions/employee/employee-not-linked.exception";
export class EmployeeController {
  static async showEmployee(req: Request, res: Response) {
    const employee = await employeeService.show(+req.params.employeeId);
    if (!employee) {
      throw new EmployeeNotFoundException();
    }
    return res.json({
      data: await new EmployeeTransformer().transform(employee, ["clinicDoctors"])
    });
  }

  static async showEmployees(req: Request, res: Response) {
    const employee = await employeeService.index();
    return res.json({
      data: await new EmployeeTransformer().transformList(employee)
    });
  }

  static async loginEmployee(req: Request, res: Response) {
    const inputData = req.body as { mobile_no: string; password: string };
    const employee  = await employeeService.showByMobile(inputData.mobile_no);
    if (!employee) {
      throw new EmployeeNotFoundException();
    }
    const isPasswordCorrect = compareSync(inputData.password, employee.password);
    if (!isPasswordCorrect) {
      res.status(403);
      return res.json({
        message: "Incorrect Password"
      });
    }
    return res.json({
      token: jwt.sign({id: employee.id}, "secret"),
      data : await new EmployeeTransformer().transform(employee)
    });
  }

  static async createEmployee(req: Request, res: Response) {
    const inputData = req.body as EmployeeCreateDto;
    const ajv       = new Ajv();
    const schema    = JSON.parse(fs.readFileSync("./schema/employee/create-employee.schema.json").toString());
    try {
      const valid = await ajv.validate(schema, inputData);
    } catch (e) {
      throw new UnprocessableEntityException(e.errors);
    }
    const transaction = await dbService.getSequelize().transaction();
    try {
      const employee   = await employeeService.create(inputData, transaction);
      const cdEmployee = await employeeService.createCdEmployee(inputData.clinic_ids, inputData.doctor_ids, employee.id, transaction);
      await transaction.commit();

      return res.json({
        data: await new EmployeeTransformer().transform(employee)
      });
    } catch (e) {
      await transaction.rollback();
      throw new UnprocessableEntityException(e);
    }
  }

  static async clinicDoctorsOfEmployee(req: Request, res: Response) {
    const filters       = req.query as { state_id?: number; query?: string };
    const clinicDoctors = await employeeService.showClinicDoctorsByEmployee(req.employee, filters);
    return res.json({
      data: await new ClinicDoctorTransformer().transformList(clinicDoctors)
    });
  }

  static async updateEmployee(req: Request, res: Response) {
    if (req.body.is_active) {
      req.body.is_active = toBoolean(req.body.is_active);
    }
    const inputData = req.body as EmployeeUpdateDto;
    const ajv       = new Ajv();
    const schema    = JSON.parse(fs.readFileSync("./schema/employee/update-employee.schema.json").toString());
    try {
      const valid = await ajv.validate(schema, inputData);
    } catch (e) {
      throw new UnprocessableEntityException(e.errors);
    }
    const employee = await employeeService.show(+req.params.employeeId);
    if (!employee) {
      throw new EmployeeNotFoundException();
    }
    await employeeService.createCdEmployee(
      inputData.clinic_ids ? inputData.clinic_ids : [],
      inputData?.doctor_ids ? inputData.doctor_ids : [],
      employee.id);
    const updatedEmployee = await employeeService.update(employee, inputData);
    return res.json({
      data: await new EmployeeTransformer().transform(updatedEmployee, ["clinicDoctors"])
    });
  }

  static async updateMe(req: Request, res: Response) {
    const inputData       = req.body as EmployeeUpdateDto;
    const updatedEmployee = await employeeService.update(req.employee, inputData);
    return res.json({
      data: await new EmployeeTransformer().transform(updatedEmployee)
    });
  }

  static async deleteEmployee(req: Request, res: Response) {
    const employee = await employeeService.show(+req.params.employeeId);
    if (!employee) {
      throw new EmployeeNotFoundException();
    }
    await employeeService.delete(employee);
    return res.json({
      data: {message: "Deletion Successful"}
    });
  }

  static async deleteMe(req: Request, res: Response) {
    await employeeService.delete(req.employee);
    return res.json({
      data: "Success"
    });
  }

  static async unlinkClinicDoctors(req: Request, res: Response) {
    const employee = await employeeService.show(+req.params.employeeId);
    if (!employee) {
      throw new EmployeeNotFoundException();
    }
    const clinicDoctor = await clinicService.showClinicDoctorById(+req.params.clinicDoctorId);
    if (!clinicDoctor) {
      throw new ClinicNotLinkedException();
    }
    const cdEmployee = await employeeService.showCdEmployeeByCdAndEmployee(clinicDoctor.id, employee.id);
    if (!cdEmployee) {
      throw new EmployeeNotLinkedException();
    }
    await employeeService.deleteCdEmployee(cdEmployee);
    return res.json("success");
  }

  static async viewClinicDoctors(req: Request, res: Response) {
    const employee = await employeeService.show(+req.params.employeeId);
    if (!employee) {
      throw new EmployeeNotFoundException();
    }
    const cDs = await employeeService.showClinicDoctorsByEmployee(employee, {});
    const clinicDoctors: {
      id: number,
      clinic_name: string,
      doctor_name: string
    }[]       = [];

    cDs.map(cd => {
      clinicDoctors.push({
        id         : cd.id,
        clinic_name: cd.clinic.name,
        doctor_name: cd.doctor_name
      });
    });
    return res.json({
      data: clinicDoctors
    });
  }
}
