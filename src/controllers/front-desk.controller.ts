import { Request, Response } from "express";
import { FrontDeskCreateDto } from "../dtos/front-desk/front-desk-create.dto";
import Ajv from "ajv";
import * as fs from "fs";
import { UnprocessableEntityException } from "../exceptions/root/unprocessable-entity.exception";
import { frontDeskService } from "../services/entities/front-desk.service";
import { FrontDeskTransformer } from "../transformers/front-desk.transformer";
import { compareSync } from "bcrypt";
import jwt from "jsonwebtoken";
import { FrontDeskNotFoundException } from "../exceptions/user/front-desk-not-found.exception";
import { FrontDeskNotVerifiedException } from "../exceptions/user/front-desk-not-verified.exception";
import { FrontDeskUpdateDto } from "../dtos/front-desk/front-desk-update.dto";
import { clinicService } from "../services/entities/clinic.service";
import { ClinicNotFoundException } from "../exceptions/clinic/clinic-not-found.exception";
import { ClinicNotLinkedException } from "../exceptions/clinic/clinic-not-linked.exception";
import { dbService } from "../services/db.service";
import { DoctorAlreadyLinkedException } from "../exceptions/user/doctor-already-linked.exception";
import { FrontDesk } from "../models/front-desk.model";
import { ClinicDoctor } from "../models/clinic-doctor.model";
import toBoolean from "validator/lib/toBoolean";
import { s3Service } from "../services/factories/s3.service";
import { ENV_S3_BASE_URL } from "../util/secrets.util";
import { ClinicDoctorTransformer } from "../transformers/clinic-doctor.transformer";
import { FdCd } from "../models/fd-cd.model";

export class FrontDeskController {
  static async createFrontDesk(req: Request, res: Response) {
    req.body.clinic_id = +req.body.clinic_id;
    if (req.body?.doctor_ids?.length > 0) {
      for (let i = 0; i < req.body.doctor_ids.length; i++) {
        req.body.doctor_ids[i] = +req.body.doctor_ids[i];
      }
    }
    const inputData = req.body as FrontDeskCreateDto;
    const ajv       = new Ajv();
    const schema    = JSON.parse(fs.readFileSync("./schema/front-desk/front-desk-create.schema.json").toString());
    const valid     = await ajv.validate(schema, inputData);
    if (!valid) {
      throw new UnprocessableEntityException(ajv.errors);
    }
    const clinic = await clinicService.show(inputData.clinic_id);
    if (!clinic) {
      throw new ClinicNotFoundException();
    }
    const transaction = await dbService.getSequelize().transaction();
    const frontDesk   = await frontDeskService.createFrontDesk(inputData, transaction);
    if (inputData?.doctor_ids?.length > 0) {
      for (const doctor_id of inputData.doctor_ids) {
        const clinicDoctor = await clinicService.showClinicDoctor(inputData.clinic_id, doctor_id);
        if (!clinicDoctor) {
          throw new ClinicNotLinkedException();
        }
        try {
          await FdCd.create({
            clinic_doctor_id: clinicDoctor.id,
            front_desk_id   : frontDesk.id
          }, {transaction});
          await transaction.commit();
        } catch (e) {
          await transaction.rollback();
          throw new UnprocessableEntityException(e);
        }
      }
    }
    return res.json({
      data: await new FrontDeskTransformer().transform(frontDesk)
    });
  }

  static async loginFrontDesk(req: Request, res: Response) {
    const inputData = req.body as { emailOrPhone: string, password: string };
    let frontDesk;
    if (inputData.emailOrPhone.indexOf("@") !== -1) {
      frontDesk = await frontDeskService.showFrontDeskByEmail(inputData.emailOrPhone, true);
    } else {
      frontDesk = await frontDeskService.showFrontDeskByMobile(inputData.emailOrPhone, true);
    }
    if (!frontDesk) {
      throw new FrontDeskNotFoundException();
    }
    const isPasswordCorrect = compareSync(inputData.password, frontDesk.password);

    if (!isPasswordCorrect) {
      res.status(403);
      return res.json({
        message: "Wrong Password"
      });
    }
    return res.json({
      token: jwt.sign({front_desk_id: frontDesk.id}, "secret", {algorithm: "HS256"}),
      data : await (new FrontDeskTransformer()).transform(frontDesk),
    });
  }

  static async updateFrontDesk(req: Request, res: Response) {
    if (req.body.is_active && typeof req.body.is_active !== "boolean") {
      req.body.is_active = toBoolean(req.body.is_active);
    }
    const body      = req.body as FrontDeskUpdateDto;
    const frontDesk = await frontDeskService.show(+req.params.fdId);
    if (!frontDesk) {
      throw new FrontDeskNotFoundException();
    }
    const ajv    = new Ajv();
    const schema = JSON.parse(fs.readFileSync("./schema/front-desk/front-desk-update.schema.json").toString());
    const valid  = await ajv.validate(schema, body);
    if (!valid) {
      throw new UnprocessableEntityException(ajv.errors);
    }
    const updatedFrontDesk = await frontDeskService.update(frontDesk, body);

    return res.json({
      data: await (new FrontDeskTransformer()).transform(updatedFrontDesk)
    });
  }

  static async updateMyFrontDeskProfile(req: Request, res: Response) {
    const body      = req.body as FrontDeskUpdateDto;
    const frontDesk = await frontDeskService.show(req.frontDesk.id);
    const ajv       = new Ajv();
    const schema    = JSON.parse(fs.readFileSync("./schema/front-desk/front-desk-update.schema.json").toString());
    const valid     = await ajv.validate(schema, body);
    if (!valid) {
      throw new UnprocessableEntityException(ajv.errors);
    }
    const updatedFrontDesk = await frontDeskService.update(frontDesk, body);

    return res.json({
      data: await (new FrontDeskTransformer()).transform(updatedFrontDesk)
    });
  }

  static async showFrontDesksOfClinic(req: Request, res: Response) {
    const frontDesks = await frontDeskService.viewFrontDesksOfClinic(+req.params.clinicId);
    return res.json({
      data: await new FrontDeskTransformer().transformList(frontDesks)
    });
  }

  static async showFrontDesk(req: Request, res: Response) {
    const frontDesk = await FrontDesk.findById(+req.params.fdId, {include: [ClinicDoctor]});
    if (!frontDesk) {
      throw new FrontDeskNotFoundException();
    }
    return res.json({
      data: await new FrontDeskTransformer().transform(frontDesk)
    });
  }

  static async showClinicDoctorsByFrontDesk(req: Request, res: Response) {
    const clinicDoctors = await clinicService.showClinicDoctorByFrontDesk(+req.frontDesk.id);
    return res.json({
      data: await new ClinicDoctorTransformer().transformList(clinicDoctors)
    });
  }

  static async showAllFrontDesks(Req: Request, res: Response) {
    const frontDesks = await frontDeskService.showAllFrontDesks();
    return res.json({
      data: await new FrontDeskTransformer().transformList(frontDesks)
    });
  }

  static async showMyFrontDeskProfile(req: Request, res: Response) {
    const frontDesk = await frontDeskService.show(req.frontDesk.id, true);
    return res.json({
      data: await new FrontDeskTransformer().transform(frontDesk)
    });
  }

  static async deleteFrontDesk(req: Request, res: Response) {
    const frontDesk = await frontDeskService.show(+req.params.fdId);
    if (!frontDesk) {
      throw new FrontDeskNotFoundException();
    }
    return res.json({
      data: "Success"
    });
  }

  static async unlinkFrontDesk(req: Request, res: Response) {
    const inputData    = req.body as { front_desk_id: number; clinic_doctor_id: number };
    if (!inputData.front_desk_id || !inputData.clinic_doctor_id) {
      throw new UnprocessableEntityException("Validation Error", "Validation Error");
    }
    const fdCd = await FdCd.findOne({
      where: {
        front_desk_id   : inputData.front_desk_id,
        clinic_doctor_id: inputData.clinic_doctor_id
      }
    });
    if (!fdCd) {
      throw new FrontDeskNotFoundException();
    }
    await fdCd.destroy();
    return res.json("success");
  }
}
