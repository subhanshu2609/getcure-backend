import { Request, Response } from "express";
import { specialityService } from "../services/entities/speciality.service";
import { SpecialityTransformer } from "../transformers/speciality.transformer";
import { SpecialityNotFoundException } from "../exceptions/speciality/speciality-not-found.exception";

export class SpecialityController {

  static async indexSpeciality(req: Request, res: Response) {
    const filters      = req.query as { query?: string };
    const specialities = await specialityService.index(filters);
    return res.json({
      data: await new SpecialityTransformer().transformList(specialities)
    });
  }

  static async createSpeciality(req: Request, res: Response) {
    const inputData  = req.body.title;
    const speciality = await specialityService.createSpeciality(inputData);
    return res.json({
      data: await new SpecialityTransformer().transform(speciality)
    });
  }

  static async updateSpeciality(req: Request, res: Response) {
    const inputData  = req.body.title;
    const speciality = await specialityService.show(+req.params.specialityId);
    if (!speciality) {
      throw new SpecialityNotFoundException();
    }
    const update = await specialityService.updateSpeciality(speciality, inputData);
    return res.json({
      data: await new SpecialityTransformer().transform(update)
    });
  }

  static async deleteSpeciality(req: Request, res: Response) {
    const speciality = await specialityService.show(+req.params.specialityId);
    if (!speciality) {
      throw new SpecialityNotFoundException();
    }
    await specialityService.delete(speciality);
    return res.json({data: "Speciality Deleted Successfully"});
  }
}
