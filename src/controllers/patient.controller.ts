import { NextFunction, Request, Response } from "express";
import { PreUser } from "../models/pre-user.model";
import { UnauthorizedException } from "../exceptions/root/unauthorized.exception";
import Ajv from "ajv";
import * as fs from "fs";
import { UnprocessableEntityException } from "../exceptions/root/unprocessable-entity.exception";
import { IncorrectOtpException } from "../exceptions/user/incorrect-otp.exception";
import { dbService } from "../services/db.service";
import { doctorService } from "../services/entities/doctor.service";
import jwt from "jsonwebtoken";
import { PatientCreateDto } from "../dtos/patient/patient-create.dto";
import { patientService } from "../services/entities/patient.service";
import { PatientTransformer } from "../transformers/patient.transformer";
import { OtpType } from "../enums/otp-type.enum";
import { tokenService } from "../services/entities/token.service";
import { TokenTransformer } from "../transformers/token.transformer";
import { PatientNotFoundException } from "../exceptions/patient/patient-not-found.exception";
import { PatientVisit } from "../models/patient-visit.model";
import { PatientVisitTransformer } from "../transformers/patient-visit.transformer";
import { Patient } from "../models/patient.model";
import { PatientAlreadyExistsException } from "../exceptions/patient/patient-already-exists.exception";
import { Clinic } from "../models/clinic.model";
import { ClinicDoctor } from "../models/clinic-doctor.model";
import { PatientUpdateDto } from "../dtos/patient/patient-update.dto";
import { Token } from "../models/token.model";
import { BookingType } from "../enums/token.enum";
import { Op } from "sequelize";
import { Helpers } from "../util/helpers.util";

export class PatientController {

  static async authenticate(req: Request, res: Response, next: NextFunction) {
    const inputData = req.body as { mobile_no: string, otp: string };
    const ajv       = new Ajv();
    const schema    = JSON.parse(fs.readFileSync("./schema/doctor/user-login.schema.json").toString());
    try {
      await ajv.validate(schema, inputData);
    } catch (e) {
      throw new UnprocessableEntityException(e.errors);
    }
    const preUser = await PreUser.findOne({
      where: {
        mobile_no: inputData.mobile_no
      }
    });

    if (!preUser) {
      throw new UnauthorizedException("You are Not Authorized", 401);
    }

    if (preUser.otp != inputData.otp) {
      throw new IncorrectOtpException();
    }
    const patient = await patientService.showByMobileNumber(inputData.mobile_no);
    await preUser.destroy();

    return res.json({
      token: jwt.sign({patient_mobile: patient[0].mobile_no}, "secret"),
      data : await (new PatientTransformer()).transformList(patient)
    });
  }

  static async createPatient(req: Request, res: Response, next: NextFunction) {
    req.body.age    = +req.body.age;
    const inputData = req.body as PatientCreateDto;

    const preUser = await PreUser.findOne({
      where: {
        mobile_no: inputData.mobile_no
      }
    });

    if (!preUser) {
      throw new UnauthorizedException("Kindly Request for OTP first", 401);
    }
    const ajv    = new Ajv();
    const schema = JSON.parse(fs.readFileSync("./schema/patient/patient-create.schema.json").toString());
    try {
      const valid = await ajv.validate(schema, inputData);
    } catch (e) {
      throw new UnprocessableEntityException(e.errors);
    }
    if (preUser.otp != inputData.otp) {
      throw new IncorrectOtpException();
    }
    const transaction = await dbService.getSequelize().transaction();
    try {
      const newPatient = await patientService.createPatient(inputData, transaction);
      const patient    = await patientService.showByMobileNumber(newPatient.mobile_no, transaction);
      await preUser.destroy({transaction});
      await transaction.commit();
      return res.json({
        token: jwt.sign({patient_mobile: patient[0].mobile_no}, "secret", {algorithm: "HS256"}),
        data : await new PatientTransformer().transformList(patient)
      });
    } catch (e) {
      await transaction.rollback();
      throw new UnprocessableEntityException(e);
    }
  }

  static async generateOtp(req: Request, res: Response) {
    const inputData = req.body as { mobile_no: string; type: OtpType };
    const ajv       = new Ajv();
    const schema    = JSON.parse(fs.readFileSync("./schema/doctor/pre-user-create.schema.json").toString());
    try {
      const valid = await ajv.validate(schema, inputData);
    } catch (e) {
      throw new UnprocessableEntityException(e.errors);
    }

    const preUser = await doctorService.preSignup(inputData);
    // todo: sendOtp

    const user = await patientService.showByMobileNumber(inputData.mobile_no);
    if (inputData.type === OtpType.LOGIN) {
      if (user.length > 0) {
        // await otpService.sendOtp(inputData.mobile_no, preUser.otp);
        return res.json({data: `OTP Generated, Kindly Login with OTP: ${preUser.otp}`});
      } else {
        return res.json({data: `Cannot Find a user with that Mobile Number`});
      }
    } else {
      if (user.length > 0) {
        return res.json({data: "Already Registered, Kindly Login"});
      } else {
        // await otpService.sendOtp(inputData.mobile_no, preUser.otp);
        return res.json({data: `OTP Generated, Kindly Register with OTP: ${preUser.otp}`});
      }
    }
  }

  static async addPatient(req: Request, res: Response) {
    req.body.age    = +req.body.age;
    const inputData = req.body as PatientCreateDto;
    const ajv       = new Ajv();
    const schema    = JSON.parse(fs.readFileSync("./schema/patient/patient-create.schema.json").toString());
    try {
      const valid = await ajv.validate(schema, inputData);
    } catch (e) {
      throw new UnprocessableEntityException(e.errors);
    }
    const oldPatient = await Patient.findOne({
      where: {
        mobile_no: inputData.mobile_no,
        name     : inputData.name
      }
    });
    if (oldPatient) {
      throw new PatientAlreadyExistsException();
    }
    const patient     = await patientService.createPatient(inputData);
    const allPatients = await patientService.showByMobileNumber(patient.mobile_no);
    return res.json({
      data: await new PatientTransformer().transformList(allPatients)
    });
  }

  static async addBulkPatient(req: Request, res: Response) {
    const patientsData = JSON.parse(req.body.patients) as PatientCreateDto[];
    const ids          = [];
    const returnObject: {
      previousId: string,
      newId: string
    }[] = [];
    for (const inputData of patientsData) {
      req.body.age = +req.body.age;
      const ajv    = new Ajv();
      const schema = JSON.parse(fs.readFileSync("./schema/patient/patient-create.schema.json").toString());
      try {
        const valid = await ajv.validate(schema, inputData);
      } catch (e) {
        throw new UnprocessableEntityException(e.errors);
      }
      const oldPatient = await Patient.findOne({
        where: {
          patient_id: inputData.patient_id
        }
      });
      if (oldPatient && oldPatient.name !== Helpers.titlecase(inputData.name)) {
        const oldData = oldPatient;
        await oldPatient.destroy();
        inputData.name   = Helpers.titlecase(inputData.name);
        const patient    = await Patient.create(inputData);
        const newPatient = await patientService.createPatient({
          name     : oldData.name,
          mobile_no: oldData.mobile_no,
          age      : oldData.age,
          address  : oldData.address,
          gender   : oldData.gender
        });
        ids.push(patient.patient_id);
      } else if (oldPatient && oldPatient.name === Helpers.titlecase(inputData.name)) {
        ids.push(oldPatient.patient_id);
      } else {
        const patient = await patientService.createPatient(inputData);
        ids.push(patient.patient_id);
      }
    }
    for (let i = 0; i < patientsData.length; i ++) {
      returnObject.push({
        previousId: patientsData[i].patient_id,
        newId: ids[i]
      });
    }
    return res.json({
      patient_ids: returnObject
    });
  }

  static async upcomingAppointments(req: Request, res: Response) {
    const tokens = await tokenService.showByMobile(req.patient[0].mobile_no);
    return res.json({
      data: await new TokenTransformer().transformList(tokens, ["clinic_doctor"])
    });
  }

  static async previousAppointments(req: Request, res: Response) {
    const visits = await PatientVisit.findAll({
      where  : {
        ["patient_id" as string]: {
          like: `%${req.patient[0].mobile_no}%`
        }
      },
      include: [{model: ClinicDoctor, include: [Clinic]}]
    });

    return res.json({
      data: await new PatientVisitTransformer().transformList(visits, ["clinic_doctor"])
    });
  }

  static async cancelledAppointments(req: Request, res: Response) {
    const appointments = await Token.findAll({
      where  : {
        ["patient_id" as string]: {
          like: `%${req.patient[0].mobile_no}%`
        },
        booking_type            : BookingType.CANCELLED
      },
      include: [Patient]
    });
    return res.json({
      data: await new TokenTransformer().transformList(appointments)
    });
  }

  static async updatePatient(req: Request, res: Response) {
    const patient = await patientService.showById(+req.params.patientId);
    if (!patient) {
      throw new PatientNotFoundException();
    }
    const inputData = req.body as PatientUpdateDto;
    if (inputData.age) {
      inputData.age = +inputData.age;
    }
    await patientService.update(patient, inputData);
    const patients = await patientService.showByMobileNumber(patient.mobile_no);
    return res.json({
      token: jwt.sign({patient_mobile: patients[0].mobile_no}, "secret"),
      data : await new PatientTransformer().transformList(patients)
    });
  }

  static async indexPatients(req: Request, res: Response) {
    const filters = req.query as {query?: string};
    const patients = await patientService.index(filters);
    return res.json({
      data: await new PatientTransformer().transformList(patients)
    });
  }
}
