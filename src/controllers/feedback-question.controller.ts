import { Request, Response } from "express";
import { feedbackQuestionService } from "../services/entities/feedback-question.service";
import { FeedbackQuestionCreateDto } from "../dtos/feedback/feedback-question-create.dto";
import Ajv from "ajv";
import * as fs from "fs";
import { FeedbackQuestionUpdateDto } from "../dtos/feedback/feedback-question-update.dto";
import { FeedbackQuestionNotFoundException } from "../exceptions/feedback/feedback-question-not-found.exception";
import { UnprocessableEntityException } from "../exceptions/root/unprocessable-entity.exception";

export class FeedbackQuestionController {

  static async indexQuestions(req: Request, res: Response) {
    const questions = await feedbackQuestionService.index();
    return res.json({
      data: questions
    });
  }

  static async viewAllQuestions(req: Request, res: Response) {
    const questions = await feedbackQuestionService.viewAll();
    return res.json({
      data: questions
    });
  }

  static async createQuestion(req: Request, res: Response) {
    const inputData = req.body as FeedbackQuestionCreateDto;
    const ajv       = new Ajv();
    const schema    = JSON.parse(fs.readFileSync("./schema/feedback/feedback-question-create.schema.json").toString());
    try {
      await ajv.validate(schema, inputData);
    } catch (e) {
      throw new UnprocessableEntityException(e);
    }
    const question = await feedbackQuestionService.create(inputData);
    return res.json({
      data: question
    });
  }

  static async updateQuestion(req: Request, res: Response) {
    const inputData = req.body as FeedbackQuestionUpdateDto;
    const ajv       = new Ajv();
    const schema    = JSON.parse(fs.readFileSync("./schema/feedback/feedback-question-update.schema.json").toString());
    try {
      await ajv.validate(schema, inputData);
    } catch (e) {
      throw new UnprocessableEntityException(e.errors);
    }
    const question = await feedbackQuestionService.show(+req.params.questionId);
    if (!question) {
      throw new FeedbackQuestionNotFoundException();
    }
    const update = await feedbackQuestionService.update(question, inputData);
    return res.json({
      data: update
    });
  }

  static async deleteQuestion(req: Request, res: Response) {
    const question = await feedbackQuestionService.show(+req.params.questionId);
    if (!question) {
      throw new FeedbackQuestionNotFoundException();
    }
    await feedbackQuestionService.delete(question);
    return res.json("success");
  }
}
