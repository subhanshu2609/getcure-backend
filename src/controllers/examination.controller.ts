import { Request, Response } from "express";
import { ClinicCreateDto } from "../dtos/clinic/clinic-create.dto";
import Ajv from "ajv";
import * as fs from "fs";
import { UnprocessableEntityException } from "../exceptions/root/unprocessable-entity.exception";
import { clinicService } from "../services/entities/clinic.service";
import { dbService } from "../services/db.service";
import { ClinicDoctor } from "../models/clinic-doctor.model";
import { ClinicTransformer } from "../transformers/clinic.transformer";
import { ClinicNotFoundException } from "../exceptions/clinic/clinic-not-found.exception";
import { ExaminationCreateDto } from "../dtos/examination/examination-create.dto";
import { doctorService } from "../services/entities/doctor.service";
import { DoctorNotFoundException } from "../exceptions/user/doctor-not-found.exception";
import { examinationService } from "../services/entities/examination.service";
import { ExaminationTransformer } from "../transformers/examination.transformer";
import { ExaminationUpdateDto } from "../dtos/examination/examination-update.dto";
import { ExaminationNotFoundException } from "../exceptions/examination/examination-not-found.exception";
import { SymptomCreateDto } from "../dtos/symptom/symptom-create.dto";
import { symptomService } from "../services/entities/symptom.service";
import { SymptomTransformer } from "../transformers/symptom.transformer";
import { Examination } from "../models/examinations.model";

export class ExaminationController {

  static async showExaminations(req: Request, res: Response) {
    const doctor = await doctorService.show(req.doctor_id);
    if (!doctor) {
      throw new DoctorNotFoundException();
    }
    const examinations = await examinationService.showByDoctorId(doctor.id);
    return res.json({
      data: await new ExaminationTransformer().transformList(examinations)
    });
  }

  static async showExaminationsByDoctor(req: Request, res: Response) {
    const doctor = await doctorService.show(+req.params.doctorId);
    if (!doctor) {
      throw new DoctorNotFoundException();
    }
    const examinations = await examinationService.showByDoctorId(doctor.id);
    return res.json({
      data: await new ExaminationTransformer().transformList(examinations)
    });
  }

  static async addExamination(req: Request, res: Response) {
    if (req.body.price) {
      req.body.price = +req.body.price;
    }
    req.body.doctor_id = +req.body.doctor_id;
    const inputData    = req.body as ExaminationCreateDto;
    const ajv          = new Ajv();
    const schema       = JSON.parse(fs.readFileSync("./schema/examination/examination-create.schema.json").toString());
    try {
      const valid = await ajv.validate(schema, inputData);
    } catch (e) {
      throw new UnprocessableEntityException(e.errors);
    }
    const doctor = await doctorService.show(inputData.doctor_id);
    if (!doctor) {
      throw new DoctorNotFoundException();
    }
    const examination = await examinationService.create(inputData);
    return res.json({
      data: await new ExaminationTransformer().transform(examination)
    });
  }

  static async bulkCreateExaminations(req: Request, res: Response) {
    req.body.examinations = JSON.parse(req.body.examinations);
    const inputData = req.body as {examinations: ExaminationCreateDto[]};
    const ajv       = new Ajv();
    const schema    = JSON.parse(fs.readFileSync("./schema/examination/examination-bulk-create.schema.json").toString());
    try {
      const valid = await ajv.validate(schema, inputData);
    } catch (e) {
      throw new UnprocessableEntityException(e.errors);
    }
    const doctor = await doctorService.show(inputData.examinations[0].doctor_id);
    if (!doctor) {
      throw new DoctorNotFoundException();
    }

    const examinations = await examinationService.bulkCreate(inputData.examinations);
    return res.json({
      data: await new ExaminationTransformer().transformList(examinations)
    });
  }

  static async updateExamination(req: Request, res: Response) {
    if (req.body.price) {
      req.body.price = +req.body.price;
    }
    const inputData   = req.body as ExaminationUpdateDto;
    const examination = await examinationService.show(+req.params.examinationId);
    if (!examination) {
      throw new ExaminationNotFoundException();
    }
    const ajv    = new Ajv();
    const schema = JSON.parse(fs.readFileSync("./schema/examination/examination-update.schema.json").toString());
    try {
      const valid = await ajv.validate(schema, inputData);
    } catch (e) {
      throw new UnprocessableEntityException(e.errors);
    }
    const updatedExamination = await examinationService.update(inputData, examination);
    return res.json({
      data: await new ExaminationTransformer().transform(updatedExamination)
    });
  }

  static async deleteExamination(req: Request, res: Response) {
    const examination = await examinationService.show(+req.params.examinationId);
    if (!examination) {
      throw new ExaminationNotFoundException();
    }
    await examinationService.delete(examination);
    return res.json("success");
  }
}
