import { Request, Response } from "express";
import Ajv from "ajv";
import * as fs from "fs";
import { UnprocessableEntityException } from "../exceptions/root/unprocessable-entity.exception";
import { FeedbackBulkCreateDto, FeedbackCreateDto, FeedbackDto } from "../dtos/feedback/feedback-create.dto";
import { feedbackService } from "../services/entities/feedback.service";
import { patientVisitService } from "../services/entities/patient-visit.service";
import { PatientVisitNotFoundException } from "../exceptions/patient/patient-visit-not-found.exception";
import { FeedbackFiller } from "../enums/feedback-filler.enum";
import { UnauthorizedException } from "../exceptions/root/unauthorized.exception";
import { dbService } from "../services/db.service";
import { Helpers } from "../util/helpers.util";
import { clinicService } from "../services/entities/clinic.service";
import { ClinicNotLinkedException } from "../exceptions/clinic/clinic-not-linked.exception";
import { PatientVisit } from "../models/patient-visit.model";

export class FeedbackController {


  static async viewAllFeedback(req: Request, res: Response) {
    const questions = await feedbackService.viewAll();
    return res.json({
      data: questions
    });
  }

  static async viewClinicDoctorFeedbacks(req: Request, res: Response) {
    const clinicDoctor = await clinicService.showClinicDoctorById(+req.params.clinicDoctorId);
    if (!clinicDoctor) {
      throw new ClinicNotLinkedException();
    }
    const feedbacks = await feedbackService.viewByClinicDoctor(clinicDoctor.id);
    return res.json({
      data: feedbacks
    });
  }

  static async createFeedback(req: Request, res: Response) {
    req.body.patient_visit_id = +req.body.patient_visit_id;
    req.body.feedback         = JSON.parse(req.body.feedback) as FeedbackDto;
    const inputData           = req.body as FeedbackCreateDto;
    const ajv                 = new Ajv();
    const schema              = JSON.parse(fs.readFileSync("./schema/feedback/feedback-create.schema.json").toString());
    try {
      await ajv.validate(schema, inputData);
    } catch (e) {
      throw new UnprocessableEntityException(e);
    }
    const patientVisit = await patientVisitService.show(inputData.patient_visit_id);
    if (!patientVisit) {
      throw new PatientVisitNotFoundException();
    }
    await feedbackService.create(inputData.feedback, patientVisit, FeedbackFiller.PATIENT);
    await patientVisit.update({
      is_patient_feedback: true
    });
    return res.json("success");
  }

  static async createDoctorFeedback(req: Request, res: Response) {
    req.body.patient_visit_id = +req.body.patient_visit_id;
    const inputData = req.body as FeedbackCreateDto;
    const ajv       = new Ajv();
    const schema    = JSON.parse(fs.readFileSync("./schema/feedback/feedback-create.schema.json").toString());
    try {
      await ajv.validate(schema, inputData);
    } catch (e) {
      throw new UnprocessableEntityException(e);
    }
    const patientVisit = await patientVisitService.show(inputData.patient_visit_id);
    if (!patientVisit) {
      throw new PatientVisitNotFoundException();
    }
    await feedbackService.create(inputData.feedback, patientVisit, FeedbackFiller.DOCTOR);
    await patientVisit.update({
      is_doctor_feedback: true
    });
    return res.json("success");
  }

  static async syncDoctorFeedback(req: Request, res: Response) {
    req.body.feedbacks = JSON.parse(req.body.feedbacks);
    const inputData    = req.body as FeedbackBulkCreateDto;
    const ajv          = new Ajv();
    const schema       = JSON.parse(fs.readFileSync("./schema/feedback/feedback-bulk-create.schema.json").toString());
    try {
      await ajv.validate(schema, inputData);
    } catch (e) {
      throw new UnprocessableEntityException(e);
    }
    for (const data of inputData.feedbacks) {
      const patientVisit = await patientVisitService.showByAppointmentClinicDoctorAndPatient(data.patient_id, data.clinic_doctor_id, data.appointment_time);
      if (!patientVisit) {
        throw new UnauthorizedException(
          "Unable to find patient visit with id " + data.patient_id + ". Aborting the sync", 404);
      }
      await feedbackService.create(data.feedback, patientVisit, FeedbackFiller.DOCTOR);
      await patientVisit.update({
        is_doctor_feedback: true
      });
    }
    return res.json("success");
  }

  static async createEmployeeFeedback(req: Request, res: Response) {
    req.body.patient_visit_id = +req.body.patient_visit_id;
    req.body.feedback = JSON.parse(req.body.feedback) as FeedbackDto;
    const inputData   = req.body as FeedbackCreateDto;
    const ajv         = new Ajv();
    const schema      = JSON.parse(fs.readFileSync("./schema/feedback/feedback-create.schema.json").toString());
    try {
      await ajv.validate(schema, inputData);
    } catch (e) {
      throw new UnprocessableEntityException(e);
    }
    const patientVisit = await patientVisitService.show(inputData.patient_visit_id);
    if (!patientVisit) {
      throw new PatientVisitNotFoundException();
    }
    await feedbackService.create(inputData.feedback, patientVisit, FeedbackFiller.EMPLOYEE);
    await patientVisit.update({
      is_doctor_feedback: true,
      is_patient_feedback: true
    });
    return res.json("success");
  }

  static async viewPatientVisitFeedback(req: Request, res: Response) {
    const patientVisit = await patientVisitService.show(+req.params.patientVisitId);
    if (!patientVisit) {
      throw new PatientVisitNotFoundException();
    }
    const feedbacks = await feedbackService.viewByPatientVisit(patientVisit.id);
    return res.json({
      data: feedbacks
    });
  }
}
