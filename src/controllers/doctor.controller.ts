import { NextFunction, Request, Response } from "express";
import jwt from "jsonwebtoken";
import { UnauthorizedException } from "../exceptions/root/unauthorized.exception";
import { UnprocessableEntityException } from "../exceptions/root/unprocessable-entity.exception";
import { dbService } from "../services/db.service";
import Ajv from "ajv";
import * as fs from "fs";
import { doctorService } from "../services/entities/doctor.service";
import { DoctorNotFoundException } from "../exceptions/user/doctor-not-found.exception";
import { DoctorTransformer } from "../transformers/doctor.transformer";
import { PreUser } from "../models/pre-user.model";
import { compareSync } from "bcrypt";
import { IncorrectOtpException } from "../exceptions/user/incorrect-otp.exception";
import { DoctorUpdateDto } from "../dtos/doctor/doctor-update.dto";
import { DoctorNotVerifiedException } from "../exceptions/user/doctor-not-verified.exception";
import { DoctorAlreadyExistsException } from "../exceptions/user/doctor-already-exists.exception";
import { clinicService } from "../services/entities/clinic.service";
import { ClinicNotLinkedException } from "../exceptions/clinic/clinic-not-linked.exception";
import { TimingCreateDto } from "../dtos/doctor/timing-create.dto";
import { ClinicDoctorNestTransformer, ClinicDoctorTransformer } from "../transformers/clinic-doctor.transformer";
import { twilioService } from "../services/factories/twilio.service";
import { DoctorCreateDto } from "../dtos/doctor/doctor-create.dto";
import { DoctorSignupDto } from "../dtos/doctor/doctor-signup.dto";
import { ClinicNotFoundException } from "../exceptions/clinic/clinic-not-found.exception";
import { Helpers } from "../util/helpers.util";
import { Timing } from "../models/timing.model";
import toBoolean from "validator/lib/toBoolean";
import { ClinicDoctorTimingUpdateDto, ClinicDoctorUpdateDto } from "../dtos/doctor/clinic-doctor-update.dto";
import { EmailAlreadyExistsException } from "../exceptions/user/email-already-exists.exception";
import { PhoneNumberAlreadyExistsException } from "../exceptions/user/phone-number-already-exists.exception";
import { s3Service } from "../services/factories/s3.service";
import { ENV_S3_BASE_URL, ENV_S3_BUCKET } from "../util/secrets.util";
import { DoctorAlreadyLinkedException } from "../exceptions/user/doctor-already-linked.exception";
import { DoctorNotLinkedException } from "../exceptions/user/doctor-not-linked.exception";
import { ImageNotFoundException } from "../exceptions/user/image-not-found.exception";
import moment from "moment";
import { addressService } from "../services/entities/address.service";
import { CityNotFoundException } from "../exceptions/address/city-not-found.exception";
import { StateNotFoundException } from "../exceptions/address/state-not-found.exception";
import { FrontDeskNotVerifiedException } from "../exceptions/user/front-desk-not-verified.exception";

export class DoctorController {

  static async show(req: Request, res: Response) {
    const userId = +req.params.userId;
    const doctor = await doctorService.show(userId);

    if (!doctor) {
      throw new DoctorNotFoundException();
    }

    return res.json({
      data: await new DoctorTransformer().transform(doctor)
    });
  }

  static async showAllDoctors(req: Request, res: Response) {
    const doctors = await doctorService.showAll();
    return res.json({
      data: await new DoctorTransformer().transformList(doctors)
    });
  }

  static async indexDoctors(req: Request, res: Response) {
    const filters = req.query as { query?: string };
    const doctors = await doctorService.index(filters);
    return res.json({
      data: await new DoctorTransformer().transformList(doctors)
    });
  }

  static async createDoctor(req: Request, res: Response) {
    const inputData             = req.body as DoctorCreateDto;
    inputData.age               = +inputData.age;
    inputData.clinic_id         = +inputData.clinic_id;
    inputData.consultation_fee  = +inputData.consultation_fee;
    inputData.emergency_fee     = +inputData.emergency_fee;
    inputData.online_charge     = +inputData.online_charge;
    inputData.paid_visit_charge = +inputData.paid_visit_charge;
    if (inputData.free_visit_charge) {
      inputData.free_visit_charge = +inputData.free_visit_charge;
    }
    inputData.on_call_paid_visit_charge = +inputData.on_call_paid_visit_charge;
    if (inputData.on_call_free_visit_charge) {
      inputData.on_call_free_visit_charge = +inputData.on_call_free_visit_charge;
    }
    if (inputData.follow_up_appointments) {
      inputData.follow_up_appointments = +inputData.follow_up_appointments;
    }
    if (inputData.follow_up_days) {
      inputData.follow_up_days = +inputData.follow_up_days;
    }
    inputData.slot_time = +inputData.slot_time;
    for (const timing of inputData.doctor_timings) {
      for (const slot of timing.slots) {
        slot.no_of_patients = +slot.no_of_patients;
      }
    }

    const clinic = await clinicService.show(inputData.clinic_id);
    if (!clinic) {
      throw new ClinicNotFoundException();
    }
    const ajv    = new Ajv();
    const schema = JSON.parse(fs.readFileSync("./schema/doctor/doctor-create.schema.json").toString());
    try {
      await ajv.validate(schema, inputData);
    } catch (e) {
      Helpers.handleError(res, new UnprocessableEntityException(e));
    }

    if (inputData.email) {
      let doctor = await doctorService.showUserByEmail(inputData.email);
      if (doctor) {
        throw new EmailAlreadyExistsException();
      }
      doctor = await doctorService.showUserByMobile(inputData.mobile_no);
      if (doctor) {
        throw new PhoneNumberAlreadyExistsException();
      }
    }

    const transaction = await dbService.getSequelize().transaction();
    try {
      let doctor;
      if (inputData.get_cure_code) {
        doctor = await doctorService.showDoctorByCode(inputData.get_cure_code);
        if (!doctor) {
          await transaction.rollback();
          Helpers.handleError(res, new DoctorNotFoundException());
        }
      } else {
        if (!inputData.image_url) {
          if (inputData.gender === "male") {
            inputData.image_url = "https://getcure-uploads.s3.ap-south-1.amazonaws.com/doctor/male.jpg";
          } else {
            inputData.image_url = "https://getcure-uploads.s3.ap-south-1.amazonaws.com/doctor/female.jpg";
          }
        }
        const data = {
          image_url   : inputData.image_url,
          name        : inputData.name,
          age         : inputData.age,
          gender      : inputData.gender,
          degree      : inputData.degree,
          mobile_no   : inputData.mobile_no,
          email       : inputData.email,
          specialities: inputData.specialities,
          language    : inputData.language,
          experience  : inputData.experience,
          designation : inputData.designation,
          password    : inputData.password,
          holidays    : inputData.holidays
        };
        doctor     = await doctorService.addDoctor(data, transaction);
      }
      const previousClinicDoctor = await clinicService.showClinicDoctor(inputData.clinic_id, doctor.id);
      if (previousClinicDoctor) {
        await transaction.rollback();
        Helpers.handleError(res, new DoctorAlreadyLinkedException());
      }
      const clinicDoctorData = {
        doctor_id                : doctor.id,
        doctor_name              : doctor.name,
        clinic_id                : inputData.clinic_id,
        consultation_fee         : inputData.consultation_fee,
        emergency_fee            : inputData.emergency_fee,
        online_charge            : inputData.online_charge,
        paid_visit_charge        : inputData.paid_visit_charge,
        free_visit_charge        : inputData.free_visit_charge,
        on_call_paid_visit_charge: inputData.on_call_paid_visit_charge,
        on_call_free_visit_charge: inputData.on_call_free_visit_charge,
        follow_up_days           : inputData.follow_up_days,
        follow_up_appointments   : inputData.follow_up_appointments,
        slot_time                : inputData.slot_time,
        doctor_timings           : inputData.doctor_timings,

      };
      const clinicDoctor     = await clinicService.addClinicDoctor(clinicDoctorData, transaction);
      await transaction.commit();
      return res.json({
        data: await new ClinicDoctorTransformer().transform(clinicDoctor)
      });
    } catch (e) {
      await transaction.rollback();
      throw new UnprocessableEntityException(e);
    }
  }

  static async updateDoctor(req: Request, res: Response) {
    if (req.body.is_active) {
      req.body.is_active = toBoolean(req.body.is_active);
    }
    const inputData = req.body as DoctorUpdateDto;
    if (inputData.age) {
      inputData.age = +inputData.age;
    }

    const ajv    = new Ajv();
    const schema = JSON.parse(fs.readFileSync("./schema/doctor/doctor-update.schema.json").toString());
    try {
      await ajv.validate(schema, inputData);
    } catch (e) {
      throw new UnprocessableEntityException(e);
    }
    const doctor = await doctorService.show(+req.params.doctorId);
    if (!doctor) {
      throw new DoctorNotFoundException();
    }
    const update = await doctorService.update(doctor, inputData);
    return res.json({
      data: await new DoctorTransformer().transform(update)
    });
  }

  static async updateClinicDoctor(req: Request, res: Response) {
    const inputData    = req.body as ClinicDoctorUpdateDto;
    const clinicDoctor = await clinicService.showClinicDoctor(inputData.clinic_id, +req.params.doctorId);
    if (!clinicDoctor) {
      throw new ClinicNotLinkedException();
    }
    const ajv    = new Ajv();
    const schema = JSON.parse(fs.readFileSync("./schema/doctor/clinic-doctor-update.schema.json").toString());
    try {
      await ajv.validate(schema, inputData);
    } catch (e) {
      Helpers.handleError(res, new UnprocessableEntityException(e));
    }

    const update = await clinicService.updateClinicDoctor(clinicDoctor, inputData);
    return res.json({
      data: await new ClinicDoctorTransformer().transform(update)
    });
  }

  static async updateMyClinicDoctorFees(req: Request, res: Response) {
    const inputData    = req.body as {consultation_fee?: number; emergency_fee?: number};
    if (inputData.consultation_fee) {
      inputData.consultation_fee = +inputData.consultation_fee;
    }
    if (inputData.emergency_fee) {
      inputData.emergency_fee = +inputData.emergency_fee;
    }
    const clinicDoctor = await clinicService.showClinicDoctorById(+req.params.clinicDoctorId);
    if (!clinicDoctor || clinicDoctor.doctor_id !== req.doctor_id) {
      throw new ClinicNotLinkedException();
    }

    const update = await clinicService.updateClinicDoctorFees(clinicDoctor, inputData);
    return res.json({
      data: await new ClinicDoctorTransformer().transform(update)
    });
  }


  static async loginDoctor(req: Request, res: Response, next: NextFunction) {
    const inputData = req.body as { emailOrPhone: string, password: string };
    let doctor;
    if (inputData.emailOrPhone.indexOf("@") !== -1) {
      doctor = await doctorService.showUserByEmail(inputData.emailOrPhone, true);
    } else {
      doctor = await doctorService.showUserByMobile(inputData.emailOrPhone, true);
    }
    if (!doctor) {
      throw new DoctorNotFoundException();
    }
    const isPasswordCorrect = compareSync(inputData.password, doctor.password);

    if (!isPasswordCorrect) {
      res.status(403);
      return res.json({
        message: "Wrong Password"
      });
    }
    return res.json({
      token: jwt.sign({doctor_id: doctor.id, doctor_name: doctor.name}, "secret"),
      data : await (new DoctorTransformer()).transform(doctor),
    });

  }


  static async signup(req: Request, res: Response, next: NextFunction) {
    const inputData = req.body as DoctorSignupDto;

    const preUser = await PreUser.findOne({
      where: {
        mobile_no: inputData.mobile_no
      }
    });

    if (!preUser) {
      throw new UnauthorizedException("Kindly Request for OTP first", 401);
    }
    const ajv    = new Ajv();
    const schema = JSON.parse(fs.readFileSync("./schema/doctor/doctor-signup.schema.json").toString());
    try {
      const valid = await ajv.validate(schema, inputData);
    } catch (e) {
      throw new UnprocessableEntityException(e.errors);
    }
    if (preUser.otp != inputData.otp) {
      throw new IncorrectOtpException();
    }
    const transaction = await dbService.getSequelize().transaction();
    try {
      const doctor = await doctorService.create(inputData, transaction);
      await preUser.destroy({transaction});
      await doctor.update({
        get_cure_code: "GCD" + (1000 + doctor.id)
      }, {transaction});
      await transaction.commit();
      return res.json({
        token: jwt.sign({doctor_id: doctor.id, doctor_name: doctor.name}, "secret", {algorithm: "HS256"}),
        data : await new DoctorTransformer().transform(doctor),
      });
    } catch (e) {
      await transaction.rollback();
      throw new UnprocessableEntityException(e);
    }
  }

  static async generateOtp(req: Request, res: Response) {
    const inputData = req.body as { mobile_no: string };
    const ajv       = new Ajv();
    const schema    = JSON.parse(fs.readFileSync("./schema/doctor/pre-user-create.schema.json").toString());
    const valid     = await ajv.validate(schema, inputData);
    if (!valid) {
      console.log("hello");
      throw new UnprocessableEntityException(ajv.errors);
    }
    const user = await doctorService.showUserByMobile(inputData.mobile_no);
    if (user) {
      throw new DoctorAlreadyExistsException();
    }
    const preUser = await doctorService.preSignup(inputData);
    // todo: sendOtp
    const send    = await twilioService.sendText(preUser.mobile_no, preUser.otp);
    return res.json(preUser.otp);
  }

  static async showMyDoctorProfile(req: Request, res: Response) {
    const doctor = await doctorService.show(req.doctor_id, true);
    return res.json({
      data: await (new DoctorTransformer()).transform(doctor),
    });
  }

  static async showByState(req: Request, res: Response) {
    const filters = req.query as { state_id: string; query?: string };

    const state = await addressService.findState(+filters.state_id);
    if (!state) {
      throw new StateNotFoundException();
    }
    const doctors = await doctorService.showByCity(filters);
    return res.json({
      data: await new ClinicDoctorNestTransformer(filters).transformList(doctors, ["doctor"])
    });
  }

  static async updateMyDoctorProfile(req: Request, res: Response) {
    const doctor = await doctorService.show(req.doctor_id);
    const body   = req.body as DoctorUpdateDto;
    if (req.body.holidays) {
      req.body.holidays = JSON.parse(req.body.holidays);
    }
    if (req.body.specialities) {
      req.body.specialities = JSON.parse(req.body.specialities);
    }
    if (body.age) {
      body.age = +body.age;
    }
    const ajv    = new Ajv();
    const schema = JSON.parse(fs.readFileSync("./schema/doctor/doctor-update.schema.json").toString());
    const valid  = await ajv.validate(schema, body);
    if (!valid) {
      throw new UnprocessableEntityException(ajv.errors);
    }
    let updatedDoctor = await doctorService.update(doctor, body);
    updatedDoctor = await doctorService.show(updatedDoctor.id, true);
    return res.json({
      data: await (new DoctorTransformer()).transform(updatedDoctor)
    });
  }

  static async updateMyClinicTimings(req: Request, res: Response) {
    const inputData      = JSON.parse(req.body.doctor_timings) as ClinicDoctorTimingUpdateDto;
    const clinicDoctorId = +req.params.clinicDoctorId;
    const clinicDoctor   = await clinicService.showClinicDoctorById(clinicDoctorId);
    if (!clinicDoctor) {
      throw new ClinicNotLinkedException();
    }
    clinicDoctor.doctor = null;
    const timings = await clinicService.updateClinicDoctorTiming(clinicDoctor, inputData.doctor_timings);
    return res.json({
      data: await new ClinicDoctorTransformer().transform(timings)
    });
  }

  static async updateClinicTimings(req: Request, res: Response) {
    const inputData    = req.body as any;
    const json         = JSON.parse(inputData.doctor_timings) as ClinicDoctorTimingUpdateDto;
    const clinicDoctor = await clinicService.showClinicDoctorById(+req.params.clinicDoctorId);
    if (!clinicDoctor) {
      throw new ClinicNotLinkedException();
    }
    const timings = await clinicService.updateClinicDoctorTiming(clinicDoctor, json.doctor_timings);
    return res.json({
      data: await new ClinicDoctorTransformer().transform(timings)
    });
  }

  static async updateDoctorByFD(req: Request, res: Response) {
    if (req.body.holidays) {
      req.body.holidays = JSON.parse(req.body.holidays);
    }
    const inputData = req.body as DoctorUpdateDto;
    if (inputData.age) {
      inputData.age = +inputData.age;
    }
    const ajv    = new Ajv();
    const schema = JSON.parse(fs.readFileSync("./schema/doctor/doctor-update.schema.json").toString());
    try {
      await ajv.validate(schema, inputData);
    } catch (e) {
      throw new UnprocessableEntityException(e);
    }
    const doctor = await doctorService.show(+req.params.doctorId);
    if (!doctor) {
      throw new DoctorNotFoundException();
    }
    const update = await doctorService.update(doctor, inputData);
    return res.json({
      data: await new DoctorTransformer().transform(update)
    });
  }

  static async deleteMyDoctorProfile(req: Request, res: Response) {
    const doctor = await doctorService.show(req.doctor_id);
    await doctorService.delete(doctor);
  }

  static async deleteDoctorFromClinic(req: Request, res: Response) {
    const doctor = await clinicService.showClinicDoctorById(+req.params.doctorId);
    if (!doctor) {
      throw new DoctorNotLinkedException();
    }
    await clinicService.deleteClinicDoctor(doctor);
    return res.json({
      data: "Doctor Removed Successfully From this Clinic"
    });
  }

  static async uploadImage(req: Request, res: Response) {
    const image = req.file;
    if (!image) {
      throw new ImageNotFoundException();
    }
    const now = moment.now();
    await s3Service.uploadToS3(image.buffer, now + image.originalname, req.params.type);
    const image_url = ENV_S3_BASE_URL + `${req.params.type}/` + now + image.originalname;
    return res.json({
      data: image_url
    });
  }

  // static async uploadIdVerification(req: Request, res: Response) {
  //   const image = req.file;
  //   if (!image) {
  //     throw new ImageNotFoundException();
  //   }
  //   const doctor = await doctorService.show(+req.doctor_id);
  //   const now = moment.now();
  //   await s3Service.uploadToS3(image.buffer, now + image.originalname, "verification");
  //   const image_url = ENV_S3_BASE_URL + `verification/` + now + image.originalname;
  //   await doctor.update({
  //     identity_verification_url: image_url
  //   });
  //   return res.json({
  //     data: image_url
  //   });
  // }

}
