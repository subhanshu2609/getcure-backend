import { Request, Response } from "express";
import { ClinicCreateDto } from "../dtos/clinic/clinic-create.dto";
import Ajv from "ajv";
import * as fs from "fs";
import { UnprocessableEntityException } from "../exceptions/root/unprocessable-entity.exception";
import { clinicService } from "../services/entities/clinic.service";
import { dbService } from "../services/db.service";
import { ClinicDoctor } from "../models/clinic-doctor.model";
import { ClinicTransformer } from "../transformers/clinic.transformer";
import { ClinicNotFoundException } from "../exceptions/clinic/clinic-not-found.exception";
import { doctorService } from "../services/entities/doctor.service";
import { DoctorNotFoundException } from "../exceptions/user/doctor-not-found.exception";
import { ClinicDoctorTransformer } from "../transformers/clinic-doctor.transformer";
import { addressService } from "../services/entities/address.service";
import { CityNotFoundException } from "../exceptions/address/city-not-found.exception";
import toBoolean from "validator/lib/toBoolean";
import { ClinicUpdateDto } from "../dtos/clinic/clinic-update.dto";
import { Helpers } from "../util/helpers.util";
import { InvalidJwtTokenException } from "../exceptions/invalid-jwt-token.exception";
import { StateNotFoundException } from "../exceptions/address/state-not-found.exception";
import { State } from "../models/state.model";
import { ClinicNotLinkedException } from "../exceptions/clinic/clinic-not-linked.exception";
import { ClinicDoctorInstantUpdateDto } from "../dtos/doctor/clinic-doctor-instant-update.dto";
import { Doctor } from "../models/doctor.model";
import { Clinic } from "../models/clinic.model";

export class ClinicController {

  static async indexClinics(req: Request, res: Response) {
    const filters = req.query as { query?: string };
    const clinics = await clinicService.index(filters);
    return res.json({
      data: await new ClinicTransformer().transformList(clinics)
    });
  }

  static async indexClinicsByState(req: Request, res: Response) {
    const filters = req.query as { query?: string };
    const clinics = await clinicService.showByState(+req.params.stateId, filters);
    return res.json({
      data: await new ClinicTransformer().transformList(clinics)
    });
  }

  static async showClinic(req: Request, res: Response) {
    const clinics = await clinicService.show(+req.params.clinicId);
    return res.json({
      data: await new ClinicTransformer().transform(clinics)
    });
  }

  static async showClinicDoctor(req: Request, res: Response) {
    const clinicDoctor = await clinicService.showClinicDoctor(+req.params.clinicId, +req.params.doctorId, true);
    if (!clinicDoctor) {
      throw new ClinicNotFoundException();
    }
    return res.json({
      data: await new ClinicDoctorTransformer().transform(clinicDoctor)
    });
  }

  static async createClinic(req: Request, res: Response) {
    req.body.state_id = +req.body.state_id;
    if (req.body.no_of_beds) {
      req.body.no_of_beds = +req.body.no_of_beds;
    }
    if (req.body.no_of_doctors) {
      req.body.no_of_doctors = +req.body.no_of_doctors;
    }
    const inputData = req.body as ClinicCreateDto;
    const ajv       = new Ajv();
    const schema    = JSON.parse(fs.readFileSync("./schema/clinic/clinic-create.schema.json").toString());
    try {
      const valid = await ajv.validate(schema, inputData);
    } catch (e) {
      throw new UnprocessableEntityException(e.errors);
    }
    const city = await addressService.findState(inputData.state_id);
    if (!city) {
      throw new StateNotFoundException();
    }
    inputData.state = city.title;
    const clinic    = await clinicService.createClinic(inputData);
    return res.json({
      data: await new ClinicTransformer().transform(clinic)
    });
  }

  static async updateClinic(req: Request, res: Response) {
    if (req.body.no_of_beds) {
      req.body.no_of_beds = +req.body.no_of_beds;
    }
    if (req.body.no_of_doctors) {
      req.body.no_of_doctors = +req.body.no_of_doctors;
    }
    if (req.body.state_id) {
      req.body.state_id = +req.body.state_id;
      const city        = await addressService.findState(+req.body.state_id);
      if (!city) {
        throw new StateNotFoundException();
      }
      req.body.state = city.title;
    }
    if (req.body.is_active) {
      req.body.is_active = toBoolean(req.body.is_active);
    }

    const inputData = req.body as ClinicUpdateDto;
    const ajv       = new Ajv();
    const schema    = JSON.parse(fs.readFileSync("./schema/clinic/clinic-update.schema.json").toString());
    try {
      await ajv.validate(schema, inputData);
    } catch (e) {
      return Helpers.handleError(res, new UnprocessableEntityException(e));
    }

    const clinic = await clinicService.show(+req.params.clinicId);

    const updatedClinic = await clinicService.update(clinic, inputData);
    return res.json({
      data: await new ClinicTransformer().transform(updatedClinic)
    });
  }

  static async addClinic(req: Request, res: Response) {
    req.body.state_id = +req.body.state_id;
    if (req.body.no_of_beds) {
      req.body.no_of_beds = +req.body.no_of_beds;
    }
    if (req.body.no_of_doctors) {
      req.body.no_of_doctors = +req.body.no_of_doctors;
    }
    if (req.body.specialities) {
      req.body.specialities = JSON.parse(req.body.specialities);
    }
    const inputData = req.body as ClinicCreateDto;
    const ajv       = new Ajv();
    const schema    = JSON.parse(fs.readFileSync("./schema/clinic/clinic-create.schema.json").toString());
    try {
      const valid = await ajv.validate(schema, inputData);
    } catch (e) {
      throw new UnprocessableEntityException(e.errors);
    }
    const transaction = await dbService.getSequelize().transaction();
    try {
      const clinic = await clinicService.createClinic(inputData, transaction);
      console.log(req.doctor_name);
      await clinicService.createClinicDoctor(clinic.id, req.doctor_id, req.doctor_name, transaction);
      await transaction.commit();
      return res.json({
        data: await new ClinicTransformer().transform(clinic)
      });
    } catch (e) {
      await transaction.rollback();
      throw new UnprocessableEntityException(e);
    }
  }

  static async addClinicByCode(req: Request, res: Response) {
    const clinic_code = req.body.clinic_code;
    const clinic      = await clinicService.showClinicByCode(clinic_code);
    if (!clinic) {
      throw new ClinicNotFoundException();
    }
    await clinicService.createClinicDoctor(clinic.id, req.doctor_id, req.doctor_name);
    return res.json({
      data: await new ClinicTransformer().transform(clinic)
    });
  }

  static async clinicsByDoctor(req: Request, res: Response) {
    const doctor = await doctorService.show(+req.params.doctorId);
    if (!doctor) {
      throw new DoctorNotFoundException();
    }
    const clinicDoctors = await clinicService.showAllClinicDoctorByDoctor(doctor.id);
    return res.json({
      data: await new ClinicDoctorTransformer().transformList(clinicDoctors)
    });
  }

  static async doctorsOfClinic(req: Request, res: Response) {
    const clinic = await clinicService.show(+req.params.clinicId);
    if (!clinic) {
      throw new ClinicNotFoundException();
    }
    const clinicDoctors = await clinicService.showAllClinicDoctorByClinic(clinic.id);
    return res.json({
      data: await new ClinicDoctorTransformer().transformList(clinicDoctors)
    });
  }

  static async deleteClinic(req: Request, res: Response) {
    const clinic = await clinicService.show(+req.params.clinicId);
    if (!clinic) {
      throw new ClinicNotFoundException();
    }
    await clinicService.delete(clinic);
    return res.json({data: "Clinic Deleted Successfully"});
  }

  static async showCDOfDoctor(req: Request, res: Response) {
    const doctor = await doctorService.show(+req.params.doctorId);
    if (!doctor) {
      throw new DoctorNotFoundException();
    }
    const clinic = await ClinicDoctor.findOne({
      where: {
        doctor_id: doctor.id
      },
      order: [
        [
          "id",
          "asc"
        ]
      ]
    });
    return res.json({
      data: await new ClinicDoctorTransformer().transform(clinic)
    });
  }

  static async instantUpdate(req: Request, res: Response) {
    const clinicDoctor = await clinicService.showClinicDoctorById(+req.params.clinicDoctorId);
    if (!clinicDoctor) {
      throw new ClinicNotLinkedException();
    }
    const inputData = req.body as ClinicDoctorInstantUpdateDto;
    const ajv       = new Ajv();
    const schema    = JSON.parse(fs.readFileSync("./schema/doctor/clinic-doctor-instant-update.schema.json").toString());
    try {
      const valid = await ajv.validate(schema, inputData);
    } catch (e) {
      throw new UnprocessableEntityException(e.errors);
    }
    if (inputData.name || inputData.email || inputData.mobile_no) {
      await clinicDoctor.doctor.update(inputData);
    }
    let cd = clinicDoctor;
    if (inputData.name || inputData?.clinic_id) {
      if (inputData.clinic_id) {
        const clinic = await Clinic.findOne({
          where: {
            id: inputData.clinic_id
          }
        });
        if (!clinic) {
          throw new ClinicNotFoundException();
        }
      }
      cd = await clinicDoctor.update({
        doctor_name: inputData.name ? inputData.name : clinicDoctor.doctor_name,
        ...inputData
      });
    }
    return res.json({
      data: await new ClinicDoctorTransformer().transform(cd)
    });
  }
}
