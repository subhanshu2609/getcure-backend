import { NextFunction, Request, Response } from "express";
import Ajv from "ajv";
import { addressService } from "../services/entities/address.service";
import { CityTransformer } from "../transformers/city.transformer";
import { StateTransformer } from "../transformers/state.transformer";
import { StateCreateDto } from "../dtos/state/state-create.dto";
import { CityCreateDto } from "../dtos/state/city-create.dto";
import { StateNotFoundException } from "../exceptions/address/state-not-found.exception";

export class AddressController {

  static async listStates(req: Request, res: Response) {
    const states = await addressService.listStates();
    return res.json({
      data: await new StateTransformer().transformList(states)
    });
  }


  static async listActiveStates(req: Request, res: Response) {
    const filters = req.query as { query?: string };
    const states  = await addressService.listActiveStates(filters);
    return res.json({
      data: await new StateTransformer().transformList(states)
    });
  }

  static async listCities(req: Request, res: Response) {
    const stateId = +req.params.stateId;
    const cities  = await addressService.listCities(stateId);
    return res.json({
      data: await new CityTransformer().transformList(cities)
    });
  }

  static async addState(req: Request, res: Response) {
    const inputData = req.body as StateCreateDto;
    const state     = await addressService.addState(inputData);
    return res.json({
      data: await new StateTransformer().transform(state)
    });
  }

  static async addCity(req: Request, res: Response) {
    req.body.state_id = +req.body.state_id;
    const inputData   = req.body as CityCreateDto;
    const state       = await addressService.findState(+inputData.state_id);
    if (!state) {
      throw new StateNotFoundException();
    }
    const city = await addressService.addCity(inputData);
    return res.json({
      data: await new CityTransformer().transform(city)
    });
  }

  static async deleteState(req: Request, res: Response) {
    const state = await addressService.findState(+req.params.stateId);
    if (!state) {
      throw new StateNotFoundException();
    }
    await addressService.deleteAddress(state);
    return res.json({
      data: "State Deleted Successfully"
    });
  }
}

