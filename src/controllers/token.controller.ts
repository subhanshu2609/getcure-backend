import { Request, Response } from "express";
import Ajv from "ajv";
import * as fs from "fs";
import { UnprocessableEntityException } from "../exceptions/root/unprocessable-entity.exception";
import { dbService } from "../services/db.service";
import { TokenCreateDto } from "../dtos/token/token-create.dto";
import { tokenService } from "../services/entities/token.service";
import { patientService } from "../services/entities/patient.service";
import { PatientNotFoundException } from "../exceptions/patient/patient-not-found.exception";
import { TokenFullTransformer, TokenTransformer } from "../transformers/token.transformer";
import { TokenNotFoundException } from "../exceptions/token/token-not-found.exception";
import toBoolean from "validator/lib/toBoolean";
import { Token } from "../models/token.model";
import { clinicService } from "../services/entities/clinic.service";
import { ClinicNotLinkedException } from "../exceptions/clinic/clinic-not-linked.exception";
import { AppointmentTypes, BookingType, VisitType } from "../enums/token.enum";
import { UnauthorizedException } from "../exceptions/root/unauthorized.exception";
import { TokenAlreadyBookedException } from "../exceptions/token/token-already-booked.exception";
import { Sequelize } from "sequelize";
import { PatientVisit } from "../models/patient-visit.model";
import moment from "moment";
import { PatientVisitTransformer } from "../transformers/patient-visit.transformer";
import { Bookie } from "../enums/bookie.enum";
import { ClinicDoctor } from "../models/clinic-doctor.model";
import { Clinic } from "../models/clinic.model";
import { Op } from "sequelize";
import { EmployeeNotFoundException } from "../exceptions/employee/employee-not-found.exception";

export class TokenController {
  static async addToken(req: Request, res: Response) {
    if (req.body.patient) {
      req.body.patient.age = +req.body.patient.age;
    }
    if (req.body.token_no) {
      req.body.token_no = +req.body.token_no;
    }
    if (req.body.is_present) {
      req.body.is_present = toBoolean(req.body.is_present);
    }
    req.body.fees   = +req.body.fees;
    const inputData = req.body as TokenCreateDto;
    const ajv       = new Ajv();
    const schema    = JSON.parse(fs.readFileSync("./schema/token/token-create.schema.json").toString());
    try {
      const valid = await ajv.validate(schema, inputData);
    } catch (e) {
      throw new UnprocessableEntityException(e.errors);
    }
    const oldToken = await Token.findOne({
      where: {
        date            : inputData.date,
        time            : inputData.time,
        clinic_doctor_id: inputData.clinic_doctor_id,
        booking_type    : {[Sequelize.Op.ne]: BookingType.CANCELLED}
      }
    });
    if (oldToken) {
      throw new TokenAlreadyBookedException();
    }
    if (inputData.patient_id) {
      const patient = await patientService.show(inputData.patient_id);
      if (!patient) {
        throw new PatientNotFoundException();
      }
      const token = await tokenService.createToken(inputData, patient.patient_id, patient.name, req.frontDesk.name);
      return res.json({
        data: await new TokenTransformer().transform(token)
      });
    }
    const transaction = await dbService.getSequelize().transaction();
    try {
      const patient = await patientService.createPatient(inputData.patient, transaction);
      const token   = await tokenService.createToken(inputData, patient.patient_id, patient.name, req.frontDesk.name, transaction);
      await transaction.commit();
      return res.json({
        data: await new TokenTransformer().transform(token)
      });
    } catch (e) {
      await transaction.rollback();
      throw new UnprocessableEntityException(e);
    }
  }

  static async updateToken(req: Request, res: Response) {
    if (req.body.is_present) {
      req.body.is_present = toBoolean(req.body.is_present);
    }
    const inputData = req.body as { name?: string; age?: number; address?: string; gender?: string; is_present?: boolean };
    if (inputData.age) {
      inputData.age = +inputData.age;
    }
    const token = await tokenService.show(+req.params.tokenId);
    if (!token) {
      throw new TokenNotFoundException();
    }
    const patient    = await patientService.show(token.patient_id);
    const newPatient = await patient.update({...inputData});
    const newToken   = await token.update({
      patient_name: newPatient.name,
      is_present  : inputData.is_present ? inputData.is_present : false,
      present_time: inputData.is_present ? moment.utc().format("YYYY/MM/DD HH:mm:ss") : null
    });
    newToken.patient = newPatient;
    return res.json({
      data: await new TokenTransformer().transform(newToken)
    });
  }

  static async showTokensByDate(req: Request, res: Response) {
    const clinic_doctor_id = +req.params.clinicDoctorId;
    let tokens;
    const filters          = req.query as { date?: string; query?: string };
    if (filters.date) {
      tokens = await tokenService.showTokenByDate(clinic_doctor_id, req.query.date as string, filters, true);
    } else {
      tokens = await tokenService.showBookedTokens(clinic_doctor_id, filters, true);
    }
    return res.json({
      data: await new TokenTransformer().transformList(tokens, ["patient"])
    });
  }

  static async showBookedTokensBetweenDates(req: Request, res: Response) {
    const clinicDoctor = await clinicService.showClinicDoctorById(+req.params.clinicDoctorId);
    if (!clinicDoctor) {
      throw new ClinicNotLinkedException();
    }
    const tokens = await tokenService.showTokensBetweenDates(clinicDoctor.id, req.query.start_date as string, req.query.end_date as string);
    return res.json({
      data: await new TokenTransformer().transformList(tokens)
    });
  }

  static async showUnbookedTokens(req: Request, res: Response) {
    const clinicDoctor = await clinicService.showClinicDoctorById(+req.params.clinicDoctorId);
    if (!clinicDoctor) {
      throw new ClinicNotLinkedException();
    }
    const tokens = await tokenService.showUnbookedTokens(clinicDoctor, req.query.date as string);
    return res.json({
      data: tokens
    });
  }

  static async deleteToken(req: Request, res: Response) {
    // const tokenId   = +req.params.tokenId;
    req.body.clinic_doctor_id = +req.body.clinic_doctor_id;
    const inputData           = req.body as { clinic_doctor_id: number, date: Date, time: string };
    const token               = await tokenService.showByDateTime(inputData);
    if (!token) {
      throw new TokenNotFoundException();
    }
    await tokenService.delete(token);
    return res.json("success");
  }

  static async bookToken(req: Request, res: Response) {
    if (req.body.token_no) {
      req.body.token_no = +req.body.token_no;
    }
    req.body.clinic_doctor_id = +req.body.clinic_doctor_id;
    req.body.fees             = +req.body.fees;
    const inputData           = req.body as TokenCreateDto;
    const ajv                 = new Ajv();
    const schema              = JSON.parse(fs.readFileSync("./schema/token/token-create.schema.json").toString());
    try {
      await ajv.validate(schema, inputData);
    } catch (e) {
      throw new UnprocessableEntityException(e.errors);
    }
    if (!inputData.transaction_id) {
      throw new UnauthorizedException("Transaction ID Not Available", 505);
    }
    const oldToken = await Token.findOne({
      where: {
        date            : inputData.date,
        time            : inputData.time,
        clinic_doctor_id: inputData.clinic_doctor_id,
        booking_type    : {[Sequelize.Op.ne]: BookingType.CANCELLED}
      }
    });
    if (oldToken) {
      throw new TokenAlreadyBookedException();
    }
    const token = await tokenService.bookMyToken(inputData);
    return res.json({
      data: await new TokenTransformer().transform(token)
    });
  }

  static async bookTokenByEmployee(req: Request, res: Response) {
    if (req.body.token_no) {
      req.body.token_no = +req.body.token_no;
    }
    req.body.clinic_doctor_id = +req.body.clinic_doctor_id;
    req.body.fees             = +req.body.fees;
    if (req.body.patient) {
      req.body.patient.age = +req.body.patient.age;
    }
    const inputData = req.body as TokenCreateDto;
    const ajv       = new Ajv();
    const schema    = JSON.parse(fs.readFileSync("./schema/token/token-create.schema.json").toString());
    try {
      await ajv.validate(schema, inputData);
    } catch (e) {
      throw new UnprocessableEntityException(e.errors);
    }
    const oldToken = await Token.findOne({
      where: {
        date            : inputData.date,
        time            : inputData.time,
        clinic_doctor_id: inputData.clinic_doctor_id,
        booking_type    : {[Sequelize.Op.ne]: BookingType.CANCELLED}
      }
    });
    if (oldToken) {
      throw new TokenAlreadyBookedException();
    }
    let token;
    if (inputData.patient) {
      const patient          = await patientService.createPatient(inputData.patient);
      inputData.patient_id   = patient.patient_id;
      inputData.patient_name = patient.name;
      token                  = await tokenService.bookTokenByEmployee(inputData, req.employee.first_name + " " + req.employee.last_name);
    } else {
      token = await tokenService.bookTokenByEmployee(inputData, req.employee.first_name + " " + req.employee.last_name);
    }
    return res.json({
      data: await new TokenTransformer().transform(token)
    });
  }

  static async bookTokenByDoctor(req: Request, res: Response) {
    if (req.body.patient) {
      req.body.patient.age = +req.body.patient.age;
    }
    if (req.body.token_no) {
      req.body.token_no = +req.body.token_no;
    }
    if (req.body.is_present) {
      req.body.is_present = toBoolean(req.body.is_present);
    }
    req.body.fees   = +req.body.fees;
    const inputData = req.body as TokenCreateDto;
    const ajv       = new Ajv();
    const schema    = JSON.parse(fs.readFileSync("./schema/token/token-create.schema.json").toString());
    try {
      const valid = await ajv.validate(schema, inputData);
    } catch (e) {
      throw new UnprocessableEntityException(e.errors);
    }
    const oldToken = await Token.findOne({
      where: {
        date            : inputData.date,
        time            : inputData.time,
        clinic_doctor_id: inputData.clinic_doctor_id,
        booking_type    : {[Sequelize.Op.ne]: BookingType.CANCELLED}
      }
    });
    if (oldToken) {
      throw new TokenAlreadyBookedException();
    }
    if (inputData.patient_id) {
      const patient = await patientService.show(inputData.patient_id);
      if (!patient) {
        throw new PatientNotFoundException();
      }
      const token = await tokenService.createTokenByDoctor(inputData, patient.patient_id, patient.name, req.frontDesk.name);
      return res.json({
        data: await new TokenTransformer().transform(token)
      });
    }
    const transaction = await dbService.getSequelize().transaction();
    try {
      const patient = await patientService.createPatient(inputData.patient, transaction);
      const token   = await tokenService.createTokenByDoctor(inputData, patient.patient_id, patient.name, req.doctor_name, transaction);
      await transaction.commit();
      return res.json({
        data: await new TokenTransformer().transform(token)
      });
    } catch (e) {
      await transaction.rollback();
      throw new UnprocessableEntityException(e);
    }
  }

  static async cancelToken(req: Request, res: Response) {
    const token = await tokenService.show(+req.params.tokenId);
    if (!token) {
      throw new TokenNotFoundException();
    }
    if (req.patient[0].mobile_no !== token.patient.mobile_no) {
      throw new UnauthorizedException("Only Patient can cancel this token", 406);
    }
    const visit = await PatientVisit.create({
      clinic_doctor_id: token.clinic_doctor_id,
      patient_id      : token.patient_id,
      patient_name    : token.patient_name,
      appointment_type: token.appointment_type,
      visit_type      : token.visit_type,
      booking_type    : BookingType.CANCELLED,
      booked_at       : moment.utc(token.createdAt).format(),
      appointment_time: moment.utc(token.date + " " + token.time).format(),
      updated_by      : Bookie.PATIENT,
      fees            : 0
    });
    await token.destroy();
    return res.json({
      data: await new PatientVisitTransformer().transform(visit)
    });
  }

  static async cancelBulkTokens(req: Request, res: Response) {
    const inputData    = JSON.parse(req.body.tokens) as {
      patient_id: string; patient_name: string; date: Date; time: string; appointment_type: AppointmentTypes; visit_type: VisitType
    }[];
    const clinicDoctor = await clinicService.showClinicDoctorById(+req.params.clinicDoctorId);
    if (!clinicDoctor) {
      throw new ClinicNotLinkedException();
    }
    const visits: PatientVisit[] = [];
    for (const tokenData of inputData) {
      const token = await tokenService.showByDateTime({
        clinic_doctor_id: clinicDoctor.id,
        date            : tokenData.date,
        time            : tokenData.time
      });
      if (!token) {
        const visit = await PatientVisit.create({
          clinic_doctor_id: clinicDoctor.id,
          patient_id      : tokenData.patient_id,
          patient_name    : tokenData.patient_name,
          appointment_type: tokenData.appointment_type,
          visit_type      : tokenData.visit_type,
          booking_type    : BookingType.CANCELLED,
          booked_at       : moment.utc(token.createdAt).format(),
          appointment_time: moment.utc(token.date + " " + token.time).format(),
          updated_by      : Bookie.DOCTOR,
          fees            : 0
        });
        visits.push(visit);
      } else {
        const visit = await PatientVisit.create({
          clinic_doctor_id: token.clinic_doctor_id,
          patient_id      : token.patient_id,
          patient_name    : token.patient_name,
          appointment_type: token.appointment_type,
          visit_type      : token.visit_type,
          booking_type    : BookingType.CANCELLED,
          booked_at       : moment.utc(token.createdAt).format(),
          appointment_time: moment.utc(token.date + " " + token.time).format(),
          updated_by      : Bookie.DOCTOR,
          fees            : 0
        });
        await token.destroy();
        visits.push(visit);
      }
    }
    return res.json({
      data: await new PatientVisitTransformer().transformList(visits)
    });
  }

  static async cancelTokenByFD(req: Request, res: Response) {
    const token = await tokenService.show(+req.params.tokenId);
    if (!token) {
      throw new TokenNotFoundException();
    }
    const visit = await PatientVisit.create({
      clinic_doctor_id: token.clinic_doctor_id,
      patient_id      : token.patient_id,
      patient_name    : token.patient_name,
      appointment_type: token.appointment_type,
      visit_type      : token.visit_type,
      booking_type    : BookingType.CANCELLED,
      booked_at       : moment.utc(token.createdAt).format(),
      appointment_time: moment.utc(token.date + " " + token.time).format(),
      updated_by      : Bookie.FRONT_DESK,
      fees            : 0
    });
    await token.destroy();
    return res.json({
      data: await new PatientVisitTransformer().transform(visit)
    });
  }

  static async cancelTokenByEmployee(req: Request, res: Response) {
    const token = await tokenService.show(+req.params.tokenId);
    if (!token) {
      throw new TokenNotFoundException();
    }
    const visit = await PatientVisit.create({
      clinic_doctor_id: token.clinic_doctor_id,
      patient_id      : token.patient_id,
      patient_name    : token.patient_name,
      appointment_type: token.appointment_type,
      visit_type      : token.visit_type,
      booking_type    : BookingType.CANCELLED,
      booked_at       : moment.utc(token.createdAt).format(),
      appointment_time: moment.utc(token.date + " " + token.time).format(),
      updated_by      : Bookie.EMPLOYEE,
      fees            : 0
    });
    await token.destroy();
    return res.json({
      data: await new PatientVisitTransformer().transform(visit)
    });
  }

  static async viewAllTokens(req: Request, res: Response) {
    const tokens = await Token.findAll({
      order  : [
        [
          "date",
          "asc"
        ],
        [
          "time",
          "asc"
        ]
      ],
      include: [
        {
          model  : ClinicDoctor,
          include: [Clinic]
        }
      ]
    });
    return res.json({
      data: await new TokenFullTransformer().transformList(tokens, ["patient"])
    });
  }

  static async syncTokens(req: Request, res: Response) {
    const inputData    = JSON.parse(req.body.tokens) as TokenCreateDto[];
    const clinicDoctor = await clinicService.showClinicDoctorById(+req.params.clinicDoctorId);
    // const ajv          = new Ajv();
    // const schema       = JSON.parse(fs.readFileSync("./schema/token/token-sync.schema.json").toString());
    // try {
    //   const valid = await ajv.validate(schema, inputData);
    // } catch (e) {
    //   throw new UnprocessableEntityException(e.errors);
    // }
    // console.log(inputData);
    const dates       = inputData.map(t => t.date).filter((value, index, self) => self.indexOf(value) === index);
    const times       = inputData.map(t => t.time);
    const tokens      = await Token.findAll({
      where  : {
        date            : dates,
        time            : times,
        clinic_doctor_id: clinicDoctor.id,
        // booked_by       : {[Sequelize.Op.ne]: Bookie.DOCTOR}
      },
      include: [ClinicDoctor]
    });
    const transaction = await dbService.getSequelize().transaction();
    try {
      const changes = await tokenService.syncTokens(inputData, tokens, clinicDoctor, transaction);
      await transaction.commit();
      return res.json({
        data: await new TokenTransformer().transformList(changes)
      });
    } catch (e) {
      await transaction.rollback();
      throw new UnprocessableEntityException(e);
    }
  }

  static async viewCancelledTokens(req: Request, res: Response) {
    const filters = req.query as { date?: string };
    if (!filters.date) {
      filters.date = moment.utc().format("YYYY-MM-DD");
    }
    const clinicDoctor = await clinicService.showClinicDoctorById(+req.params.clinicDoctorId);
    if (!clinicDoctor) {
      throw new ClinicNotLinkedException();
    }
    const cancelledVisits = await PatientVisit.findAll({
      where: {
        appointment_time: {
          [Op.between]: [
            filters.date,
            moment.utc(filters.date).add(1, "day").format("YYYY-MM-DD")
          ]
        },
        booking_type    : BookingType.CANCELLED
      }
    });
    const tokens: {
      patient_id: string;
      patient_name: string;
      date: string;
      time: string;
      appointment_id: string;
      appointment_type: AppointmentTypes;
      visit_type: VisitType;
      booking_type: BookingType;
      fees: number;
      booked_at: Date;
      booked_by: Bookie;
      booked_via: string;
      present_time: Date;
    }[]                   = [];
    for (const visit of cancelledVisits) {
      // throw visit.appointment_time;
      const token = {
        patient_id      : visit.patient_id,
        patient_name    : visit.patient_name,
        date            : moment.utc(visit.appointment_time).format("YYYY-MM-DD"),
        time            : moment.utc(visit.appointment_time).format("HH:mm:ss"),
        appointment_id  : visit.appointment_id,
        appointment_type: visit.appointment_type,
        visit_type      : visit.visit_type,
        booking_type    : visit.booking_type,
        fees            : visit.fees,
        booked_at       : visit.booked_at,
        booked_by       : visit.booked_by,
        booked_via      : visit.booked_via,
        present_time    : visit.present_time
      };
      tokens.push(token);
    }
    return res.json({
      data: tokens
    });
  }

  static async viewTokensByEmployee(req: Request, res: Response) {
    const employee = req.employee;
    const filters  = req.query as { start_date?: string; end_date?: string; query?: string };
    if (!employee) {
      throw new EmployeeNotFoundException();
    }
    const visits = await tokenService.viewTokensOfEmployee(employee, filters);
    return res.json({
      data: await new TokenTransformer().transformList(visits, [
        "cds",
        "patient"
      ])
    });
  }
}
