import { Request, Response } from "express";
import { doctorService } from "../services/entities/doctor.service";
import { DoctorNotFoundException } from "../exceptions/user/doctor-not-found.exception";
import { ClinicDoctor } from "../models/clinic-doctor.model";
import { Recommendation } from "../models/recommendation.model";
import { recommendationService } from "../services/entities/recommendation.service";

export class RecommendationController {

  static async showByDoctor(req: Request, res: Response) {
    const doctor = await doctorService.show(+req.params.doctorId);
    if (!doctor) {
      throw new DoctorNotFoundException();
    }
    const recommendations = await recommendationService.showByDoctor(doctor.id);
    return res.json({
      data: recommendations
    });
  }

  static async diseaseAnalysis(req: Request, res: Response) {
    const filters = req.query;
    const doctor = await doctorService.show(+req.params.doctorId);
    if (!doctor) {
      throw new DoctorNotFoundException();
    }
    const diseases = await recommendationService.diseaseAnalysis(doctor.id, filters);
    return res.json({
      data: diseases
    });
  }

  static async medicineAnalysis(req: Request, res: Response) {
    const filters = req.query;
    const doctor = await doctorService.show(+req.params.doctorId);
    if (!doctor) {
      throw new DoctorNotFoundException();
    }
    const diseases = await recommendationService.medicineAnalysis(doctor.id, filters);
    return res.json({
      data: diseases
    });
  }


  static async patientFeedbackAnalysis(req: Request, res: Response) {
    const filters = req.query;
    const doctor = await doctorService.show(+req.params.doctorId);
    if (!doctor) {
      throw new DoctorNotFoundException();
    }
    const feedbacks = await recommendationService.patientFeedbackAnalysis(doctor.id, filters);
    return res.json({
      data: feedbacks
    });
  }
}
