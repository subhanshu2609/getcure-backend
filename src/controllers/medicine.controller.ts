import { Request, Response } from "express";
import Ajv from "ajv";
import * as fs from "fs";
import { UnprocessableEntityException } from "../exceptions/root/unprocessable-entity.exception";
import { MedicineCreateDto } from "../dtos/medicine/medicine-create.dto";
import { medicineService } from "../services/entities/medicine.service";
import { MedicineTransformer } from "../transformers/medicine.transformer";
import { MedicineUpdateDto } from "../dtos/medicine/medicine-update.dto";
import { MedicineNotFoundException } from "../exceptions/medicine/medicine-not-found.exception";
import { resolve } from "dns";
import { MedicineParameter, MedicineUpdateParameters } from "../enums/medicine-parameter.enum";
import { doctorService } from "../services/entities/doctor.service";
import { DoctorNotFoundException } from "../exceptions/user/doctor-not-found.exception";
import { ParameterUpdateDto } from "../dtos/medicine/parameter-update.dto";
import { clinicService } from "../services/entities/clinic.service";
import { Variable } from "../models/variable.model";
import * as _ from "lodash";

export class MedicineController {

  static async viewAllMedicine(req: Request, res: Response) {
    const questions = await medicineService.viewAll();
    return res.json({
      data: questions
    });
  }


  static async viewDoctorMedicine(req: Request, res: Response) {
    const medicines = await medicineService.viewByDoctor(+req.params.doctorId);
    return res.json({
      data: await new MedicineTransformer().transformList(medicines)
    });
  }

  static async viewDoctorMedicineParameter(req: Request, res: Response) {
    const medicines: any[] = await medicineService.viewParameterByDoctor(+req.params.doctorId);
    const transform = _.groupBy(medicines, "type");
    const transformedMedicines = {
      dose: transform.dose ? transform.dose : [],
      route: transform.route ? transform.route : [],
      unit: transform.unit ? transform.unit : [],
      frequency: transform.frequency ? transform.frequency : [],
      direction: transform.direction ? transform.direction : [],
      duration: transform.duration ? transform.duration : [],
      category: transform.category ? transform.category : [],
    };
    return res.json({
      data: transformedMedicines
    });
  }

  static async createMedicine(req: Request, res: Response) {
    const inputData = req.body as MedicineCreateDto;
    const ajv       = new Ajv();
    const schema    = JSON.parse(fs.readFileSync("./schema/medicine/medicine-create.schema.json").toString());
    try {
      await ajv.validate(schema, inputData);
    } catch (e) {
      throw new UnprocessableEntityException(e);
    }
    const medicine = await medicineService.create(inputData);
    return res.json({
      data: await new MedicineTransformer().transform(medicine)
    });
  }
  static async createBulkMedicine(req: Request, res: Response) {
    req.body.medicines = JSON.parse(req.body.medicines);
    const inputData    = req.body as { medicines: MedicineCreateDto[] };
    if (req.body.medicines.length > 0) {
      for (let i = 0; i < inputData.medicines.length; i++) {
        inputData.medicines[i].doctor_id = +inputData.medicines[i].doctor_id;
      }
    }
    const ajv    = new Ajv();
    const schema = JSON.parse(fs.readFileSync("./schema/medicine/medicine-bulk-create.schema.json").toString());
    try {
      await ajv.validate(schema, inputData);
    } catch (e) {
      throw new UnprocessableEntityException(e);
    }
    const medicine = await medicineService.bulkCreate(inputData.medicines);
    return res.json({
      data: await new MedicineTransformer().transformList(medicine)
    });
  }

  static async updateMedicine(req: Request, res: Response) {
    const inputData = req.body as MedicineUpdateDto;
    const ajv       = new Ajv();
    const schema    = JSON.parse(fs.readFileSync("./schema/medicine/medicine-update.schema.json").toString());
    try {
      await ajv.validate(schema, inputData);
    } catch (e) {
      throw new UnprocessableEntityException(e.errors);
    }
    const medicine = await medicineService.show(+req.params.medicineId);
    if (!medicine) {
      throw new MedicineNotFoundException();
    }
    const update = await medicineService.update(medicine, inputData);
    return res.json({
      data: await new MedicineTransformer().transform(update)
    });
  }

  static async deleteMedicine(req: Request, res: Response) {
    const medicine = await medicineService.show(+req.params.medicineId);
    if (!medicine) {
      throw new MedicineNotFoundException();
    }
    await medicineService.delete(medicine);
    return res.json("success");
  }

  static async updateMedicineParam(req: Request, res: Response) {
    const doctor = await doctorService.show(+req.params.doctorId);
    if (!doctor) {
      throw new DoctorNotFoundException();
    }
    const clinicDoctors = await clinicService.showAllClinicDoctorByDoctor(doctor.id);
    const inputData     = req.body as ParameterUpdateDto;
    if (!inputData.value) {
      inputData.value = 0;
    }
    const params = await medicineService.createParameter(doctor, clinicDoctors, inputData);
    return res.json({
      data: params
    });
  }

  static async bulkCreateParams(req: Request, res: Response) {
    const inputData = JSON.parse(req.body.params) as ParameterUpdateDto[];
    const variables = [];
    for (const data of inputData) {
      const variable = await Variable.create({
        doctor_id: req.doctor_id,
        type     : data.type,
        title    : data.name,
        value    : data.value ? data.value : 0
      });
      variables.push(variable);
    }
    return res.json({
      data: variables
    });
  }
}
