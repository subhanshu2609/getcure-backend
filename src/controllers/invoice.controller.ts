import { Request, Response } from "express";
import { clinicService } from "../services/entities/clinic.service";
import { ClinicInvoice, DailyInvoice, DoctorInvoice } from "../models/invoice.model";
import { AppointmentTypes, BookingType, VisitType } from "../enums/token.enum";
import { UnauthorizedException } from "../exceptions/root/unauthorized.exception";
import { ClinicDoctor } from "../models/clinic-doctor.model";
import { PatientVisit } from "../models/patient-visit.model";
import moment from "moment";
import { Sequelize } from "sequelize";
import { ClinicNotLinkedException } from "../exceptions/clinic/clinic-not-linked.exception";
import { Token } from "../models/token.model";
import { Bookie } from "../enums/bookie.enum";
import { AdminInvoiceIndexDto } from "../dtos/invoice/admin-invoice-index.dto";
import { PatientVisitTransformer } from "../transformers/patient-visit.transformer";
import { Clinic } from "../models/clinic.model";
import { CdEmployee } from "../models/cd-employee.model";
import { Op } from "sequelize";
import { patientVisitService } from "../services/entities/patient-visit.service";
import { prescriptionService } from "../services/factories/prescription.service";
import { Helpers } from "../util/helpers.util";
import * as _ from "lodash";

export class InvoiceController {
  static async displayDoctorInvoice(req: Request, res: Response) {
    const clinicDoctorId = +req.params.clinicDoctorId;
    const filters        = req.query as { start_date: string; end_date: string };
    const clinicDoctor   = await clinicService.showClinicDoctorById(clinicDoctorId);
    const visits         = await PatientVisit.findAll({
      where: {
        clinic_doctor_id: clinicDoctorId,
        booking_type    : {
          [Sequelize.Op.ne]: BookingType.CANCELLED
        },
        appointment_time: {
          [Op.between]: [
            filters.start_date ? filters.start_date : "2020-01-01",
            filters.end_date ? moment.utc(filters.end_date).add(1, "day").format("YYYY-MM-DD") : moment.utc().format("YYYY-MM-DD")
          ]
        }
      },
      order: [
        [
          "appointment_time",
          "desc"
        ]
      ]
    });
    const tokens         = await Token.findAll({
      where: {
        clinic_doctor_id: clinicDoctorId,
        booking_type    : {
          [Sequelize.Op.ne]: BookingType.CANCELLED
        },
        date: {
          [Op.between]: [
            filters.start_date ? filters.start_date : "2020-01-01",
            filters.end_date ? moment.utc(filters.end_date).add(1, "day").format("YYYY-MM-DD") : moment.utc().format("YYYY-MM-DD")
          ]
        }
      }
    });
    const tokenDates = tokens.map(t => moment.utc(t.date).format("YYYY-MM-DD"));
    const dates          = visits.map(t => moment.utc(t.appointment_time).format("YYYY-MM-DD")).concat(...tokenDates).filter((value, index, self) => self.indexOf(value) === index);
    const orderedDates = _.orderBy(dates, null , "desc");

    const doctorInvoices: DoctorInvoice[] = [];
    for (const date of orderedDates) {
      const doctorInvoice: DoctorInvoice = {
        date              : date,
        emergencies       : 0,
        new_visits        : 0,
        follow_ups        : 0,
        total_appointments: 0,
        fees_collected    : 0,
        on_call           : 0
      };
      for (const token of visits) {
        if (moment.utc(token.appointment_time).format("YYYY-MM-DD") === date) {
          doctorInvoice.total_appointments += 1;
          doctorInvoice.fees_collected += token.fees;
          if (token.booking_type === BookingType.ON_CALL) {
            doctorInvoice.on_call += 1;
          }
          if (token.appointment_type === AppointmentTypes.GENERAL) {
            if (token.visit_type === VisitType.NEW_VISIT) {
              doctorInvoice.new_visits += 1;
            } else {
              doctorInvoice.follow_ups += 1;
            }
          } else {
            doctorInvoice.emergencies += 1;
          }
        }
      }
      for (const token of tokens) {
        if (token.date.toString() === date) {
          doctorInvoice.total_appointments += 1;
          doctorInvoice.fees_collected += token.fees;
          if (token.booking_type === BookingType.ON_CALL) {
            doctorInvoice.on_call += 1;
          }
          if (token.appointment_type === AppointmentTypes.GENERAL) {
            if (token.visit_type === VisitType.NEW_VISIT) {
              doctorInvoice.new_visits += 1;
            } else {
              doctorInvoice.follow_ups += 1;
            }
          } else {
            doctorInvoice.emergencies += 1;
          }
        }
      }
      doctorInvoices.push(doctorInvoice);
    }
    return res.json({
      data: doctorInvoices
    });
  }

  static async displayClinicInvoice(req: Request, res: Response) {
    const clinicId = +req.params.clinicId;
    let start      = req.query.start_date;
    let end        = req.query.end_date as string;
    if (!start || !end) {
      start = moment.utc().format("YYYY-MM-DD");
      end   = moment.utc().add(1, "days").format("YYYY-MM-DD");
    }
    if (start === end) {
      end = moment.utc(end).add(1, "days").format("YYYY-MM-DD");
    }

    const clinicDoctors: ClinicDoctor[]   = await clinicService.showClinicDoctorByClinicIdAndFrontDesk(clinicId, +req.frontDesk.id);
    const clinicInvoices: ClinicInvoice[] = [];
    for (const clinicDoctor of clinicDoctors) {
      const visits = await PatientVisit.findAll({
        where: {
          clinic_doctor_id: clinicDoctor.id,
          appointment_time       : {
            [Sequelize.Op.between]: [
              start,
              end
            ]
          },
          booking_type    : {
            [Sequelize.Op.ne]: BookingType.CANCELLED
          }
        }
      });
      const tokens = await Token.findAll({
        where: {
          clinic_doctor_id: clinicDoctor.id,
          date       : {
            [Sequelize.Op.between]: [
              start,
              end
            ]
          },
          booking_type    : {
            [Sequelize.Op.ne]: BookingType.CANCELLED
          }
        }
      });

      const clinicInvoice: ClinicInvoice = {
        doctor_name       : clinicDoctor.doctor_name,
        new_visits        : 0,
        follow_ups        : 0,
        emergencies       : 0,
        total_appointments: tokens.length + visits.length,
        fees_collected    : 0,
        on_call           : 0
      };

      for (const token of visits) {
        clinicInvoice.fees_collected += token.fees;
        if (token.booking_type === BookingType.ON_CALL) {
          clinicInvoice.on_call += 1;
        }
        if (token.appointment_type === AppointmentTypes.GENERAL) {
          if (token.visit_type === VisitType.NEW_VISIT) {
            clinicInvoice.new_visits += 1;
          } else {
            clinicInvoice.follow_ups += 1;
          }
        } else {
          clinicInvoice.emergencies += 1;
        }
      }
      for (const token of tokens) {
        clinicInvoice.fees_collected += token.fees;
        if (token.booking_type === BookingType.ON_CALL) {
          clinicInvoice.on_call += 1;
        }
        if (token.appointment_type === AppointmentTypes.GENERAL) {
          if (token.visit_type === VisitType.NEW_VISIT) {
            clinicInvoice.new_visits += 1;
          } else {
            clinicInvoice.follow_ups += 1;
          }
        } else {
          clinicInvoice.emergencies += 1;
        }
      }

      clinicInvoices.push(clinicInvoice);
    }
    return res.json({
      data: clinicInvoices
    });
  }

  static async displayDailyInvoice(req: Request, res: Response) {
    const date         = req.query.date ? req.query.date as string : moment.utc().format("YYYY-MM-DD");
    const clinicDoctor = await clinicService.showClinicDoctorById(+req.params.clinicDoctorId);
    if (!clinicDoctor) {
      throw new ClinicNotLinkedException();
    }
    const tokens                     = await Token.findAll({
      where: {
        date            : date,
        clinic_doctor_id: clinicDoctor.id
      }
    });
    const visits                     = await PatientVisit.findAll({
      where  : {
        clinic_doctor_id: clinicDoctor.id,
        appointment_time: {
          [Sequelize.Op.between]: [
            date,
            moment.utc(date).add(1, "days").format("YYYY-MM-DD")
          ]
        },
        booking_type: {
          [Op.ne]: BookingType.CANCELLED
        }
      },
      logging: true
    });
    const dailyInvoice: DailyInvoice = {
      booked    : tokens.length + visits.length,
      completed : visits.length,
      on_call   : 0,
      on_line   : 0,
      front_desk: 0,
      present   : 0
    };
    for (const token of tokens) {
      if (token.booking_type === BookingType.ON_CALL) {
        dailyInvoice.on_call += 1;
      } else if (token.booking_type === BookingType.ONLINE) {
        dailyInvoice.on_line += 1;
      }
      if (token.booked_by === Bookie.FRONT_DESK) {
        dailyInvoice.front_desk += 1;
      }
      if (token.is_present) {
        dailyInvoice.present += 1;
      }
    }
    for (const visit of visits) {
      if (visit.booking_type === BookingType.ON_CALL) {
        dailyInvoice.on_call += 1;
      } else if (visit.booking_type === BookingType.ONLINE) {
        dailyInvoice.on_line += 1;
      }
      if (visit.booked_by === Bookie.FRONT_DESK) {
        dailyInvoice.front_desk += 1;
      }
    }
    return res.json({
      data: dailyInvoice
    });
  }


  static async adminInvoice(req: Request, res: Response) {
    const filters = req.query as AdminInvoiceIndexDto;
    let startDate = moment.utc().format("YYYY-MM-DD");
    let endDate   = moment.utc().add(1, "days").format("YYYY-MM-DD");
    if (filters.start_date) {
      startDate = moment.utc(filters.start_date).format("YYYY-MM-DD");
    }
    if (filters.end_date) {
      endDate = moment.utc(filters.end_date).format("YYYY-MM-DD");
    }
    let whereClause: any = {
      appointment_time: {
        [Sequelize.Op.between]: [
          startDate,
          endDate
        ]
      }
    };
    if (filters.clinic_id) {
      whereClause = {
        ...whereClause,
        ["$clinic_doctor.clinic_id$"]: +filters.clinic_id
      };
    }
    if (filters.doctor_id) {
      whereClause = {
        ...whereClause,
        ["$clinic_doctor.doctor_id$"]: +filters.doctor_id
      };
    }
    if (filters.state) {
      whereClause = {
        ...whereClause,
        ["$clinic_doctor.clinic.state$"]: filters.state
      };
    }
    if (filters.employee_id) {
      whereClause = {
        ...whereClause,
        ["$clinic_doctor.cdEmployees.employee_id$"]: +filters.employee_id
      };
    }
    const patientVisits = await PatientVisit.findAll({
      where   : whereClause,
      subQuery: false,
      include : [
        {
          model  : ClinicDoctor,
          include: [
            Clinic,
            CdEmployee
          ]
        }
      ]
    });

    const countData = {
      total_appointments: patientVisits.length,
      paid_visit        : 0,
      free_visit        : 0,
      on_call_paid      : 0,
      on_call_free      : 0,
      online_charge     : 0,
      total_revenue     : 0
    };

    const adminInvoice: {
      clinic_doctor_id: number;
      state: string,
      clinic_name: string,
      doctor_name: string,
      follow_up: number,
      new_visit: number,
      online: number,
      front_desk: number,
      on_call: number,
      present: number,
      revenue: number,
    }[] = [];
    for (const patientVisit of patientVisits) {
      const cd              = patientVisit.clinic_doctor;
      const previousInvoice = adminInvoice.findIndex(i => i.clinic_doctor_id === patientVisit.clinic_doctor_id);
      if (previousInvoice === -1) {
        const revenue = cd.paid_visit_charge + cd.free_visit_charge + cd.on_call_paid_visit_charge + cd.on_call_free_visit_charge + cd.online_charge;
        const invoice = {
          clinic_doctor_id: patientVisit.clinic_doctor_id,
          state           : patientVisit.clinic_doctor.clinic.state,
          clinic_name     : patientVisit.clinic_doctor.clinic.name,
          doctor_name     : patientVisit.clinic_doctor.doctor_name,
          follow_up       : 0,
          new_visit       : 0,
          online          : 0,
          front_desk      : 0,
          on_call         : 0,
          present         : 0,
          revenue         : revenue
        };
        if (patientVisit.visit_type === VisitType.FOLLOW_UP) {
          invoice.follow_up += 1;
        } else {
          invoice.new_visit += 1;
        }
        if (patientVisit.booking_type === BookingType.ONLINE) {
          invoice.online += 1;
        } else if (patientVisit.booking_type === BookingType.ON_CALL) {
          invoice.on_call += 1;
        }
        if (patientVisit.booked_by === Bookie.FRONT_DESK) {
          invoice.front_desk += 1;
        }
        adminInvoice.push(invoice);
        countData.free_visit += cd.free_visit_charge;
        countData.paid_visit += cd.paid_visit_charge;
        countData.on_call_free += cd.on_call_free_visit_charge;
        countData.on_call_paid += cd.on_call_paid_visit_charge;
        countData.online_charge += cd.online_charge;
        countData.total_revenue += revenue;
      } else {
        if (patientVisit.visit_type === VisitType.FOLLOW_UP) {
          adminInvoice[previousInvoice].follow_up += 1;
        } else {
          adminInvoice[previousInvoice].new_visit += 1;
        }
        if (patientVisit.booking_type === BookingType.ONLINE) {
          adminInvoice[previousInvoice].online += 1;
        } else if (patientVisit.booking_type === BookingType.ON_CALL) {
          adminInvoice[previousInvoice].on_call += 1;
        }
        if (patientVisit.booked_by === Bookie.FRONT_DESK) {
          adminInvoice[previousInvoice].front_desk += 1;
        }
        const revenue = cd.paid_visit_charge + cd.free_visit_charge + cd.on_call_paid_visit_charge + cd.on_call_free_visit_charge + cd.online_charge;
        adminInvoice[previousInvoice].revenue += revenue;
        countData.free_visit += cd.free_visit_charge;
        countData.paid_visit += cd.paid_visit_charge;
        countData.on_call_free += cd.on_call_free_visit_charge;
        countData.on_call_paid += cd.on_call_paid_visit_charge;
        countData.online_charge += cd.online_charge;
        countData.total_revenue += revenue;
      }
    }
    return res.json({
      count_data: countData,
      data      : adminInvoice
    });
  }

  static async getPrescription(req: Request, res: Response) {
    const patientVisit = await patientVisitService.show(+req.params.patientVisitId);
    const html         = await prescriptionService.generatePdf(patientVisit);
    const pdf          = await Helpers.downloadPDFFromHtml(res, html, `${patientVisit.id}.pdf`);
  }
}
