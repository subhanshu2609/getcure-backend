export enum PaymentModes {
  WALLET    = "wallet",
  COD       = "cod",
  RAZOR_PAY = "razor_pay"
}
