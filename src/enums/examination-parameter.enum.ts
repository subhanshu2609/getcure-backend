export enum ExaminationParameterEnum {
  RADIO   = "radio",
  NUMERIC = "numeric",
}
