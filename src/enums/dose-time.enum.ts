export enum DoseTimeEnum {
  BEFORE = "before",
  AFTER  = "after",
}

export enum ExaminationStats {
  ADVISED   = "advised",
  COMPLETED = "completed"
}
