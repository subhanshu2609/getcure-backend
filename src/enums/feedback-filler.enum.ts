export enum FeedbackFiller {
  EMPLOYEE = "employee",
  PATIENT  = "patient",
  DOCTOR   = "doctor",
}
