export enum VisibilityEnum {
  ONE_MONTH   = "one month",
  THREE_MONTH = "three month",
  SIX_MONTH   = "six month",
  ONE_YEAR    = "one year",
  FIVE_YEAR   = "five year",
  ALWAYS      = "always"
}

