export enum AppointmentTypes {
  GENERAL   = "general",
  EMERGENCY = "emergency"
}

export enum VisitType {
  NEW_VISIT = "new visit",
  FOLLOW_UP = "follow up",
}

export enum BookingType {
  WALK_IN   = "walk in",
  ON_CALL   = "on call",
  ONLINE    = "online",
  CANCELLED = "cancelled"
}
