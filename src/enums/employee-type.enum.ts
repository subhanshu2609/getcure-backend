export enum EmployeeType {
  ADMIN    = "admin",
  EMPLOYEE = "employee",
}
