export enum MedicineParameter {
  DOSE      = "dose",
  FREQUENCY = "frequency",
  DURATION  = "duration",
}

export enum   MedicineUpdateParameters {
  DOSE      = "dose",
  ROUTE     = "route",
  UNIT      = "unit",
  FREQUENCY = "frequency",
  DIRECTION = "direction",
  DURATION  = "duration",
  CATEGORY  = "category"
}
