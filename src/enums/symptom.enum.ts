export enum SymptomType {
  BRIEF_HISTORY = "Brief History",
  VISIT_REASON  = "Visit Reason"
}

export enum HabitType {
  ALLERGY   = "allergy",
  LIFESTYLE = "lifestyle"
}
