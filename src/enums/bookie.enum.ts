export enum Bookie {
  FRONT_DESK = "front_desk",
  EMPLOYEE   = "employee",
  PATIENT    = "patient",
  DOCTOR     = "doctor"
}
