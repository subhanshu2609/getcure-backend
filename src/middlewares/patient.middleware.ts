import { NextFunction, Request, Response } from "express";
import { Helpers } from "../util/helpers.util";
import * as jwt from "jsonwebtoken";
import { InvalidJwtTokenException } from "../exceptions/invalid-jwt-token.exception";
import { InternalException } from "../exceptions/root/internal.exception";
import { ApiErrorCode } from "../exceptions/root/http.exception";
import { doctorService } from "../services/entities/doctor.service";
import { patientService } from "../services/entities/patient.service";

export const patientMiddleware = async (req: Request, res: Response, next: NextFunction) => {
  const jwtToken = req.headers.authorization;
  if (!jwtToken) {
    return Helpers.handleError(res, new InvalidJwtTokenException());
  }
  try {
    const payload  = jwt.decode(jwtToken) as any;
    const employee = await patientService.showByMobileNumber(payload.patient_mobile as string);
    console.log(employee);
    if (!employee || employee.length === 0) {
      return Helpers.handleError(res, new InvalidJwtTokenException());
    }

    req.patient = employee;

  } catch (e) {
    if (e instanceof jwt.JsonWebTokenError) {
      return Helpers.handleError(res, new InvalidJwtTokenException());
    } else {
      return Helpers.handleError(res, new InternalException(e.message, ApiErrorCode.UNKNOWN, e.stack));
    }
  }
  next();
};
