import { NextFunction, Request, Response } from "express";
import { Helpers } from "../util/helpers.util";
import * as jwt from "jsonwebtoken";
import { InvalidJwtTokenException } from "../exceptions/invalid-jwt-token.exception";
import { InternalException } from "../exceptions/root/internal.exception";
import { ApiErrorCode } from "../exceptions/root/http.exception";
import { doctorService } from "../services/entities/doctor.service";

export const doctorMiddleware = async (req: Request, res: Response, next: NextFunction) => {
  const jwtToken = req.headers.authorization;
  if (!jwtToken) {
    return Helpers.handleError(res, new InvalidJwtTokenException());
  }
  try {
    const payload = jwt.decode(jwtToken) as any;
    const doctor  = await doctorService.show(payload.doctor_id as number);

    if (!doctor) {
      return Helpers.handleError(res, new InvalidJwtTokenException());
    }
    req.doctor_id   = doctor.id;
    req.doctor_name = doctor.name;
  } catch (e) {
    if (e instanceof jwt.JsonWebTokenError) {
      return Helpers.handleError(res, new InvalidJwtTokenException());
    } else {
      return Helpers.handleError(res, new InternalException(e.message, ApiErrorCode.UNKNOWN, e.stack));
    }
  }
  next();
};
