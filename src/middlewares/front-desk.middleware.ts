import { NextFunction, Request, Response } from "express";
import { Helpers } from "../util/helpers.util";
import * as jwt from "jsonwebtoken";
import { InvalidJwtTokenException } from "../exceptions/invalid-jwt-token.exception";
import { InternalException } from "../exceptions/root/internal.exception";
import { ApiErrorCode } from "../exceptions/root/http.exception";
import { frontDeskService } from "../services/entities/front-desk.service";

export const frontDeskMiddleware = async (req: Request, res: Response, next: NextFunction) => {
  const jwtToken = req.headers.authorization;
  if (!jwtToken) {
    return Helpers.handleError(res, new InvalidJwtTokenException());
  }
  try {
    const payload   = jwt.decode(jwtToken) as any;
    const frontDesk = await frontDeskService.show(payload.front_desk_id as number);

    if (!frontDesk) {
      return Helpers.handleError(res, new InvalidJwtTokenException());
    }
    req.frontDesk = frontDesk;
  } catch (e) {
    if (e instanceof jwt.JsonWebTokenError) {
      return Helpers.handleError(res, new InvalidJwtTokenException());
    } else {
      return Helpers.handleError(res, new InternalException(e.message, ApiErrorCode.UNKNOWN, e.stack));
    }
  }
  next();
};
