import { ModelNotFoundException } from "../root/model-not-found.exception";
import { ApiErrorCode } from "../root/http.exception";

export class StateNotFoundException extends ModelNotFoundException {

  constructor() {
    super("State Not Found!", ApiErrorCode.CITY_NOT_FOUND);
  }
}
