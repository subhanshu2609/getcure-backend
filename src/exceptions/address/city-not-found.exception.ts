import { ModelNotFoundException } from "../root/model-not-found.exception";
import { ApiErrorCode } from "../root/http.exception";

export class CityNotFoundException extends ModelNotFoundException {

  constructor() {
    super("City Not Found!", ApiErrorCode.CITY_NOT_FOUND);
  }
}
