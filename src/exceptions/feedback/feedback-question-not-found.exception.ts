import { ModelNotFoundException } from "../root/model-not-found.exception";
import { ApiErrorCode } from "../root/http.exception";

export class FeedbackQuestionNotFoundException extends ModelNotFoundException {

  constructor() {
    super("Feedback Question Not Found!", ApiErrorCode.FEEDBACK_QUESTION_NOT_FOUND);
  }
}
