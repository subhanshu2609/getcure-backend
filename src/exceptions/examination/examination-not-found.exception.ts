import { ModelNotFoundException } from "../root/model-not-found.exception";
import { ApiErrorCode } from "../root/http.exception";

export class ExaminationNotFoundException extends ModelNotFoundException {

  constructor() {
    super("Examination Not Found!", ApiErrorCode.EXAMINATION_NOT_FOUND);
  }
}
