import { ModelNotFoundException } from "../root/model-not-found.exception";
import { ApiErrorCode } from "../root/http.exception";

export class SpecialityNotFoundException extends ModelNotFoundException {

  constructor() {
    super("Speciality Not Found!", ApiErrorCode.SPECIALITY_NOT_FOUND);
  }
}
