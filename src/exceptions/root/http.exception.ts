export class HttpException extends Error {

  /**
   * Message in english that explains the error
   *
   * It should be directly addressed to doctor
   * as in most cases this will be displayed to doctor.
   */
  message: string;

  /**
   * App Specific error code that uniquely identifies an error
   *
   * e.g. => When Requesting a specific entity with a specific id,
   * If it is not found, there should be a unique id associated with it
   * and using which frontend should redirect doctor back to the previous page
   */
  errorCode: number;

  /**
   * Http status code
   *
   * https://developer.mozilla.org/en-US/docs/Web/HTTP/Status
   */
  statusCode: number;

  /**
   * Any additional data associated with a specific error.
   *
   * e.g. => Login API might want to send
   * number of attempts left on incorrect password
   */
  meta?: any;

  /**
   * Validation errors thrown by AJV
   * send as it is being thrown by the library...
   */
  errors?: any;

  constructor(message: string, errorCode: ApiErrorCode, statusCode: number, meta?: any, errors?: any) {
    super(message);

    this.message    = message;
    this.errorCode  = errorCode;
    this.statusCode = statusCode;

    this.meta   = meta;
    this.errors = errors;
  }
}


export enum ApiErrorCode {

  // User Related
  INCORRECT_OTP              = 100,
  DOCTOR_NOT_FOUND           = 101,
  DOCTOR_NOT_VERIFIED,
  DOCTOR_NOT_AVAILABLE,
  DOCTOR_ALREADY_EXISTS,
  DOCTOR_ALREADY_LINKED,
  FRONT_DESK_NOT_FOUND,
  FRONT_DESK_NOT_VERIFIED,
  CLINIC_NOT_FOUND,
  CLINIC_NOT_LINKED,
  PATIENT_NOT_FOUND,
  PATIENT_VISIT_NOT_FOUND,
  PATIENT_ALREADY_EXISTS,
  TOKEN_NOT_FOUND,
  TOKEN_ALREADY_BOOKED,
  EXAMINATION_NOT_FOUND,
  SYMPTOM_NOT_FOUND,
  EMPLOYEE_NOT_FOUND,
  EMPLOYEE_NOT_LINKED,
  CITY_NOT_FOUND,
  SPECIALITY_NOT_FOUND,
  IMAGE_NOT_FOUND,
  FEEDBACK_QUESTION_NOT_FOUND,
  MEDICINE_NOT_FOUND,
  HABIT_NOT_FOUND,

  // JWT

  JWT_INVALID                = 9101,
  JWT_INCORRECT_PAYLOAD_TYPE = 9102,
  VALIDATION_ERROR           = 9998,

  UNKNOWN                    = 9999 // Reserved...
}
