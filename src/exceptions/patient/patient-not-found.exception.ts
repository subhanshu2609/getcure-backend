import { ModelNotFoundException } from "../root/model-not-found.exception";
import { ApiErrorCode } from "../root/http.exception";

export class PatientNotFoundException extends ModelNotFoundException {

  constructor() {
    super("Patient Not Found!", ApiErrorCode.PATIENT_NOT_FOUND);
  }
}
