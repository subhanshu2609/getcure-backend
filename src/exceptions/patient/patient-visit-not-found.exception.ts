import { ModelNotFoundException } from "../root/model-not-found.exception";
import { ApiErrorCode } from "../root/http.exception";

export class PatientVisitNotFoundException extends ModelNotFoundException {

  constructor() {
    super("Patient Visit Not Found!", ApiErrorCode.PATIENT_VISIT_NOT_FOUND);
  }
}
