import { ModelNotFoundException } from "../root/model-not-found.exception";
import { ApiErrorCode } from "../root/http.exception";

export class PatientAlreadyExistsException extends ModelNotFoundException {

  constructor() {
    super("Patient Already Exists!", ApiErrorCode.PATIENT_ALREADY_EXISTS);
  }
}
