import { ModelNotFoundException } from "../root/model-not-found.exception";
import { ApiErrorCode } from "../root/http.exception";

export class EmployeeNotLinkedException extends ModelNotFoundException {

  constructor() {
    super("Employee Not Linked!", ApiErrorCode.EMPLOYEE_NOT_LINKED);
  }
}
