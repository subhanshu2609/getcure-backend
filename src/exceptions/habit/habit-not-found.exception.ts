import { ModelNotFoundException } from "../root/model-not-found.exception";
import { ApiErrorCode } from "../root/http.exception";

export class HabitNotFoundException extends ModelNotFoundException {

  constructor() {
    super("Habit Not Found!", ApiErrorCode.HABIT_NOT_FOUND);
  }
}
