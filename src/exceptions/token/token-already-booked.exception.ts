import { ModelNotFoundException } from "../root/model-not-found.exception";
import { ApiErrorCode } from "../root/http.exception";

export class TokenAlreadyBookedException extends ModelNotFoundException {

  constructor() {
    super("Token Already Booked!", ApiErrorCode.TOKEN_ALREADY_BOOKED);
  }
}
