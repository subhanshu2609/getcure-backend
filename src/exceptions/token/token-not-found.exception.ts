import { ModelNotFoundException } from "../root/model-not-found.exception";
import { ApiErrorCode } from "../root/http.exception";

export class TokenNotFoundException extends ModelNotFoundException {

  constructor() {
    super("Token Not Found!", ApiErrorCode.TOKEN_NOT_FOUND);
  }
}
