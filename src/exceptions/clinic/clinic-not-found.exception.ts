import { ModelNotFoundException } from "../root/model-not-found.exception";
import { ApiErrorCode } from "../root/http.exception";

export class ClinicNotFoundException extends ModelNotFoundException {

  constructor() {
    super("Clinic Not Found!", ApiErrorCode.CLINIC_NOT_FOUND);
  }
}
