import { ModelNotFoundException } from "../root/model-not-found.exception";
import { ApiErrorCode } from "../root/http.exception";

export class ClinicNotLinkedException extends ModelNotFoundException {

  constructor() {
    super("Clinic Not Linked with Doctor!", ApiErrorCode.CLINIC_NOT_LINKED);
  }
}
