import { ModelNotFoundException } from "../root/model-not-found.exception";
import { ApiErrorCode } from "../root/http.exception";

export class PhoneNumberAlreadyExistsException extends ModelNotFoundException {

  constructor() {
    super("Phone Number Already Exists!", ApiErrorCode.DOCTOR_ALREADY_EXISTS);
  }
}
