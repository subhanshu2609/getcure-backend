import { ModelNotFoundException } from "../root/model-not-found.exception";
import { ApiErrorCode } from "../root/http.exception";

export class FrontDeskNotVerifiedException extends ModelNotFoundException {

  constructor() {
    super("Front Desk Not Verified!", ApiErrorCode.FRONT_DESK_NOT_VERIFIED);
  }
}
