import { ModelNotFoundException } from "../root/model-not-found.exception";
import { ApiErrorCode } from "../root/http.exception";

export class DoctorNotAvailableException extends ModelNotFoundException {

  constructor() {
    super("Doctor Not Available!", ApiErrorCode.DOCTOR_NOT_FOUND);
  }
}
