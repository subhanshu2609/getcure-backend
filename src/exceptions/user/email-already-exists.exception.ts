import { ModelNotFoundException } from "../root/model-not-found.exception";
import { ApiErrorCode } from "../root/http.exception";

export class EmailAlreadyExistsException extends ModelNotFoundException {

  constructor() {
    super("Email Already Exists!", ApiErrorCode.DOCTOR_ALREADY_EXISTS);
  }
}
