import { ModelNotFoundException } from "../root/model-not-found.exception";
import { ApiErrorCode } from "../root/http.exception";

export class DoctorAlreadyLinkedToClinicException extends ModelNotFoundException {

  constructor() {
    super("Doctor Already Linked to Clinic!", ApiErrorCode.DOCTOR_ALREADY_LINKED);
  }
}
