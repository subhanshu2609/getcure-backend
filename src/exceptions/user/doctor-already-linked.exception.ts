import { ModelNotFoundException } from "../root/model-not-found.exception";
import { ApiErrorCode } from "../root/http.exception";

export class DoctorAlreadyLinkedException extends ModelNotFoundException {

  constructor() {
    super("Doctor Already Linked to Front Desk!", ApiErrorCode.DOCTOR_ALREADY_LINKED);
  }
}
