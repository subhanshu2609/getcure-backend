import { ModelNotFoundException } from "../root/model-not-found.exception";
import { ApiErrorCode } from "../root/http.exception";

export class DoctorNotFoundException extends ModelNotFoundException {

  constructor() {
    super("Doctor Not Found!", ApiErrorCode.DOCTOR_NOT_FOUND);
  }
}
