import { ModelNotFoundException } from "../root/model-not-found.exception";
import { ApiErrorCode } from "../root/http.exception";

export class DoctorNotLinkedException extends ModelNotFoundException {

  constructor() {
    super("Doctor Not Linked to Clinic!", ApiErrorCode.DOCTOR_ALREADY_LINKED);
  }
}
