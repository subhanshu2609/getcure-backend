import { ModelNotFoundException } from "../root/model-not-found.exception";
import { ApiErrorCode } from "../root/http.exception";

export class DoctorNotVerifiedException extends ModelNotFoundException {

  constructor() {
    super("Doctor Not Verified!", ApiErrorCode.DOCTOR_NOT_VERIFIED);
  }
}
