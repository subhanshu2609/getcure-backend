import { ModelNotFoundException } from "../root/model-not-found.exception";
import { ApiErrorCode } from "../root/http.exception";

export class DoctorAlreadyExistsException extends ModelNotFoundException {

  constructor() {
    super("Doctor Already Exists!", ApiErrorCode.DOCTOR_ALREADY_EXISTS);
  }
}
