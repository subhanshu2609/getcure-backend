import { ModelNotFoundException } from "../root/model-not-found.exception";
import { ApiErrorCode } from "../root/http.exception";

export class FrontDeskNotFoundException extends ModelNotFoundException {

  constructor() {
    super("Front Desk Not Found!", ApiErrorCode.FRONT_DESK_NOT_FOUND);
  }
}
