import { ModelNotFoundException } from "../root/model-not-found.exception";
import { ApiErrorCode } from "../root/http.exception";

export class ImageNotFoundException extends ModelNotFoundException {

  constructor() {
    super("Image Not Found!", ApiErrorCode.IMAGE_NOT_FOUND);
  }
}
