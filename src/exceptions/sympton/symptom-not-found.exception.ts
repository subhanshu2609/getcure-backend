import { ModelNotFoundException } from "../root/model-not-found.exception";
import { ApiErrorCode } from "../root/http.exception";

export class SymptomNotFoundException extends ModelNotFoundException {

  constructor() {
    super("Symptom Not Found!", ApiErrorCode.SYMPTOM_NOT_FOUND);
  }
}
