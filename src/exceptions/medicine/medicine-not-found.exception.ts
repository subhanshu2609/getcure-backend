import { ModelNotFoundException } from "../root/model-not-found.exception";
import { ApiErrorCode } from "../root/http.exception";

export class MedicineNotFoundException extends ModelNotFoundException {

  constructor() {
    super("Medicine Not Found!", ApiErrorCode.MEDICINE_NOT_FOUND);
  }
}
