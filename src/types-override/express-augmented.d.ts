import { Doctor } from "../models/doctor.model";
import { FrontDesk } from "../models/front-desk.model";
import { Patient } from "../models/patient.model";
import { Employee } from "../models/employee.model";

declare module "express" {
  export interface Request {
    doctor_id: number;
    doctor_name: string;
    frontDesk: FrontDesk;
    patient: Patient[];
    employee: Employee;
  }
}
