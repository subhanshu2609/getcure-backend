import {
  Medication,
  PatientBriefHistory,
  PatientDiagnosis,
  PatientExamination,
  PatientVisitReason
} from "../../models/about-patient.model";
import { AppointmentTypes, BookingType, VisitType } from "../../enums/token.enum";
import { Bookie } from "../../enums/bookie.enum";

export interface PatientVisitCreateDto {
  clinic_doctor_id: number;
  patient_id: string;
  patient_name: string;
  temperature: number;
  blood_pressure: string;
  pulse: number;
  weight: number;
  brief_history: PatientBriefHistory[];
  visit_reason: PatientVisitReason[];
  examination: PatientExamination[];
  diagnosis: PatientDiagnosis[];
  medication: Medication[];
  allergies: string[];
  lifestyle: string[];
}

export interface PatientVisitBulkCreateDto {
  patient_visits: {
    clinic_doctor_id: number;
    patient_id: string;
    patient_name: string;
    temperature: number;
    blood_pressure: string;
    pulse: number;
    weight: number;
    brief_history: PatientBriefHistory[];
    visit_reason: PatientVisitReason[];
    examination: PatientExamination[];
    diagnosis: PatientDiagnosis[];
    medication: Medication[];
    allergies: string[];
    lifestyle: string[];
    booked_at: Date;
    appointment_time: Date;
    present_time: Date;
    appointment_type: AppointmentTypes;
    visit_type: VisitType;
    booking_type: BookingType;
    booked_by: Bookie;
    booked_via: string;
    fees: number;
  }[];
}
