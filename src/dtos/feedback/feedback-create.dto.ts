import { PrescribedMedicine } from "../../models/about-patient.model";

export interface FeedbackDto {
  question: string;
  option: string;
  medicine?: PrescribedMedicine[];
}

export interface FeedbackCreateDto {
  patient_visit_id: number;
  feedback: FeedbackDto[];
}

export interface FeedbackBulkCreateDto {
  feedbacks: {
    patient_id: string;
    clinic_doctor_id: number;
    appointment_time: string;
    feedback: FeedbackDto[];
  }[];
}
