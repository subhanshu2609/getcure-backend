export interface FeedbackQuestionUpdateDto {
  title?: string;
  options?: string[];
  is_active?: boolean;
}
