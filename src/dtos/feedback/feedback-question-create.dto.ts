export interface FeedbackQuestionCreateDto {
  title: string;
  options: string[];
}
