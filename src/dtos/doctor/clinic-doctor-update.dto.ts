import { Timing } from "../../models/timing.model";

export interface ClinicDoctorUpdateDto {
  clinic_id: number;
  consultation_fee?: number;
  emergency_fee?: number;
  paid_visit_charge?: number;
  free_visit_charge?: number;
  on_call_paid_visit_charge?: number;
  on_call_free_visit_charge?: number;
  follow_up_appointments?: number;
  follow_up_days?: number;
  slot_time?: number;
  doctor_timings?: Timing[];
}

export interface ClinicDoctorTimingUpdateDto {
  doctor_timings: Timing[];
}
