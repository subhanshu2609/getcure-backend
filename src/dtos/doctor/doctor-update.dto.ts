import { Verification } from "../../models/about-patient.model";

export interface DoctorUpdateDto {
  image_url?: string;
  name?: string;
  age?: number;
  gender?: string;
  degree?: string;
  mobile_no?: string;
  email?: string;
  specialities?: string[];
  identity_verification_url?: Verification[];
  language?: string;
  experience?: string;
  designation?: string;
  password?: string;
  holidays?: Date[];
  is_active?: boolean;
}
