import { MedicineParameter } from "../../enums/medicine-parameter.enum";

export interface MedicineParameterCreateDto {
  doctor_id: number;
  title: string;
  value: number;
  type: MedicineParameter;
}
