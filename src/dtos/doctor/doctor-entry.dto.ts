import { Timing } from "../../models/timing.model";

export interface DoctorEntryDto {
  name: string;
  age: number;
  gender: string;
  degree: string;
  mobile_no: string;
  email: string;
  specialities: string[];
  language: string;
  experience: string;
  designation: string;
  password: string;
}
