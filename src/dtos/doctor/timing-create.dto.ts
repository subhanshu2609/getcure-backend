import { Timing } from "../../models/timing.model";

export interface TimingCreateDto {
  doctor_timings: Timing[];
}
