export interface ClinicDoctorInstantUpdateDto {
  clinic_id: number;
  name: string;
  email: string;
  mobile_no: string;
}
