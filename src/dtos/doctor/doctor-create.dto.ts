import { Timing } from "../../models/timing.model";

export interface DoctorCreateDto {
  image_url?: string;
  name?: string;
  age?: number;
  gender?: string;
  degree?: string;
  mobile_no?: string;
  email?: string;
  specialities?: string[];
  language?: string;
  experience?: string;
  designation?: string;
  password?: string;
  holidays?: Date[];
  get_cure_code?: string;
  clinic_id: number;
  consultation_fee: number;
  emergency_fee: number;
  paid_visit_charge: number;
  free_visit_charge?: number;
  on_call_paid_visit_charge: number;
  on_call_free_visit_charge?: number;
  online_charge: number;
  follow_up_appointments?: number;
  follow_up_days?: number;
  slot_time: number;
  doctor_timings: Timing[];
}
