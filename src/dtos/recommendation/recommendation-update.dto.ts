export interface RecommendationUpdateDto {
  total_count?: number;
  cured?: number;
  partially_cured?: number;
  not_cured?: number;
  symptoms_increased?: number;
}

