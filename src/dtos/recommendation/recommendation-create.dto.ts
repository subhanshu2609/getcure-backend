export interface RecommendationCreateDto {
  clinic_doctor_id: number;
  disease: string;
  medicines: string[];
  total_count: number;
  cured: number;
  partially_cured: number;
  not_cured: number;
  symptoms_increased: number;
}

export interface RecommendationBulkCreateDto {
  recommendations: RecommendationCreateDto[];
}
