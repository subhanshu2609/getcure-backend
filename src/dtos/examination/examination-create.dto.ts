import { Parameter } from "../../models/parameter.model";

export interface ExaminationCreateDto {
  doctor_id: number;
  title: string;
  parameters: Parameter[];
  price?: number;
}
