import { Parameter } from "../../models/parameter.model";

export interface ExaminationUpdateDto {
  title?: string;
  parameters?: Parameter[];
  price?: number;
}
