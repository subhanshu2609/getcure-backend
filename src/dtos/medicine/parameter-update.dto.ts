import { MedicineParameter, MedicineUpdateParameters } from "../../enums/medicine-parameter.enum";

export interface ParameterUpdateDto {
  type: MedicineUpdateParameters;
  name: string;
  value?: number;
}
