export interface MedicineCreateDto {
  doctor_id: number;
  title: string;
  salt?: string;
  interaction_drugs?: string[];
  category: string;
  default_dose: string;
  default_unit: string;
  default_route: string;
  default_frequency: string;
  default_direction: string;
  default_duration: string;
}
