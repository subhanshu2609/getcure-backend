import { AppointmentTypes, BookingType, VisitType } from "../../enums/token.enum";
import { PatientCreateDto } from "../patient/patient-create.dto";


export interface TokenCreateDto {
  patient_id?: string;
  patient_name?: string;
  patient?: PatientCreateDto;
  appointment_type: AppointmentTypes;
  visit_type: VisitType;
  booking_type: BookingType;
  date: string;
  time: string;
  fees: number;
  token_no: number | string;
  clinic_doctor_id: number;
  is_present?: boolean;
  booked_at?: Date;
  transaction_id?: string;
}
