import { HabitType, SymptomType } from "../../enums/symptom.enum";
import { VisibilityEnum } from "../../enums/visibility.enum";

export interface HabitUpdateDto {
  title?: string;
  is_active?: boolean;
  visibility_period?: VisibilityEnum;
}
