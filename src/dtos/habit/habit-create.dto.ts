import { HabitType, SymptomType } from "../../enums/symptom.enum";

export interface HabitCreateDto {
  doctor_id: number;
  title: string;
  type?: HabitType;
}
