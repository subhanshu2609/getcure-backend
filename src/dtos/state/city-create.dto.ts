export interface CityCreateDto {
  title: string;
  state_id: number;
}
