import { EmployeeType } from "../../enums/employee-type.enum";

export interface EmployeeUpdateDto {
  first_name?: string;
  last_name?: string;
  password?: string;
  type?: EmployeeType;
  aadhar_no?: string;
  mobile_no?: string;
  gender?: string;
  dob?: Date;
  address?: string;
  pin_code?: string;
  is_active?: string | boolean;
  clinic_ids?: number[];
  doctor_ids?: number[];
}
