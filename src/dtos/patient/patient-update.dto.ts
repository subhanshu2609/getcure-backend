export interface PatientUpdateDto {
  name?: string;
  address?: string;
  age?: number | string;
  gender?: string;
}
