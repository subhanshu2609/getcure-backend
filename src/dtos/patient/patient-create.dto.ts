export interface PatientCreateDto {
  name: string;
  mobile_no: string;
  address: string;
  age: number | string;
  gender: string;
  patient_id?: string;
  otp?: string;
}
export interface PatientBulkCreateDto {
  patients: PatientCreateDto[];
}
