export interface FrontDeskCreateDto {
  name: string;
  email: string;
  mobile_no: string;
  password: string;
  gender: string;
  clinic_id: number;
  doctor_ids?: number[];
}
