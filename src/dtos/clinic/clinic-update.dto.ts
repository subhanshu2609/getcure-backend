import { Timing } from "../../models/timing.model";

export interface ClinicUpdateDto {
  name?: string;
  type?: string;
  established_year?: string;
  no_of_beds?: number;
  no_of_doctors?: number;
  state_id?: number;
  state?: string;
  timings?: string;
  about?: string;
  specialities?: string[];
  address?: string;
  pin_code?: string;
  phone_no?: string;
  latitude?: string;
  longitude?: string;
  image_urls?: string[];
  is_active: boolean;
}
