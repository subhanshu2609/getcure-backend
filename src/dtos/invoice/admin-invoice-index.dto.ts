export interface AdminInvoiceIndexDto {
  state?: string;
  clinic_id?: number;
  doctor_id?: number;
  employee_id?: number;
  start_date?: string;
  end_date?: string;
}
