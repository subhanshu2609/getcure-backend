import { SymptomType } from "../../enums/symptom.enum";
import { VisibilityEnum } from "../../enums/visibility.enum";

export interface SymptomCreateDto {
  doctor_id: number;
  title: string;
  visibility_period: VisibilityEnum;
}

export interface SymptomBulkCreateDto {
  symptoms: SymptomCreateDto[];
}
