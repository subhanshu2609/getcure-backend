import { TransformerAbstract } from "./transformer.abstract";
import { Dictionary } from "async";
import { Patient } from "../models/patient.model";
import { Medicine } from "../models/medicine.model";

export class MedicineTransformer extends TransformerAbstract<Medicine> {

  protected _map(medicine: Medicine): Dictionary<any> {
    return {
      id               : medicine.id,
      doctor_id        : medicine.doctor_id,
      title            : medicine.title,
      salt             : medicine.salt,
      interaction_drugs: medicine.interaction_drugs,
      category         : medicine.category,
      default_dose     : medicine.default_dose,
      default_unit     : medicine.default_unit,
      default_route    : medicine.default_route,
      default_frequency: medicine.default_frequency,
      default_direction: medicine.default_direction,
      default_duration : medicine.default_duration,
      created_at       : medicine.createdAt,
      updated_at       : medicine.updatedAt,
    };
  }

}
