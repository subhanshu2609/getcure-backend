import { TransformerAbstract } from "./transformer.abstract";
import { Dictionary } from "async";
import { Speciality } from "../models/speciality.model";
import { Helpers } from "../util/helpers.util";

export class SpecialityTransformer extends TransformerAbstract<Speciality> {

  protected _map(speciality: Speciality): Dictionary<any> {
    return {
      id        : Helpers.replaceUndefinedWithNull(speciality.id),
      title     : Helpers.replaceUndefinedWithNull(speciality.title),
      is_active : Helpers.replaceUndefinedWithNull(speciality.is_active),
      created_at: Helpers.replaceUndefinedWithNull(speciality.createdAt),
      updated_at: Helpers.replaceUndefinedWithNull(speciality.updatedAt),
      deleted_at: Helpers.replaceUndefinedWithNull(speciality.deletedAt)
    };
  }

}
