import { TransformerAbstract } from "./transformer.abstract";
import { Dictionary } from "async";
import { Doctor } from "../models/doctor.model";
import { isUndefined } from "util";
import { Helpers } from "../util/helpers.util";
import { ClinicDoctor } from "../models/clinic-doctor.model";
import { Clinic } from "../models/clinic.model";
import { ClinicTransformer } from "./clinic.transformer";
import { ClinicDoctorTransformer } from "./clinic-doctor.transformer";
import { FrontDesk } from "../models/front-desk.model";
//
// export class UserCompactTransformer extends TransformerAbstract<User> {
//   protected _map(doctor: User): Dictionary<any> {
//     return {
//       id        : Helpers.replaceUndefinedWithNull(doctor.id),
//       name      : Helpers.replaceUndefinedWithNull(doctor.name),
//       email     : Helpers.replaceUndefinedWithNull(doctor.email),
//       mobile_no : Helpers.replaceUndefinedWithNull(doctor.mobile_no),
//       created_at: Helpers.replaceUndefinedWithNull(doctor.createdAt),
//       updated_at: Helpers.replaceUndefinedWithNull(doctor.updatedAt)
//     };
//   }
// }

export class  DoctorTransformer extends TransformerAbstract<Doctor> {

  defaultIncludes = ["clinicDoctor", "clinics"];

  async includeClinicDoctor(doctor: Doctor): Promise<Dictionary<any>> {
    let clinicDoctors = doctor.clinicDoctor;
    if (!clinicDoctors) {
      return null;
    }

    if (isUndefined(clinicDoctors)) {
      clinicDoctors = await doctor.$get("clinicDoctor") as ClinicDoctor[];
    }
    if (!clinicDoctors) {
    return null;
    }

    return new ClinicDoctorTransformer().transformList(clinicDoctors);
  }


  async includeClinics(doctor: Doctor): Promise<Dictionary<any>> {
    let clinic = doctor.clinics;
    if (!clinic) {
      return null;
    }

    if (isUndefined(clinic)) {
      clinic = await doctor.$get("clinics") as Clinic[];
    }

    return new ClinicTransformer().transformList(clinic);
  }

  async includeExtra(doctor: Doctor): Promise<Dictionary<any>> {
    return null;
  }

  protected _map(doctor: Doctor): Dictionary<any> {
    return {
      id                       : Helpers.replaceUndefinedWithNull(doctor.id),
      name                     : Helpers.replaceUndefinedWithNull(doctor.name),
      age                      : Helpers.replaceUndefinedWithNull(doctor.age),
      gender                   : Helpers.replaceUndefinedWithNull(doctor.gender),
      degree                   : Helpers.replaceUndefinedWithNull(doctor.degree),
      email                    : Helpers.replaceUndefinedWithNull(doctor.email),
      password                 : Helpers.replaceUndefinedWithNull(doctor.password),
      mobile_no                : Helpers.replaceUndefinedWithNull(doctor.mobile_no),
      language                 : Helpers.replaceUndefinedWithNull(doctor.language),
      experience               : Helpers.replaceUndefinedWithNull(doctor.experience),
      designation              : Helpers.replaceUndefinedWithNull(doctor.designation),
      image_url                : Helpers.replaceUndefinedWithNull(doctor.image_url),
      specialities             : Helpers.replaceUndefinedWithNull(doctor.specialities),
      is_active                : Helpers.replaceUndefinedWithNull(doctor.is_active),
      get_cure_code            : Helpers.replaceUndefinedWithNull(doctor.get_cure_code),
      identity_verification_url: Helpers.replaceUndefinedWithNull(doctor.identity_verification_url),
      holidays                 : Helpers.replaceUndefinedWithNull(doctor.holidays),
      created_at               : Helpers.replaceUndefinedWithNull(doctor.createdAt),
      updated_at               : Helpers.replaceUndefinedWithNull(doctor.updatedAt)
    };
  }

}
