import { TransformerAbstract } from "./transformer.abstract";
import { Dictionary } from "async";
import { Helpers } from "../util/helpers.util";
import { Habit } from "../models/habit.model";


export class HabitTransformer extends TransformerAbstract<Habit> {

  protected _map(habit: Habit): Dictionary<any> {
    return {
      id        : Helpers.replaceUndefinedWithNull(habit.id),
      doctor_id : Helpers.replaceUndefinedWithNull(habit.doctor_id),
      title     : Helpers.replaceUndefinedWithNull(habit.title),
      type      : Helpers.replaceUndefinedWithNull(habit.type),
      is_active : habit.is_active,
      created_at: Helpers.replaceUndefinedWithNull(habit.createdAt),
      updated_at: Helpers.replaceUndefinedWithNull(habit.updatedAt),
      deleted_at: Helpers.replaceUndefinedWithNull(habit.deletedAt)
    };
  }

}
