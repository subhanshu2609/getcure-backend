import { TransformerAbstract } from "./transformer.abstract";
import { Dictionary } from "async";
import { Helpers } from "../util/helpers.util";
import { FrontDesk } from "../models/front-desk.model";
import { isUndefined } from "util";
import { Doctor } from "../models/doctor.model";
import { DoctorTransformer } from "./doctor.transformer";
import { ClinicDoctor } from "../models/clinic-doctor.model";
import { Clinic } from "../models/clinic.model";
import { ClinicTransformer } from "./clinic.transformer";
import { clinicService } from "../services/entities/clinic.service";
import { doctorService } from "../services/entities/doctor.service";

export class ClinicDoctorTransformer extends TransformerAbstract<ClinicDoctor> {

  defaultIncludes = [
    "doctor",
    "clinic"
  ];

  async includeDoctor(clinicDoctor: ClinicDoctor): Promise<Dictionary<any>> {
    let doctor = clinicDoctor.doctor;
    if (!doctor) {
      return null;
    }

    if (isUndefined(doctor)) {
      doctor = await clinicDoctor.$get("doctor") as Doctor;
    }

    return new DoctorTransformer().transform(doctor);
  }

  async includeClinic(clinicDoctor: ClinicDoctor): Promise<Dictionary<any>> {
    let clinic = clinicDoctor.clinic;
    if (!clinic) {
      return null;
    }

    if (isUndefined(clinic)) {
      clinic = await clinicDoctor.$get("clinic") as Clinic;
    }

    return new ClinicTransformer().transform(clinic);
  }

  protected _map(clinicDoctor: ClinicDoctor): Dictionary<any> {
    return {
      id                       : Helpers.replaceUndefinedWithNull(clinicDoctor.id),
      clinic_id                : Helpers.replaceUndefinedWithNull(clinicDoctor.clinic_id),
      doctor_id                : Helpers.replaceUndefinedWithNull(clinicDoctor.doctor_id),
      doctor_name              : Helpers.replaceUndefinedWithNull(clinicDoctor.doctor_name),
      front_desk_id            : Helpers.replaceUndefinedWithNull(clinicDoctor.front_desk_id),
      consultation_fee         : Helpers.replaceUndefinedWithNull(clinicDoctor.consultation_fee),
      emergency_fee            : Helpers.replaceUndefinedWithNull(clinicDoctor.emergency_fee),
      online_charge            : Helpers.replaceUndefinedWithNull(clinicDoctor.online_charge),
      paid_visit_charge        : Helpers.replaceUndefinedWithNull(clinicDoctor.paid_visit_charge),
      free_visit_charge        : Helpers.replaceUndefinedWithNull(clinicDoctor.free_visit_charge),
      on_call_paid_visit_charge: Helpers.replaceUndefinedWithNull(clinicDoctor.on_call_paid_visit_charge),
      on_call_free_visit_charge: Helpers.replaceUndefinedWithNull(clinicDoctor.on_call_free_visit_charge),
      follow_up_appointments   : Helpers.replaceUndefinedWithNull(clinicDoctor.follow_up_appointments),
      follow_up_days           : Helpers.replaceUndefinedWithNull(clinicDoctor.follow_up_days),
      slot_time                : Helpers.replaceUndefinedWithNull(clinicDoctor.slot_time),
      doctor_timings           : Helpers.replaceUndefinedWithNull(clinicDoctor.doctor_timings),
      medicine_categories      : Helpers.replaceUndefinedWithNull(clinicDoctor.medicine_categories),
      medicine_doses           : Helpers.replaceUndefinedWithNull(clinicDoctor.medicine_doses),
      medicine_units           : Helpers.replaceUndefinedWithNull(clinicDoctor.medicine_units),
      medicine_routes          : Helpers.replaceUndefinedWithNull(clinicDoctor.medicine_routes),
      medicine_frequencies     : Helpers.replaceUndefinedWithNull(clinicDoctor.medicine_frequencies),
      medicine_directions      : Helpers.replaceUndefinedWithNull(clinicDoctor.medicine_directions),
      medicine_durations       : Helpers.replaceUndefinedWithNull(clinicDoctor.medicine_durations),
      created_at               : Helpers.replaceUndefinedWithNull(clinicDoctor.createdAt),
      updated_at               : Helpers.replaceUndefinedWithNull(clinicDoctor.updatedAt)
    };
  }
}
export class ClinicDoctorNestTransformer extends ClinicDoctorTransformer {
  public filters: { state_id: string; query?: string };

  constructor(filters: { state_id: string; query?: string }) {
    super();
    this.filters = filters;
  }

  defaultIncludes = ["clinicName", "clinicDoctors"];

  async includeClinicDoctors(clinicDoctor: ClinicDoctor): Promise<Dictionary<any>> {
    const clinicDoctors = await clinicService.showAllClinicDoctorByDoctor(clinicDoctor.doctor_id);
    return new ClinicDoctorTransformer().transformList(clinicDoctors);
  }

  async includeClinicName(clinicDoctor: ClinicDoctor): Promise<string> {
    return clinicDoctor.clinic ? clinicDoctor.clinic.name : null;
  }

  protected _map(clinicDoctor: ClinicDoctor): Dictionary<any> {
    return {
      ...super._map(clinicDoctor)
    };
  }
}

