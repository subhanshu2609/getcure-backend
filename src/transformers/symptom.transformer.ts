import { TransformerAbstract } from "./transformer.abstract";
import { Dictionary } from "async";
import { Helpers } from "../util/helpers.util";
import { Symptom } from "../models/symptom.model";


export class SymptomTransformer extends TransformerAbstract<Symptom> {

  protected _map(symptom: Symptom): Dictionary<any> {
    return {
      id               : Helpers.replaceUndefinedWithNull(symptom.id),
      doctor_id        : Helpers.replaceUndefinedWithNull(symptom.doctor_id),
      title            : Helpers.replaceUndefinedWithNull(symptom.title),
      visibility_period: symptom.visibility_period,
      is_active        : symptom.is_active,
      created_at       : Helpers.replaceUndefinedWithNull(symptom.createdAt),
      updated_at       : Helpers.replaceUndefinedWithNull(symptom.updatedAt),
      deleted_at       : Helpers.replaceUndefinedWithNull(symptom.deletedAt)
    };
  }

}
