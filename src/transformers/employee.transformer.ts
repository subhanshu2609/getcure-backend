import { TransformerAbstract } from "./transformer.abstract";
import { Dictionary } from "async";
import { State } from "../models/state.model";
import { EmployeeCreateDto } from "../dtos/employee/employee-create.dto";
import { Employee } from "../models/employee.model";
import { Helpers } from "../util/helpers.util";
import { ClinicDoctor } from "../models/clinic-doctor.model";
import { isUndefined } from "util";
import { Doctor } from "../models/doctor.model";
import { DoctorTransformer } from "./doctor.transformer";
import { ClinicDoctorTransformer } from "./clinic-doctor.transformer";

export class EmployeeTransformer extends TransformerAbstract<Employee> {


  async includeClinicDoctors(employee: Employee): Promise<Dictionary<any>> {
    let clinicDoctor = employee.clinicDoctors;
    if (isUndefined(clinicDoctor)) {
      clinicDoctor = await employee.$get("clinicDoctors") as ClinicDoctor[];
    }
    if (!clinicDoctor) {
      return null;
    }

    return new ClinicDoctorTransformer().transformList(clinicDoctor);
  }

  protected _map(employee: Employee): Dictionary<any> {
    return {
      id           : Helpers.replaceUndefinedWithNull(employee.id),
      first_name   : Helpers.replaceUndefinedWithNull(employee.first_name),
      last_name    : Helpers.replaceUndefinedWithNull(employee.last_name),
      type         : Helpers.replaceUndefinedWithNull(employee.type),
      aadhar_no    : Helpers.replaceUndefinedWithNull(employee.aadhar_no),
      mobile_no    : Helpers.replaceUndefinedWithNull(employee.mobile_no),
      gender       : Helpers.replaceUndefinedWithNull(employee.gender),
      dob          : Helpers.replaceUndefinedWithNull(employee.dob),
      profile_image: Helpers.replaceUndefinedWithNull(employee.profile_image),
      address      : Helpers.replaceUndefinedWithNull(employee.address),
      pin_code     : Helpers.replaceUndefinedWithNull(employee.pin_code),
      is_active    : Helpers.replaceUndefinedWithNull(employee.is_active),
      created_at   : Helpers.replaceUndefinedWithNull(employee.createdAt),
      updated_at   : Helpers.replaceUndefinedWithNull(employee.updatedAt),
      deleted_at   : Helpers.replaceUndefinedWithNull(employee.deletedAt)
    };
  }

}
