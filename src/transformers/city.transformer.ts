import { TransformerAbstract } from "./transformer.abstract";
import { Dictionary } from "async";
import { City } from "../models/city.model";

export class CityTransformer extends TransformerAbstract<City> {

  protected _map(area: City): Dictionary<any> {
    return {
      id         : area.id,
      title      : area.title,
      slug       : area.slug,
      image_url  : area.state_id,
      is_active  : area.is_active,
      created_at : area.createdAt,
      updated_at : area.updatedAt,
      deleted_at : area.deletedAt
    };
  }

}
