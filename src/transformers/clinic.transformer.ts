import { TransformerAbstract } from "./transformer.abstract";
import { Dictionary } from "async";
import { Clinic } from "../models/clinic.model";
import { Helpers } from "../util/helpers.util";

export class ClinicTransformer extends TransformerAbstract<Clinic> {

  protected _map(clinic: Clinic): Dictionary<any> {
    return {
      id                : Helpers.replaceUndefinedWithNull(clinic.id),
      name              : Helpers.replaceUndefinedWithNull(clinic.name),
      type              : Helpers.replaceUndefinedWithNull(clinic.type),
      establishment_year: Helpers.replaceUndefinedWithNull(clinic.establishment_year),
      no_of_beds        : Helpers.replaceUndefinedWithNull(clinic.no_of_beds),
      no_of_doctors     : Helpers.replaceUndefinedWithNull(clinic.no_of_doctors),
      state             : Helpers.replaceUndefinedWithNull(clinic.state),
      state_id          : Helpers.replaceUndefinedWithNull(clinic.state_id),
      timings           : Helpers.replaceUndefinedWithNull(clinic.timings),
      about             : Helpers.replaceUndefinedWithNull(clinic.about),
      specialities      : Helpers.replaceUndefinedWithNull(clinic.specialities),
      address           : Helpers.replaceUndefinedWithNull(clinic.address),
      pin_code          : Helpers.replaceUndefinedWithNull(clinic.pin_code),
      phone_no          : Helpers.replaceUndefinedWithNull(clinic.phone_no),
      latitude          : Helpers.replaceUndefinedWithNull(clinic.latitude),
      longitude         : Helpers.replaceUndefinedWithNull(clinic.longitude),
      get_cure_code     : Helpers.replaceUndefinedWithNull(clinic.get_cure_code),
      image_urls        : Helpers.replaceUndefinedWithNull(clinic.image_urls),
      is_active         : Helpers.replaceUndefinedWithNull(clinic.is_active),
      created_at        : Helpers.replaceUndefinedWithNull(clinic.createdAt),
      updated_at        : Helpers.replaceUndefinedWithNull(clinic.updatedAt),
      deleted_at        : Helpers.replaceUndefinedWithNull(clinic.deletedAt)
    };
  }

}
