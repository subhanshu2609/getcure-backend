import { TransformerAbstract } from "./transformer.abstract";
import { Dictionary } from "async";
import { Helpers } from "../util/helpers.util";
import { FrontDesk } from "../models/front-desk.model";
import { isUndefined } from "util";
import { Doctor } from "../models/doctor.model";
import { DoctorTransformer } from "./doctor.transformer";
import { ClinicDoctor } from "../models/clinic-doctor.model";
import { ClinicDoctorTransformer } from "./clinic-doctor.transformer";
import { Clinic } from "../models/clinic.model";
import { ClinicTransformer } from "./clinic.transformer";
import { clinicService } from "../services/entities/clinic.service";

export class FrontDeskTransformer extends TransformerAbstract<FrontDesk> {

  defaultIncludes = [
    "clinicDoctors",
    "clinic"
  ];

  async includeClinicDoctors(frontDesk: FrontDesk): Promise<Dictionary<any>> {
    let clinicDoctors = frontDesk.clinicDoctors;

    if (isUndefined(clinicDoctors)) {
      clinicDoctors = await frontDesk.$get("clinicDoctors") as ClinicDoctor[];
    }
    if (!clinicDoctors) {
      return null;
    }

    return new ClinicDoctorTransformer().transformList(clinicDoctors);
  }


  async includeClinic(frontDesk: FrontDesk): Promise<Dictionary<any>> {
    let clinic = frontDesk.clinic;
    if (!clinic) {
      return null;
    }

    if (isUndefined(clinic)) {
      clinic = await clinicService.show(frontDesk.clinic_id);
    }

    return new ClinicTransformer().transform(clinic);
  }

  protected _map(frontDesk: FrontDesk): Dictionary<any> {
    return {
      id        : Helpers.replaceUndefinedWithNull(frontDesk.id),
      name      : Helpers.replaceUndefinedWithNull(frontDesk.name),
      email     : Helpers.replaceUndefinedWithNull(frontDesk.email),
      mobile_no : Helpers.replaceUndefinedWithNull(frontDesk.mobile_no),
      clinic_id : Helpers.replaceUndefinedWithNull(frontDesk.clinic_id),
      gender    : Helpers.replaceUndefinedWithNull(frontDesk.gender),
      password  : Helpers.replaceUndefinedWithNull(frontDesk.password),
      image_url : Helpers.replaceUndefinedWithNull(frontDesk.image_url),
      is_active : Helpers.replaceUndefinedWithNull(frontDesk.is_active),
      created_at: Helpers.replaceUndefinedWithNull(frontDesk.createdAt),
      updated_at: Helpers.replaceUndefinedWithNull(frontDesk.updatedAt)
    };
  }

}
