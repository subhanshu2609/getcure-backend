import { TransformerAbstract } from "./transformer.abstract";
import { Dictionary } from "async";
import { Patient } from "../models/patient.model";

export class PatientTransformer extends TransformerAbstract<Patient> {

  protected _map(patient: Patient): Dictionary<any> {
    return {
      id        : patient.id,
      name      : patient.name,
      patient_id: patient.patient_id,
      mobile_no : patient.mobile_no,
      age       : patient.age,
      gender    : patient.gender,
      address   : patient.address,
      created_at: patient.createdAt,
      updated_at: patient.updatedAt,
      deleted_at: patient.deletedAt
    };
  }

}
