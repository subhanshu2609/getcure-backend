import { TransformerAbstract } from "./transformer.abstract";
import { Dictionary } from "async";
import { Patient } from "../models/patient.model";
import { PatientVisit } from "../models/patient-visit.model";
import { Token } from "../models/token.model";
import { isUndefined } from "util";
import { ClinicDoctor } from "../models/clinic-doctor.model";
import { ClinicDoctorTransformer } from "./clinic-doctor.transformer";
import { Helpers } from "../util/helpers.util";
import { PatientTransformer } from "./patient.transformer";
import { Feedback } from "../models/feedback.model";

export class PatientVisitTransformer extends TransformerAbstract<PatientVisit> {

  async includeClinicDoctor(token: Token): Promise<Dictionary<any>> {
    let doctor = token.clinic_doctor;
    if (!doctor) {
      return null;
    }

    if (isUndefined(doctor)) {
      doctor = await token.$get("clinic_doctor") as ClinicDoctor;
    }

    return new ClinicDoctorTransformer().transform(doctor);
  }

  async includePatient(patientVisit: PatientVisit): Promise<Dictionary<any>> {
    let patient = patientVisit.patient;
    if (isUndefined(patient)) {
      patient = await patientVisit.$get("patient") as Patient;
    }
    return new PatientTransformer().transform(patient);
  }

  async includeFeedback(visit: PatientVisit): Promise<Dictionary<any>> {
    let feedback = visit.feedback;
    if (isUndefined(feedback)) {
      feedback = await visit.$get("feedback") as Feedback[];
    }
    return feedback;
  }

  protected _map(patient: PatientVisit): Dictionary<any> {
    return {
      id                 : patient.id,
      appointment_id     : Helpers.replaceUndefinedWithNull(patient.appointment_id),
      clinic_doctor_id   : patient.clinic_doctor_id,
      patient_id         : patient.patient_id,
      patient_name       : patient.patient_name,
      doctor_name        : Helpers.replaceUndefinedWithNull(patient.clinic_doctor?.doctor_name),
      clinic_name        : Helpers.replaceUndefinedWithNull(patient.clinic_doctor?.clinic?.name),
      state              : Helpers.replaceUndefinedWithNull(patient.clinic_doctor?.clinic?.state),
      temperature        : patient.temperature,
      blood_pressure     : patient.blood_pressure,
      pulse              : patient.pulse,
      weight             : patient.weight,
      brief_history      : patient.brief_history,
      visit_reason       : patient.visit_reason,
      examination        : patient.examination,
      diagnosis          : patient.diagnosis,
      medication         : patient.medication,
      allergies          : patient.allergies,
      lifestyle          : patient.lifestyle,
      booking_type       : patient.booking_type,
      visit_type         : patient.visit_type,
      appointment_type   : patient.appointment_type,
      booked_at          : patient.booked_at,
      appointment_time   : patient.appointment_time,
      present_time       : Helpers.replaceUndefinedWithNull(patient.present_time),
      booked_by          : patient.booked_by,
      booked_via         : patient.booked_via,
      updated_by         : Helpers.replaceUndefinedWithNull(patient.updated_by),
      is_doctor_feedback : patient.is_doctor_feedback,
      is_patient_feedback: patient.is_patient_feedback,
      created_at         : patient.createdAt,
      updated_at         : patient.updatedAt,
      deleted_at         : patient.deletedAt
    };
  }
}
