import { TransformerAbstract } from "./transformer.abstract";
import { Dictionary } from "async";
import { Helpers } from "../util/helpers.util";
import { Examination } from "../models/examinations.model";


export class ExaminationTransformer extends TransformerAbstract<Examination> {

  protected _map(examination: Examination): Dictionary<any> {
    return {
      id        : Helpers.replaceUndefinedWithNull(examination.id),
      doctor_id : Helpers.replaceUndefinedWithNull(examination.doctor_id),
      title     : Helpers.replaceUndefinedWithNull(examination.title),
      parameters: Helpers.replaceUndefinedWithNull(examination.parameters),
      price     : Helpers.replaceUndefinedWithNull(examination.price),
      created_at: Helpers.replaceUndefinedWithNull(examination.createdAt),
      updated_at: Helpers.replaceUndefinedWithNull(examination.updatedAt),
      deleted_at: Helpers.replaceUndefinedWithNull(examination.deletedAt)
    };
  }

}
