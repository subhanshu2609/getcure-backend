import { TransformerAbstract } from "./transformer.abstract";
import { Dictionary } from "async";
import { State } from "../models/state.model";

export class StateTransformer extends TransformerAbstract<State> {

  protected _map(city: State): Dictionary<any> {
    return {
      id        : city.id,
      title     : city.title,
      slug      : city.slug,
      is_active : city.is_active,
      created_at: city.createdAt,
      updated_at: city.updatedAt,
      deleted_at: city.deletedAt
    };
  }

}
