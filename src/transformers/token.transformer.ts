import { TransformerAbstract } from "./transformer.abstract";
import { Dictionary } from "async";
import { Token } from "../models/token.model";
import { Helpers } from "../util/helpers.util";
import { ClinicDoctor } from "../models/clinic-doctor.model";
import { isNullOrUndefined, isUndefined } from "util";
import { Clinic } from "../models/clinic.model";
import { ClinicTransformer } from "./clinic.transformer";
import { Patient } from "../models/patient.model";
import { PatientTransformer } from "./patient.transformer";
import { TokenNotFoundException } from "../exceptions/token/token-not-found.exception";
import { Doctor } from "../models/doctor.model";
import { DoctorTransformer } from "./doctor.transformer";
import { ClinicDoctorTransformer } from "./clinic-doctor.transformer";
import { clinicService } from "../services/entities/clinic.service";


export class TokenTransformer extends TransformerAbstract<Token> {

  async includePatient(token: Token): Promise<Dictionary<any>> {
    let doctor = token.patient;

    if (isUndefined(doctor)) {
      doctor = await token.$get("patient") as Patient;
    }

    return new PatientTransformer().transform(doctor);
  }

  async includeClinicDoctor(token: Token): Promise<Dictionary<any>> {
    let doctor = token.clinic_doctor;
    if (!doctor) {
      return null;
    }

    if (isUndefined(doctor)) {
      doctor = await token.$get("clinic_doctor") as ClinicDoctor;
    }

    return new ClinicDoctorTransformer().transform(doctor);
  }

  async includeCds(token: Token): Promise<Dictionary<any>> {
    let cd = token.clinic_doctor;
    if (isNullOrUndefined(cd)) {
      cd = await clinicService.showClinicDoctorById(token.clinic_doctor_id, true);
    }
    if (isNullOrUndefined(cd.clinic)) {
      cd.clinic = await clinicService.show(cd.clinic_id);
    }
    return {
      doctor_name: cd.doctor_name,
      clinic_name: cd.clinic.name,
      state      : cd.clinic.state
    };
  }


  protected _map(token: Token): Dictionary<any> {
    return {
      id              : Helpers.replaceUndefinedWithNull(token.id),
      appointment_id  : Helpers.replaceUndefinedWithNull(token.appointment_id),
      token_no        : Helpers.replaceUndefinedWithNull(token.token_no),
      patient_name    : Helpers.replaceUndefinedWithNull(token.patient_name),
      patient_id      : Helpers.replaceUndefinedWithNull(token.patient_id),
      time            : Helpers.replaceUndefinedWithNull(token.time),
      date            : Helpers.replaceUndefinedWithNull(token.date),
      appointment_type: Helpers.replaceUndefinedWithNull(token.appointment_type),
      visit_type      : Helpers.replaceUndefinedWithNull(token.visit_type),
      booking_type    : Helpers.replaceUndefinedWithNull(token.booking_type),
      clinic_doctor_id: Helpers.replaceUndefinedWithNull(token.clinic_doctor_id),
      is_present      : Helpers.replaceUndefinedWithNull(token.is_present),
      present_time    : Helpers.replaceUndefinedWithNull(token.present_time),
      fees            : Helpers.replaceUndefinedWithNull(token.fees),
      booked_at       : Helpers.replaceUndefinedWithNull(token.booked_at),
      booked_by       : Helpers.replaceUndefinedWithNull(token.booked_by),
      booked_via      : Helpers.replaceUndefinedWithNull(token.booked_via),
      transaction_id  : Helpers.replaceUndefinedWithNull(token.transaction_id),
      created_at      : Helpers.replaceUndefinedWithNull(token.createdAt),
      updated_at      : Helpers.replaceUndefinedWithNull(token.updatedAt),
      deleted_at      : Helpers.replaceUndefinedWithNull(token.deletedAt)
    };
  }

}

export class TokenFullTransformer extends TokenTransformer {

  protected _map(token: Token): Dictionary<any> {
    return {
      ...super._map(token),
      doctor_name: Helpers.replaceUndefinedWithNull(token.clinic_doctor.doctor_name),
      clinic_name: Helpers.replaceUndefinedWithNull(token.clinic_doctor.clinic.name),
      state      : Helpers.replaceUndefinedWithNull(token.clinic_doctor.clinic.state),
    };
  }
}
