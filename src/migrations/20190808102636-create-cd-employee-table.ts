import { QueryInterface, SequelizeStatic } from "sequelize";

export = {
  up: (queryInterface: QueryInterface, Sequelize: SequelizeStatic) => {
    return queryInterface.createTable("cd_employee", {
      id                       : {
        allowNull    : false,
        primaryKey   : true,
        autoIncrement: true,
        type         : Sequelize.BIGINT
      },
      clinic_doctor_id                : {
        type      : Sequelize.BIGINT,
        allowNull : false,
        references: {
          model: "clinics_doctors",
          key  : "id"
        },
        onDelete  : "cascade"
      },
      employee_id                : {
        type      : Sequelize.BIGINT,
        allowNull : false,
        references: {
          model: "employees",
          key  : "id"
        },
        onDelete  : "cascade"
      },
      createdAt                : {
        allowNull: true,
        type     : Sequelize.DATE
      },
      updatedAt                : {
        allowNull: true,
        type     : Sequelize.DATE
      },
      deletedAt                : {
        allowNull: true,
        type     : Sequelize.DATE
      }
    });
  },

  down: (queryInterface: QueryInterface, Sequelize: SequelizeStatic) => {
    return Promise.all([
      queryInterface.dropTable("cd_employee"),
    ]);
  }
};
