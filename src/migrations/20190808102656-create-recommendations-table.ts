import { QueryInterface, SequelizeStatic } from "sequelize";
import { Helpers } from "../util/helpers.util";
import { AppointmentTypes, BookingType, VisitType } from "../enums/token.enum";
import { Bookie } from "../enums/bookie.enum";
import { FeedbackFiller } from "../enums/feedback-filler.enum";

export = {
  up: (queryInterface: QueryInterface, Sequelize: SequelizeStatic) => {
    return queryInterface.createTable("recommendations", {
      id              : {
        allowNull    : false,
        primaryKey   : true,
        autoIncrement: true,
        type         : Sequelize.BIGINT
      },
      clinic_doctor_id: {
        allowNull : false,
        type      : Sequelize.BIGINT,
        references: {
          model: "clinics_doctors",
          key  : "id"
        }
      },
      disease        : {
        allowNull: false,
        type     : Sequelize.STRING
      },
      medicines          : {
        type     : Sequelize.JSON,
        allowNull: false
      },
      total_count        : {
        type     : Sequelize.BIGINT,
        allowNull: false,
        defaultValue: 1
      },
      cured        : {
        type     : Sequelize.BIGINT,
        allowNull: false,
        defaultValue: 0
      },
      partially_cured        : {
        type     : Sequelize.BIGINT,
        allowNull: false,
        defaultValue: 0
      },
      not_cured        : {
        type     : Sequelize.BIGINT,
        allowNull: false,
        defaultValue: 0
      },
      symptoms_increased        : {
        type     : Sequelize.BIGINT,
        allowNull: false,
        defaultValue: 0
      },
      createdAt       : {
        allowNull: true,
        type     : Sequelize.DATE
      },
      updatedAt       : {
        allowNull: true,
        type     : Sequelize.DATE
      }
    });
  },

  down: (queryInterface: QueryInterface, Sequelize: SequelizeStatic) => {
    return Promise.all([
      queryInterface.dropTable("recommendations"),
    ]);
  }
};
