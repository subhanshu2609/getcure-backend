import { QueryInterface, SequelizeStatic } from "sequelize";

export = {
  up: (queryInterface: QueryInterface, Sequelize: SequelizeStatic) => {
    return queryInterface.createTable("patients", {
      id              : {
        allowNull    : false,
        primaryKey   : true,
        autoIncrement: true,
        type         : Sequelize.BIGINT
      },
      name            : {
        type     : Sequelize.STRING,
        allowNull: false
      },
      mobile_no       : {
        type     : Sequelize.STRING,
        allowNull: false,
      },
      age             : {
        type     : Sequelize.INTEGER,
        allowNull: false
      },
      gender          : {
        type     : Sequelize.ENUM,
        allowNull: false,
        values   : [
          "male",
          "female"
        ]
      },
      address         : {
        allowNull: false,
        type     : Sequelize.STRING
      },
      patient_id      : {
        unique    : true,
        primaryKey: true,
        allowNull : false,
        type      : Sequelize.STRING
      },
      createdAt       : {
        allowNull: true,
        type     : Sequelize.DATE
      },
      updatedAt       : {
        allowNull: true,
        type     : Sequelize.DATE
      },
      deletedAt       : {
        allowNull: true,
        type     : Sequelize.DATE
      }
    });
  },

  down: (queryInterface: QueryInterface, Sequelize: SequelizeStatic) => {
    return Promise.all([
      queryInterface.dropTable("patients"),
    ]);
  }
};
