import { QueryInterface, SequelizeStatic } from "sequelize";

export = {
  up: (queryInterface: QueryInterface, Sequelize: SequelizeStatic) => {
    return queryInterface.createTable("doctors", {
      id                       : {
        allowNull    : false,
        primaryKey   : true,
        autoIncrement: true,
        type         : Sequelize.BIGINT
      },
      name                     : {
        type     : Sequelize.STRING,
        allowNull: false
      },
      age                      : {
        type     : Sequelize.INTEGER,
        allowNull: false
      },
      gender                   : {
        type  : Sequelize.ENUM,
        values: [
          "male",
          "female",
          "other"
        ]
      },
      degree                   : {
        type     : Sequelize.STRING,
        allowNull: false
      },
      email                    : {
        type     : Sequelize.STRING,
        unique   : true,
        allowNull: false
      },
      mobile_no                : {
        type     : Sequelize.STRING,
        allowNull: false,
        unique   : true,
      },
      password                 : {
        type     : Sequelize.STRING,
        allowNull: false
      },
      specialities             : {
        type     : Sequelize.JSON,
        allowNull: false,
      },
      holidays             : {
        type     : Sequelize.JSON,
        allowNull: false,
      },
      language                 : {
        type     : Sequelize.STRING,
        allowNull: true
      },
      experience               : {
        type     : Sequelize.STRING,
        allowNull: true
      },
      designation              : {
        type     : Sequelize.STRING,
        allowNull: true
      },
      image_url                : {
        type     : Sequelize.STRING,
        allowNull: true
      },
      is_active                : {
        allowNull   : false,
        defaultValue: true,
        type        : Sequelize.BOOLEAN
      },
      get_cure_code            : {
        allowNull: true,
        type     : Sequelize.STRING,
        unique   : true
      },
      identity_verification_url: {
        allowNull: false,
        type     : Sequelize.JSON,
        defaultValue: []
      },
      createdAt                : {
        allowNull: true,
        type     : Sequelize.DATE
      },
      updatedAt                : {
        allowNull: true,
        type     : Sequelize.DATE
      },
      deletedAt                : {
        allowNull: true,
        type     : Sequelize.DATE
      }
    });
  },

  down: (queryInterface: QueryInterface, Sequelize: SequelizeStatic) => {
    return Promise.all([
      queryInterface.dropTable("doctors"),
    ]);
  }
};
