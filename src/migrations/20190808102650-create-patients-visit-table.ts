import { QueryInterface, SequelizeStatic } from "sequelize";
import { Helpers } from "../util/helpers.util";
import { AppointmentTypes, BookingType, VisitType } from "../enums/token.enum";
import { Bookie } from "../enums/bookie.enum";

export = {
  up: (queryInterface: QueryInterface, Sequelize: SequelizeStatic) => {
    return queryInterface.createTable("patients_visits", {
      id                 : {
        allowNull    : false,
        primaryKey   : true,
        autoIncrement: true,
        type         : Sequelize.BIGINT
      },
      appointment_id     : {
        allowNull: true,
        type     : Sequelize.STRING
      },
      clinic_doctor_id   : {
        type      : Sequelize.BIGINT,
        allowNull : true,
        references: {
          model: "clinics_doctors",
          key  : "id"
        },
        onDelete  : "set null"
      },
      patient_id         : {
        type      : Sequelize.STRING,
        allowNull : false,
        references: {
          model: "patients",
          key  : "patient_id"
        },
        onDelete  : "cascade"
      },
      patient_name       : {
        type     : Sequelize.STRING,
        allowNull: false
      },
      temperature        : {
        type        : Sequelize.FLOAT,
        allowNull   : false,
        defaultValue: 98
      },
      blood_pressure     : {
        type        : Sequelize.STRING,
        allowNull   : false,
        defaultValue: "120/80"
      },
      pulse              : {
        type     : Sequelize.FLOAT,
        allowNull: true
      },
      weight             : {
        type     : Sequelize.FLOAT,
        allowNull: true
      },
      brief_history      : {
        type     : Sequelize.JSON,
        allowNull: false
      },
      visit_reason       : {
        type     : Sequelize.JSON,
        allowNull: false
      },
      examination        : {
        type     : Sequelize.JSON,
        allowNull: false
      },
      diagnosis          : {
        type     : Sequelize.JSON,
        allowNull: false
      },
      medication         : {
        type     : Sequelize.JSON,
        allowNull: false
      },
      allergies          : {
        type: Sequelize.JSON
      },
      lifestyle          : {
        type: Sequelize.JSON
      },
      appointment_type   : {
        type     : Sequelize.ENUM,
        allowNull: false,
        values   : Helpers.iterateEnum(AppointmentTypes)
      },
      visit_type         : {
        type     : Sequelize.ENUM,
        allowNull: false,
        values   : Helpers.iterateEnum(VisitType)
      },
      booking_type       : {
        type     : Sequelize.ENUM,
        allowNull: false,
        values   : Helpers.iterateEnum(BookingType)
      },
      booked_by          : {
        type        : Sequelize.ENUM,
        allowNull   : false,
        values      : Helpers.iterateEnum(Bookie),
        defaultValue: Bookie.FRONT_DESK
      },
      booked_via         : {
        type     : Sequelize.STRING,
        allowNull: true
      },
      updated_by         : {
        type     : Sequelize.ENUM,
        allowNull: true,
        values   : Helpers.iterateEnum(Bookie)
      },
      booked_at          : {
        allowNull: true,
        type     : Sequelize.DATE
      },
      appointment_time   : {
        allowNull: true,
        type     : Sequelize.DATE
      },
      present_time       : {
        allowNull: true,
        type     : Sequelize.DATE
      },
      is_doctor_feedback : {
        allowNull   : false,
        type        : Sequelize.BOOLEAN,
        defaultValue: false
      },
      is_patient_feedback: {
        allowNull   : false,
        type        : Sequelize.BOOLEAN,
        defaultValue: false
      },
      feedback           : {
        allowNull: true,
        type     : Sequelize.JSON
      },
      fees               : {
        allowNull: false,
        type     : Sequelize.FLOAT,
      },
      createdAt          : {
        allowNull: true,
        type     : Sequelize.DATE
      },
      updatedAt          : {
        allowNull: true,
        type     : Sequelize.DATE
      },
      deletedAt          : {
        allowNull: true,
        type     : Sequelize.DATE
      }
    });
  },

  down: (queryInterface: QueryInterface, Sequelize: SequelizeStatic) => {
    return Promise.all([
      queryInterface.dropTable("patients_visits"),
    ]);
  }
};
