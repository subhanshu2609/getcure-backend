import { QueryInterface, SequelizeStatic } from "sequelize";
import { Helpers } from "../util/helpers.util";
import { AppointmentTypes, BookingType, VisitType } from "../enums/token.enum";
import { Bookie } from "../enums/bookie.enum";

export = {
  up: (queryInterface: QueryInterface, Sequelize: SequelizeStatic) => {
    return queryInterface.createTable("tokens", {
      id              : {
        allowNull    : false,
        primaryKey   : true,
        autoIncrement: true,
        type         : Sequelize.BIGINT
      },
      appointment_id: {
        type: Sequelize.STRING,
        allowNull: true
      },
      clinic_doctor_id: {
        type      : Sequelize.BIGINT,
        allowNull : false,
        references: {
          model: "clinics_doctors",
          key  : "id"
        },
        onDelete  : "cascade"
      },
      patient_id      : {
        type      : Sequelize.STRING,
        allowNull : false,
        references: {
          model: "patients",
          key  : "patient_id"
        },
        onDelete  : "cascade"
      },
      patient_name    : {
        type     : Sequelize.STRING,
        allowNull: false
      },
      appointment_type: {
        type     : Sequelize.ENUM,
        allowNull: false,
        values   : Helpers.iterateEnum(AppointmentTypes)
      },
      visit_type      : {
        type     : Sequelize.ENUM,
        allowNull: false,
        values   : Helpers.iterateEnum(VisitType)
      },
      booking_type    : {
        type     : Sequelize.ENUM,
        allowNull: false,
        values   : Helpers.iterateEnum(BookingType)
      },
      date            : {
        type     : Sequelize.DATEONLY,
        allowNull: false
      },
      time            : {
        type     : Sequelize.TIME,
        allowNull: false
      },
      token_no        : {
        type     : Sequelize.INTEGER,
        allowNull: false
      },
      is_present      : {
        type     : Sequelize.BOOLEAN,
        allowNull: false
      },
      present_time    : {
        allowNull: true,
        type     : Sequelize.DATE
      },
      fees            : {
        type     : Sequelize.FLOAT,
        allowNull: false
      },
      booked_at       : {
        allowNull: true,
        type     : Sequelize.DATE
      },
      booked_by       : {
        allowNull: false,
        type     : Sequelize.ENUM,
        values   : Helpers.iterateEnum(Bookie)
      },
      booked_via      : {
        allowNull   : false,
        type        : Sequelize.STRING,
        defaultValue: ""
      },
      transaction_id  : {
        type     : Sequelize.STRING,
        allowNull: true
      },
      createdAt       : {
        allowNull: true,
        type     : Sequelize.DATE
      },
      updatedAt       : {
        allowNull: true,
        type     : Sequelize.DATE
      },
      deletedAt       : {
        allowNull: true,
        type     : Sequelize.DATE
      }
    });
  },

  down: (queryInterface: QueryInterface, Sequelize: SequelizeStatic) => {
    return Promise.all([
      queryInterface.dropTable("tokens"),
    ]);
  }
};
