import { QueryInterface, SequelizeStatic } from "sequelize";

export = {
  up: (queryInterface: QueryInterface, Sequelize: SequelizeStatic) => {
    return queryInterface.createTable("examinations", {
      id              : {
        allowNull    : false,
        primaryKey   : true,
        autoIncrement: true,
        type         : Sequelize.BIGINT
      },
      clinic_doctor_id: {
        type      : Sequelize.BIGINT,
        allowNull : true,
        references: {
          model: "clinics_doctors",
          key  : "id"
        },
        onDelete  : "cascade"
      },
      doctor_id       : {
        type      : Sequelize.BIGINT,
        allowNull : false,
        references: {
          model: "doctors",
          key  : "id"
        },
        onDelete  : "cascade"
      },
      title           : {
        allowNull: false,
        type     : Sequelize.STRING
      },
      parameters      : {
        allowNull: false,
        type     : Sequelize.JSON
      },
      price           : {
        allowNull: true,
        type     : Sequelize.FLOAT
      },
      createdAt       : {
        allowNull: true,
        type     : Sequelize.DATE
      },
      updatedAt       : {
        allowNull: true,
        type     : Sequelize.DATE
      },
      deletedAt       : {
        allowNull: true,
        type     : Sequelize.DATE
      }
    });
  },

  down: (queryInterface: QueryInterface, Sequelize: SequelizeStatic) => {
    return Promise.all([
      queryInterface.dropTable("examinations"),
    ]);
  }
};
