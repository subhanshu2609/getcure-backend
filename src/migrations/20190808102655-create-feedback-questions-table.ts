import { QueryInterface, SequelizeStatic } from "sequelize";
import { Helpers } from "../util/helpers.util";
import { AppointmentTypes, BookingType, VisitType } from "../enums/token.enum";
import { Bookie } from "../enums/bookie.enum";

export = {
  up: (queryInterface: QueryInterface, Sequelize: SequelizeStatic) => {
    return queryInterface.createTable("feedback_questions", {
      id       : {
        allowNull    : false,
        primaryKey   : true,
        autoIncrement: true,
        type         : Sequelize.BIGINT
      },
      title    : {
        type     : Sequelize.STRING,
        allowNull: false
      },
      options  : {
        type     : Sequelize.JSON,
        allowNull: false
      },
      is_active: {
        type        : Sequelize.BOOLEAN,
        allowNull   : false,
        defaultValue: true
      },
      createdAt: {
        allowNull: true,
        type     : Sequelize.DATE
      },
      updatedAt: {
        allowNull: true,
        type     : Sequelize.DATE
      },
      deletedAt: {
        allowNull: true,
        type     : Sequelize.DATE
      }
    });
  },

  down: (queryInterface: QueryInterface, Sequelize: SequelizeStatic) => {
    return Promise.all([
      queryInterface.dropTable("feedback_questions"),
    ]);
  }
};
