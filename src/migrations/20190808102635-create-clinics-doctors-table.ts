import { QueryInterface, SequelizeStatic } from "sequelize";

export = {
  up: (queryInterface: QueryInterface, Sequelize: SequelizeStatic) => {
    return queryInterface.createTable("clinics_doctors", {
      id                       : {
        allowNull    : false,
        primaryKey   : true,
        autoIncrement: true,
        type         : Sequelize.BIGINT
      },
      clinic_id                : {
        type      : Sequelize.BIGINT,
        allowNull : false,
        references: {
          model: "clinics",
          key  : "id"
        },
        onDelete  : "cascade"
      },
      doctor_id                : {
        type      : Sequelize.BIGINT,
        allowNull : false,
        references: {
          model: "doctors",
          key  : "id"
        },
        onDelete  : "cascade"
      },
      doctor_name              : {
        type     : Sequelize.STRING,
        allowNull: false
      },
      front_desk_id            : {
        type      : Sequelize.BIGINT,
        allowNull : true,
        references: {
          model: "front_desks",
          key  : "id"
        },
        onDelete  : "set null"
      },
      slot_time                : {
        type     : Sequelize.INTEGER,
        allowNull: true
      },
      doctor_timings           : {
        type     : Sequelize.JSON,
        allowNull: true
      },
      consultation_fee         : {
        type     : Sequelize.FLOAT,
        allowNull: true
      },
      emergency_fee            : {
        type     : Sequelize.FLOAT,
        allowNull: true
      },
      online_charge            : {
        type     : Sequelize.FLOAT,
        allowNull: true
      },
      paid_visit_charge        : {
        type     : Sequelize.FLOAT,
        allowNull: true
      },
      free_visit_charge        : {
        type     : Sequelize.FLOAT,
        allowNull: true
      },
      on_call_paid_visit_charge: {
        type     : Sequelize.FLOAT,
        allowNull: true
      },
      on_call_free_visit_charge: {
        type     : Sequelize.FLOAT,
        allowNull: true
      },
      follow_up_appointments   : {
        type     : Sequelize.FLOAT,
        allowNull: true
      },
      follow_up_days           : {
        type     : Sequelize.FLOAT,
        allowNull: true
      },
      medicine_categories      : {
        type     : Sequelize.JSON,
        allowNull: false,
        defaultValue: []
      },
      medicine_doses           : {
        type     : Sequelize.JSON,
        allowNull: false,
        defaultValue: []
      },
      medicine_units           : {
        type     : Sequelize.JSON,
        allowNull: false,
        defaultValue: []
      },
      medicine_routes          : {
        type     : Sequelize.JSON,
        allowNull: false,
        defaultValue: []
      },
      medicine_frequencies     : {
        type     : Sequelize.JSON,
        allowNull: false,
        defaultValue: []
      },
      medicine_directions      : {
        type     : Sequelize.JSON,
        allowNull: false,
        defaultValue: []
      },
      medicine_durations       : {
        type     : Sequelize.JSON,
        allowNull: false,
        defaultValue: []
      },
      createdAt                : {
        allowNull: true,
        type     : Sequelize.DATE
      },
      updatedAt                : {
        allowNull: true,
        type     : Sequelize.DATE
      },
      deletedAt                : {
        allowNull: true,
        type     : Sequelize.DATE
      }
    });
  },

  down: (queryInterface: QueryInterface, Sequelize: SequelizeStatic) => {
    return Promise.all([
      queryInterface.dropTable("clinics_doctors"),
    ]);
  }
};
