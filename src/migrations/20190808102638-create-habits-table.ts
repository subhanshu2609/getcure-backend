import { QueryInterface, SequelizeStatic } from "sequelize";
import { Helpers } from "../util/helpers.util";
import { HabitType, SymptomType } from "../enums/symptom.enum";
import { VisibilityEnum } from "../enums/visibility.enum";

export = {
  up: (queryInterface: QueryInterface, Sequelize: SequelizeStatic) => {
    return queryInterface.createTable("habits", {
      id              : {
        allowNull    : false,
        primaryKey   : true,
        autoIncrement: true,
        type         : Sequelize.BIGINT
      },
      clinic_doctor_id: {
        type      : Sequelize.BIGINT,
        allowNull : true,
        references: {
          model: "clinics_doctors",
          key  : "id"
        },
        onDelete  : "cascade"
      },
      doctor_id       : {
        type      : Sequelize.BIGINT,
        allowNull : false,
        references: {
          model: "doctors",
          key  : "id"
        },
        onDelete  : "cascade"
      },
      title           : {
        allowNull: false,
        type     : Sequelize.STRING
      },
      type            : {
        allowNull: false,
        type     : Sequelize.ENUM,
        values   : Helpers.iterateEnum(HabitType)
      },
      is_active       : {
        allowNull   : false,
        type        : Sequelize.BOOLEAN,
        defaultValue: true
      },
      createdAt       : {
        allowNull: true,
        type     : Sequelize.DATE
      },
      updatedAt       : {
        allowNull: true,
        type     : Sequelize.DATE
      },
      deletedAt       : {
        allowNull: true,
        type     : Sequelize.DATE
      }
    });
  },

  down: (queryInterface: QueryInterface, Sequelize: SequelizeStatic) => {
    return Promise.all([
      queryInterface.dropTable("habits"),
    ]);
  }
};
