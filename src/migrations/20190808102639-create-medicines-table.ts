import { QueryInterface, SequelizeStatic } from "sequelize";

export = {
  up: (queryInterface: QueryInterface, Sequelize: SequelizeStatic) => {
    return queryInterface.createTable("medicines", {
      id               : {
        allowNull    : false,
        primaryKey   : true,
        autoIncrement: true,
        type         : Sequelize.BIGINT
      },
      clinic_doctor_id : {
        type      : Sequelize.BIGINT,
        allowNull : true,
        references: {
          model: "clinics_doctors",
          key  : "id"
        },
        onDelete  : "cascade"
      },
      doctor_id        : {
        type      : Sequelize.BIGINT,
        allowNull : false,
        references: {
          model: "doctors",
          key  : "id"
        },
        onDelete  : "cascade"
      },
      title            : {
        allowNull: false,
        type     : Sequelize.STRING
      },
      salt             : {
        allowNull: false,
        type     : Sequelize.STRING
      },
      interaction_drugs: {
        allowNull: false,
        type     : Sequelize.JSON
      },
      category         : {
        allowNull: false,
        type     : Sequelize.STRING
      },
      default_dose     : {
        allowNull: false,
        type     : Sequelize.STRING
      },
      default_unit     : {
        allowNull: false,
        type     : Sequelize.STRING
      },
      default_route    : {
        allowNull: false,
        type     : Sequelize.STRING
      },
      default_frequency: {
        allowNull: false,
        type     : Sequelize.STRING
      },
      default_direction: {
        allowNull: false,
        type     : Sequelize.STRING
      },
      default_duration : {
        allowNull: false,
        type     : Sequelize.STRING
      },
      createdAt        : {
        allowNull: true,
        type     : Sequelize.DATE
      },
      updatedAt        : {
        allowNull: true,
        type     : Sequelize.DATE
      },
      deletedAt        : {
        allowNull: true,
        type     : Sequelize.DATE
      }
    });
  },

  down: (queryInterface: QueryInterface, Sequelize: SequelizeStatic) => {
    return Promise.all([
      queryInterface.dropTable("medicines"),
    ]);
  }
};
