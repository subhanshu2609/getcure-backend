import { QueryInterface, SequelizeStatic } from "sequelize";
import { Helpers } from "../util/helpers.util";
import { AppointmentTypes, BookingType, VisitType } from "../enums/token.enum";
import { Bookie } from "../enums/bookie.enum";
import { FeedbackFiller } from "../enums/feedback-filler.enum";
import { MedicineParameter } from "../enums/medicine-parameter.enum";

export = {
  up: (queryInterface: QueryInterface, Sequelize: SequelizeStatic) => {
    return queryInterface.createTable("fd_cd", {
      id       : {
        allowNull    : false,
        primaryKey   : true,
        autoIncrement: true,
        type         : Sequelize.BIGINT
      },
      front_desk_id: {
        allowNull : false,
        type      : Sequelize.BIGINT,
        references: {
          model: "front_desks",
          key  : "id"
        }
      },
      clinic_doctor_id: {
        allowNull : false,
        type      : Sequelize.BIGINT,
        references: {
          model: "clinics_doctors",
          key  : "id"
        }
      },
      createdAt: {
        allowNull: true,
        type     : Sequelize.DATE
      },
      updatedAt: {
        allowNull: true,
        type     : Sequelize.DATE
      }
    });
  },

  down: (queryInterface: QueryInterface, Sequelize: SequelizeStatic) => {
    return Promise.all([
      queryInterface.dropTable("fd_cd"),
    ]);
  }
};
