import { QueryInterface, SequelizeStatic } from "sequelize";
import { Helpers } from "../util/helpers.util";
import { SymptomType } from "../enums/symptom.enum";
import { VisibilityEnum } from "../enums/visibility.enum";

export = {
  up: (queryInterface: QueryInterface, Sequelize: SequelizeStatic) => {
    return queryInterface.createTable("symptoms", {
      id               : {
        allowNull    : false,
        primaryKey   : true,
        autoIncrement: true,
        type         : Sequelize.BIGINT
      },
      clinic_doctor_id : {
        type      : Sequelize.BIGINT,
        allowNull : true,
        references: {
          model: "clinics_doctors",
          key  : "id"
        },
        onDelete  : "cascade"
      },
      doctor_id        : {
        type      : Sequelize.BIGINT,
        allowNull : false,
        references: {
          model: "doctors",
          key  : "id"
        },
        onDelete  : "cascade"
      },
      title            : {
        allowNull: false,
        type     : Sequelize.STRING
      },
      visibility_period: {
        type     : Sequelize.ENUM,
        values   : Helpers.iterateEnum(VisibilityEnum),
        allowNull: false
      },
      is_active       : {
        allowNull   : false,
        type        : Sequelize.BOOLEAN,
        defaultValue: true
      },
      createdAt        : {
        allowNull: true,
        type     : Sequelize.DATE
      },
      updatedAt        : {
        allowNull: true,
        type     : Sequelize.DATE
      },
      deletedAt        : {
        allowNull: true,
        type     : Sequelize.DATE
      }
    });
  },

  down: (queryInterface: QueryInterface, Sequelize: SequelizeStatic) => {
    return Promise.all([
      queryInterface.dropTable("symptoms"),
    ]);
  }
};
