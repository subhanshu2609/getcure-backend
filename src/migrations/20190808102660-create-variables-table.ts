import { QueryInterface, SequelizeStatic } from "sequelize";
import { Helpers } from "../util/helpers.util";
import { AppointmentTypes, BookingType, VisitType } from "../enums/token.enum";
import { Bookie } from "../enums/bookie.enum";
import { FeedbackFiller } from "../enums/feedback-filler.enum";
import { MedicineParameter, MedicineUpdateParameters } from "../enums/medicine-parameter.enum";

export = {
  up: (queryInterface: QueryInterface, Sequelize: SequelizeStatic) => {
    return queryInterface.createTable("variables", {
      id       : {
        allowNull    : false,
        primaryKey   : true,
        autoIncrement: true,
        type         : Sequelize.BIGINT
      },
      doctor_id: {
        allowNull : false,
        type      : Sequelize.BIGINT,
        references: {
          model: "doctors",
          key  : "id"
        }
      },
      title    : {
        allowNull: false,
        type     : Sequelize.STRING
      },
      value    : {
        allowNull: false,
        type     : Sequelize.INTEGER
      },
      type     : {
        type     : Sequelize.ENUM,
        allowNull: false,
        values   : Helpers.iterateEnum(MedicineUpdateParameters)
      },
      createdAt: {
        allowNull: true,
        type     : Sequelize.DATE
      },
      updatedAt: {
        allowNull: true,
        type     : Sequelize.DATE
      }
    });
  },

  down: (queryInterface: QueryInterface, Sequelize: SequelizeStatic) => {
    return Promise.all([
      queryInterface.dropTable("variables"),
    ]);
  }
};
