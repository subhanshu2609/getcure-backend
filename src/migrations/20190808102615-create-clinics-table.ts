import { QueryInterface, SequelizeStatic } from "sequelize";

export = {
  up: (queryInterface: QueryInterface, Sequelize: SequelizeStatic) => {
    return queryInterface.createTable("clinics", {
      id                : {
        allowNull    : false,
        primaryKey   : true,
        autoIncrement: true,
        type         : Sequelize.BIGINT
      },
      name              : {
        type     : Sequelize.STRING,
        allowNull: false
      },
      type              : {
        type     : Sequelize.STRING,
        allowNull: true
      },
      establishment_year: {
        type     : Sequelize.STRING,
        allowNull: true
      },
      no_of_beds        : {
        type     : Sequelize.INTEGER,
        allowNull: true
      },
      no_of_doctors     : {
        type     : Sequelize.INTEGER,
        allowNull: true
      },
      state             : {
        type     : Sequelize.STRING,
        allowNull: true,
      },
      state_id          : {
        type      : Sequelize.BIGINT,
        allowNull : true,
        references: {
          model: "states",
          key  : "id"
        },
        onDelete  : "set null"
      },
      timings           : {
        type     : Sequelize.STRING,
        allowNull: false
      },
      about             : {
        type     : Sequelize.TEXT,
        allowNull: true
      },
      specialities      : {
        type     : Sequelize.JSON,
        allowNull: true
      },
      address           : {
        type     : Sequelize.STRING,
        allowNull: false
      },
      pin_code          : {
        type     : Sequelize.STRING,
        allowNull: true
      },
      phone_no          : {
        type     : Sequelize.STRING,
        allowNull: true
      },
      latitude          : {
        type     : Sequelize.STRING,
        allowNull: true
      },
      longitude         : {
        type     : Sequelize.STRING,
        allowNull: true
      },
      get_cure_code     : {
        type     : Sequelize.STRING,
        allowNull: true
      },
      image_urls        : {
        type     : Sequelize.JSON,
        allowNull: true
      },
      is_active         : {
        type        : Sequelize.BOOLEAN,
        allowNull   : false,
        defaultValue: true
      },
      createdAt         : {
        allowNull: true,
        type     : Sequelize.DATE
      },
      updatedAt         : {
        allowNull: true,
        type     : Sequelize.DATE
      },
      deletedAt         : {
        allowNull: true,
        type     : Sequelize.DATE
      }
    });
  },

  down: (queryInterface: QueryInterface, Sequelize: SequelizeStatic) => {
    return Promise.all([
      queryInterface.dropTable("clinics"),
    ]);
  }
};
