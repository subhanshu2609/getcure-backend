import { QueryInterface, SequelizeStatic } from "sequelize";
import { Helpers } from "../util/helpers.util";

export = {
  up: (queryInterface: QueryInterface, Sequelize: SequelizeStatic) => {
    return queryInterface.createTable("employees", {
      id           : {
        allowNull    : false,
        primaryKey   : true,
        autoIncrement: true,
        type         : Sequelize.BIGINT
      },
      first_name   : {
        type     : Sequelize.STRING,
        allowNull: false
      },
      last_name    : {
        type     : Sequelize.STRING,
        allowNull: false
      },
      password     : {
        type     : Sequelize.STRING,
        allowNull: true
      },
      type         : {
        type     : Sequelize.STRING,
        allowNull: false
      },
      aadhar_no    : {
        type     : Sequelize.STRING,
        allowNull: false
      },
      mobile_no    : {
        type     : Sequelize.STRING,
        unique   : true,
        allowNull: false
      },
      gender       : {
        type     : Sequelize.ENUM,
        values   : [
          "male",
          "female"
        ],
        allowNull: false
      },
      dob          : {
        type     : Sequelize.DATEONLY,
        allowNull: false
      },
      profile_image: {
        type     : Sequelize.STRING,
        allowNull: true
      },
      address      : {
        type     : Sequelize.STRING,
        allowNull: false
      },
      pin_code     : {
        type     : Sequelize.STRING,
        allowNull: false
      },
      is_active    : {
        allowNull: false,
        type     : Sequelize.BOOLEAN
      },
      createdAt    : {
        allowNull: true,
        type     : Sequelize.DATE
      },
      updatedAt    : {
        allowNull: true,
        type     : Sequelize.DATE
      },
      deletedAt    : {
        allowNull: true,
        type     : Sequelize.DATE
      }
    });
  },

  down: (queryInterface: QueryInterface, Sequelize: SequelizeStatic) => {
    return Promise.all([
      queryInterface.dropTable("employees"),
    ]);
  }
};
