import { QueryInterface, SequelizeStatic } from "sequelize";
import { Helpers } from "../util/helpers.util";
import { AppointmentTypes, BookingType, VisitType } from "../enums/token.enum";
import { Bookie } from "../enums/bookie.enum";
import { FeedbackFiller } from "../enums/feedback-filler.enum";

export = {
  up: (queryInterface: QueryInterface, Sequelize: SequelizeStatic) => {
    return queryInterface.createTable("feedbacks", {
      id              : {
        allowNull    : false,
        primaryKey   : true,
        autoIncrement: true,
        type         : Sequelize.BIGINT
      },
      clinic_doctor_id: {
        allowNull : false,
        type      : Sequelize.BIGINT,
        references: {
          model: "clinics_doctors",
          key  : "id"
        }
      },
      patient_visit_id: {
        allowNull : false,
        type      : Sequelize.BIGINT,
        references: {
          model: "patients_visits",
          key  : "id"
        }
      },
      question        : {
        allowNull: false,
        type     : Sequelize.STRING
      },
      option          : {
        type     : Sequelize.STRING,
        allowNull: false
      },
      filler          : {
        type     : Sequelize.ENUM,
        allowNull: false,
        values   : Helpers.iterateEnum(FeedbackFiller)
      },
      medicine        : {
        type     : Sequelize.JSON,
        allowNull: true
      },
      createdAt       : {
        allowNull: true,
        type     : Sequelize.DATE
      },
      updatedAt       : {
        allowNull: true,
        type     : Sequelize.DATE
      }
    });
  },

  down: (queryInterface: QueryInterface, Sequelize: SequelizeStatic) => {
    return Promise.all([
      queryInterface.dropTable("feedbacks"),
    ]);
  }
};
