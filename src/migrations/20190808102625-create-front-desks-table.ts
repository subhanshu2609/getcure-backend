import { QueryInterface, SequelizeStatic } from "sequelize";

export = {
  up: (queryInterface: QueryInterface, Sequelize: SequelizeStatic) => {
    return queryInterface.createTable("front_desks", {
      id         : {
        allowNull    : false,
        primaryKey   : true,
        autoIncrement: true,
        type         : Sequelize.BIGINT
      },
      name       : {
        type     : Sequelize.STRING,
        allowNull: false
      },
      email      : {
        type     : Sequelize.STRING,
        unique   : true,
        allowNull: false
      },
      mobile_no  : {
        type     : Sequelize.STRING,
        allowNull: false,
        unique   : true,
      },
      password   : {
        type     : Sequelize.STRING,
        allowNull: false
      },
      gender     : {
        type     : Sequelize.ENUM,
        allowNull: true,
        values   : [
          "male",
          "female",
          "other"
        ]
      },
      clinic_id  : {
        allowNull : false,
        type      : Sequelize.BIGINT,
        references: {
          model: "clinics",
          key  : "id"
        },
        onDelete  : "cascade"
      },
      image_url: {
        allowNull: true,
        type: Sequelize.STRING
      },
      is_active  : {
        allowNull   : false,
        type        : Sequelize.BOOLEAN,
        defaultValue: true
      },
      createdAt  : {
        allowNull: true,
        type     : Sequelize.DATE
      },
      updatedAt  : {
        allowNull: true,
        type     : Sequelize.DATE
      },
      deletedAt  : {
        allowNull: true,
        type     : Sequelize.DATE
      }
    });
  },

  down: (queryInterface: QueryInterface, Sequelize: SequelizeStatic) => {
    return Promise.all([
      queryInterface.dropTable("front_desks"),
    ]);
  }
};
