import { QueryInterface, SequelizeStatic } from "sequelize";
import { Helpers } from "../util/helpers.util";
import { AppointmentTypes, BookingType, VisitType } from "../enums/token.enum";
import { Bookie } from "../enums/bookie.enum";
import { FeedbackFiller } from "../enums/feedback-filler.enum";
import { MedicineParameter } from "../enums/medicine-parameter.enum";

export = {
  up: (queryInterface: QueryInterface, Sequelize: SequelizeStatic) => {
    return queryInterface.createTable("visit_attachments", {
      id       : {
        allowNull    : false,
        primaryKey   : true,
        autoIncrement: true,
        type         : Sequelize.BIGINT
      },
      doctor_id: {
        allowNull : false,
        type      : Sequelize.BIGINT,
        references: {
          model: "doctors",
          key  : "id"
        }
      },
      clinic_doctor_id: {
        allowNull : false,
        type      : Sequelize.BIGINT,
        references: {
          model: "clinics_doctors",
          key  : "id"
        }
      },
      patient_visit_id: {
        allowNull : false,
        type      : Sequelize.BIGINT,
        references: {
          model: "patients_visits",
          key  : "id"
        }
      },
      appointment_time: {
        allowNull: false,
        type: Sequelize.STRING
      },
      attachment_url: {
        allowNull: false,
        type: Sequelize.STRING
      },
      createdAt: {
        allowNull: true,
        type     : Sequelize.DATE
      },
      updatedAt: {
        allowNull: true,
        type     : Sequelize.DATE
      }
    });
  },

  down: (queryInterface: QueryInterface, Sequelize: SequelizeStatic) => {
    return Promise.all([
      queryInterface.dropTable("visit_attachments"),
    ]);
  }
};
